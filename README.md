## About BigCommerce ##
https://www.bigcommerce.com/
BigCommerce is allowing sellers to create an online store, which is hosted in the cloud, not on customers’ servers. They charge monthly fees and offer some level of customization.

## GeekSeller’s BigCommerce Integration ##
Our integration allows a seller to connect multiple BigCommerce stores to a single GeekSeller accounts. GeekSeller and BigCommerce exchange data about orders and products.

### Version 1.0 ###
* PRODUCTS: The extension allows to import products from BigCommerce to BC.GeekSeller.com database, then export them to formatted in a specific way CSV file.
* QTY: The extension allows to either read qty from BigCommerce stores, store in BC.GeekSeller.com database then send them to other GeekSeller panels OR the extension sends qty data to BigCommerce.
* ORDERS: the extension can read orders information from BC and send them to other panels of GeekSeller, it can also take from other panels of GeekSeller information, and update with this information located on BigCommerce stores data about orders.
* User does NOT have a nice interface for BC.GeekSeller.com, this panel is designed for admin to use, a user just needs to provide API keys to admin and request connection.

### Version 2.0 (not implemented yet) ###
* User can create products on BC.GeekSeller.com and send them to BigCommerce
* …


# Slim Framework 3 Skeleton Application


### Notes

https://github.com/igorw/websockets-talk/blob/master/IgorwRealtimeLoggingBundle/Monolog/ZeromqHandler.php
https://gist.github.com/jadb/3949954 - cli parser
https://amphp.org/amp/event-loop/ event loop

### TODO
 * Auth
 * Todo List


### BigCommerce api

    check header limits !
    before pool creates tasks check request limist

## [Supervisor: A Process Control System](docs/Supervisor.md)

### API
* [supervisorphp](https://github.com/supervisorphp/supervisor) - php api library