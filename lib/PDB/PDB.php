<?php

namespace PDB;

use PDO;


class PDB extends PDO {

	function __construct(string $host, string $port, string $dbname, string $user, string $password)
	{
		if ($host === '')
			$dsn = sprintf('pgsql:dbname=%s',
				$dbname );
		else
			$dsn = sprintf('pgsql:host=%s;port=%s;dbname=%s',
				$host, $port, $dbname );

		$opt = [
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_STATEMENT_CLASS => [ 'PDBStatement', [] ] ];

		parent::__construct($dsn, $user, $password);

//		$this->exec('SET SESSION CHARACTERISTICS AS TRANSACTION ISOLATION LEVEL REPEATABLE READ');
	}


	function one(string $sql, array $data = null) : array
	{
		if ($data === null)
			$qry = $this->query($sql);
		else {
			$qry = $this->prepare($sql);
			$qry->execute($data); }

		$a = $qry->fetchAll();
		switch ($cnt = count($a)) {
		case 1:
			return array_shift($a);
		default:
			throw new \PDB\RowNotFoundException('expected exactly one record, got ' .$cnt); }
	}


	public
	function many(string $sql, array $data = null) : array
	{
		if ($data === null){
			$qry = $this->query($sql);
        } else {
			$qry = $this->prepare($sql);
			$qry->execute($data);
		}

		return $qry->fetchAll();
	}


	public
	function QInsert(string $to_table) : QInsert
	{
		return new QInsert($this, $to_table);
	}


	public
	function QUpdate(string $table) : QUpdate
	{
		return new QUpdate($this, $table);
	}


	/**
	* escape SQL name (database, table, column, etc)
	* as per: "a. use backquotes and disallow the backquote, backslash and nul character in names because escaping them is unreliable"
	* http://stackoverflow.com/a/1543309
	*/
	function escapeName(string $name) : string
	{
		if (strpos($name, "\0") !== false)
			throw new Exception('cannot handle NIL');

			# backslash is just fine in PostgreSQL's quoted /identifiers/
			# (the situation is more complex for quoted strings)
			# as per https://www.postgresql.org/docs/9.6/static/sql-syntax-lexical.html
			# >Quoted identifiers can contain any character, except the character with code zero.
			# >(To include a double quote, write two double quotes.) This allows constructing table
			# >or column names that would otherwise not be possible, such as ones containing spaces
			# >or ampersands. The length limitation still applies.
		if (false)
		if (strpos($name, '\\') !== false)
			throw new Exception('cannot handle backslash');

		return '"' . str_replace('"', '""', $name) .'"';
	}


	function requireTransaction()
	{
		if (!$this->inTransaction())
			throw new LogicException('the calling code requires an active DB transaction');
	}
}
