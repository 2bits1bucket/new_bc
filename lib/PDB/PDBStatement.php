<?php

namespace PDB;

class PDBStatement extends PDOStatement
{
	protected
	function __construct()
	{
	}


	function one(array $data = null) : array
	{
		if ($data === null)
			$this->execute();
		else
			$this->execute($data);

		$a = $this->fetchAll();
		switch ($cnt = count($a)) {
		case 1:
			return array_shift($a);
		default:
			throw new RowNotFoundException('expected exactly one record, got ' .$cnt);
		}
	}


	function many(array $data = null) : array
	{
		if ($data === null)
			$this->execute();
		else
			$this->execute($data);

		return $this->fetchAll();
	}
}
