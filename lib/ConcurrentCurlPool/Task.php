<?php


namespace ConcurrentCurlPool;

abstract class Task {

	private $task_completed = false;

	abstract function taskCurlHandle(); # : resource
	abstract function taskRequestProcessed();

	function taskResponse() : TaskResponse
	{
		return new TaskResponse($this->taskCurlHandle());
	}


	function taskExceptionHandler(\Throwable $e)
	{
	    echo "Task Exception handler";
	    echo $e->getMessage();
        echo "\n";

        if(isset($e->xdebug_message)){
            echo $e->xdebug_message;
        }else{
            echo $e->getFile().PHP_EOL;
            echo $e->getTraceAsString();
        }

    }


	/////////////////////////
    function log($val) {
        echo microtime().' | ';
        printf($val.PHP_EOL);
        if (ob_get_level())
            ob_flush();
        flush();
    }


    function rutime($ru, $rus, $index) {
        return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
            -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
    }


}

