<?php


namespace ConcurrentCurlPool;


abstract
class MultiStepTask extends Task
{
	abstract
	function hasNextTask() : bool;

	abstract
	function nextTask() : Task;


	function signalTaskChainCompleted() {}

}
