<?php


namespace ConcurrentCurlPool;


class TaskResponse
{
	protected $curl_info;


	function __construct(/* resource*/ $curl_handle)
	{
		$this->curl_info = curl_getinfo($curl_handle);
		$this->body = curl_multi_getcontent($curl_handle);
	}


	function body() : string
	{
		return $this->body;
	}
}
