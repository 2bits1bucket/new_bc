<?php


namespace ConcurrentCurlPool;


class Pool
{
	protected $nrConcurrentSlots;
	protected $tasksQueue = [];
	protected $tasksRunning = [];
	protected $multi_handle;


	function __construct(int $nrConcurrentSlots)
	{
		$this->nrConcurrentSlots = $nrConcurrentSlots;
		$this->multi_handle = curl_multi_init();
	}

    //TODO WHY ?
	function __destruct()
	{
	//	curl_multi_close($this->multi_handle);
	}

	function addTask(Task $Task)
	{
		if ($Task instanceof NullTask){
            return;
        }

		$this->tasksQueue[] = $Task;
	}

	function select(float $timeoutSeconds = 120) : \Generator
	{
		$nr_running = $previous_nr_running = null;
		do {
			$this->maintainTaskQueueRunningTasks();
//			echo '.'; xflush();
			$count = curl_multi_select($this->multi_handle, $timeoutSeconds);
				# as suggested by man curl_multi_fdset(), if curl_multi_fdset() returns -1,
				# the connection(s) in question cannot be monitored for activity via select()
				# and instead we're supposed to sleep for a short while.
				# they suggest 100ms. let's go with 50ms :)
			if ($count === -1)
				usleep(100 * 1000); // 50*1000

				# as described in C:curl_multi_perform() man page, when C:curl_multi_perform()
				# returns CURLM_CALL_MULTI_PERFORM, we are supposed to call
				# this function again right away, because there's more work queued to do
			do {
				$v = curl_multi_exec($this->multi_handle, $nr_running); }
			while ($v === CURLM_CALL_MULTI_PERFORM);

			if ($nr_running !== $previous_nr_running)
				while (($info = curl_multi_info_read($this->multi_handle)) !== false) {
					if (!is_resource($info['handle']))
						throw new \LogicException('got info, but not handle');
					$Task = $this->taskForResource($info['handle']);
//					echo '^'; xflush();
					try {
						$Task->taskRequestProcessed();
						yield $Task; }
					catch (\Throwable $e) {
						$Task->taskExceptionHandler($e); }
					finally {
						$this->dropTask($Task); } }
			$previous_nr_running = $nr_running;
		} while ($this->hasTasksP());

		if ($nr_running)
			throw new \LogicException('!->hasTasksP() but $nr_running > 0');
	}


	private
	function taskForResource(/*resource*/ $resource) : Task
	{
		foreach ($this->tasksRunning as $Task)
			if ($Task->taskCurlHandle() === $resource)
				return $Task;
		throw new \LogicException('could not find task for the resource');
	}


	/*
	 * This can happen after __destruct()
	 */
	private
	function dropTask(Task $Task)
	{

		curl_multi_remove_handle($this->multi_handle, $Task->taskCurlHandle());

		foreach ($this->tasksRunning as $n => $T)
			if ($Task === $T) {
				unset($this->tasksRunning[$n]);
				return; }

		throw new \LogicException('cound not find the task that\'s supposed to be dropped');
	}


	private function maintainTaskQueueRunningTasks()
	{
		while (($this->tasksQueue) && (count($this->tasksRunning) < $this->nrConcurrentSlots)) {
			$Task = array_shift($this->tasksQueue);
			# $Task->taskEnqueued();
//            echo '+'; xflush();
			curl_multi_add_handle($this->multi_handle, $Task->taskCurlHandle());
			$this->tasksRunning[] = $Task;
		}
	}


	function hasTasksP() : bool
	{
		return !empty($this->tasksQueue) || !empty($this->tasksRunning);
	}
}
