<?php


namespace ConcurrentCurlPool;


	# represents a task that does NOT need execution
	# to be used in case where implementing MultiStepTask->hasNextTask() is too complex or expensive
class NullTask extends Task
{
	function taskCurlHandle() # : resource
	{
		throw new \LogicException('null task, this method should not be called');
	}


	function taskRequestProcessed()
	{
		throw new \LogicException('null task, this method should not be called');
	}
}
