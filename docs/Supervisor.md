## Supervisor: A Process Control System

[Supervisor](http://supervisord.org/) is a client/server system that allows its users to monitor and control a number of processes on UNIX-like operating systems.

## Installation centos

    $ yum install python-setuptools
    $ easy_install supervisor

  You should now have the latest version installed:

    $ supervisord --version
    3.1.0

## init.d

To run services via an init script. Example file supervisor_init.d place and rename in /etc/rc.d/init.d/supervisord):
You will need to create that file, containing the above script, and make sure it's executable

## Settings

 supervisord.conf - configuration file in project root dir

### Adding a program

 Example Command entry for supervisord.conf file

     [program:foo_bar]
     command=/usr/bin/php7.1 /home/masakra/HTTP/www/yada/bin/run foo_bar
     out_logfile=/home/masakra/HTTP/www/yada/logs/foo_bar.log
     stdout_logfile_maxbytes=0
     redirect_stderr=true

## Commands

    $ service supervisord start
    $ service supervisord stop
    $ service supervisord status
    $ service supervisord restart

### Starting/Stop

Those commands dont reload configuration !

    service supervisor restart

Restart supervisor service without making configuration changes available. It stops, and re-starts all managed applications.

    supervisorctl restart <name>
Restart application without making configuration changes available. It stops, and re-starts the application.


### Reload Configuration

If you want to apply your configuration changes in both existing and new configurations, start applications in new configurations, and re-start all managed applications, you should run:

    service supervisor stop
    service supervisor start

If you do not want to re-start all managed applications, but make your configuration changes available, use this command:

supervisorctl reread
This command only updates the changes. It does not restart any of the managed applications, even if their configuration has changed. New application configurations cannot be started, neither. (See the “update” command below)

supervisorctl update
Restarts the applications whose configuration has changed.
Note: After the update command, new application configurations becomes available to start, but do not start automatically until the supervisor service restarts or system reboots (even if autostart option is not disabled). In order to start new application, e.g app2, simply use the following command:

supervisorctl start app2

    service supervisor stop

    service supervisor start


If you do not want to re-start all managed applications, but make your configuration changes available, use this command:

    supervisorctl reread

This command only updates the changes. It does not restart any of the managed applications, even if their configuration has changed. New application configurations cannot be started, neither. (See the “update” command below)


## Superlance

Superlance is a set of additional plugins for Supervisor, that includes plugins for sending email or SMS alerts when a Supervisor process exists unexpectedly, for monitoring memory usage of Supervisor processes, and a few other useful monitoring and alerting utilities. You can also install it via EasyInstall:

    $ easy_install superlance



