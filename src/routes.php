<?php

$app->get('/test-product-limit', '\BigCommerce\Controllers\BcMainController:getProducts');

$app->get('/aa', function ($request, $response, $args) {
    return "<code> UI Disabled </code>";
});

$app->get('/', function ($request, $response, $args) {
    return $this->renderer->render($response, '/src/templates/index.phtml', $args);
});

$app->group('/api', function(){

    $this->group('/auth', function(){
        $this->post('/login',  '\Auth\AuthController:doLogin');
        $this->post('/logout',  '\Auth\AuthController:doLogout');
        $this->get('/check',  '\Auth\AuthController:validToken');
        $this->get('/privilege',  '\Auth\AuthController:privileges');
    });

    $this->post('/account', '\BigCommerce\Controllers\AccountController:persist');
    $this->put('/account', '\BigCommerce\Controllers\AccountController:persist');

    $this->get('/account/list', '\BigCommerce\Controllers\AccountController:getList');
    $this->get('/account/{id}', '\BigCommerce\Controllers\AccountController:getById');
    $this->put('/account/{id}', '\BigCommerce\Controllers\AccountController:update');
    $this->delete('/account/{id}', '\BigCommerce\Controllers\AccountController:deactivate');

    $this->get('/get-jet-csv', '\BigCommerce\Controllers\CsvFileController:jetCsvDownload');

    $this->group('/bc', function(){
        $this->get('/account/check', '\BigCommerce\Controllers\AccountController:check');
    });

});

