<?php

namespace Admin;


use Slim\Container as Container;
use Auth\ForbiddenException as ForbiddenException;
use Auth\AuthService as AuthService;

use Firebase\JWT\JWT;
use Tuupola\Base62;
use DateTime;


class AdminController
{

    protected $ci;
    private static $TPL_DIR = 'src/templates/';

    //Constructor
    public function __construct(Container $ci) {
        $this->ci = $ci;
    }



    // - below is shity dumpy dump

    public function index($request, $response, $args){
        var_dump($request);
    }
    

    //http://stackoverflow.com/questions/29121112/how-to-implement-token-based-authentication-securely-for-accessing-the-website
    public function doLogin($request, $response, $args){

        $user = AuthService::userByAuth($request);

        $scopes = [
            "todo.create",
            "todo.read",
            "todo.update",
            "todo.delete",
            "todo.list",
            "todo.all"
        ];

        $now = new DateTime();
        $future = new DateTime("now +2 hours");
        $server = $request->getServerParams();
        $jti = Base62::encode(random_bytes(16));
        
        $payload = [
            "iat" => $now->getTimeStamp(),
            "exp" => $future->getTimeStamp(),
            "jti" => $jti,
            "sub" => "admin",// $server["PHP_AUTH_USER"],
            "scope" => $scopes
        ];
        
        $secret = getenv("JWT_SECRET");
        $token = JWT::encode($payload, $secret, "HS256");

        $data["status"] = "ok";
        $data["token"] = $token;

        /*
         * make login in javascript ! - with backbone !
         *
         * redirect on success with token !
         * https://www.sitepoint.com/php-authorization-jwt-json-web-tokens/
         * https://github.com/alexanderscott/backbone-login
         *  e.preventDefault();
        $.ajax({
            url: 'resource/image',
            beforeSend: function(request){
                request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
            },
            type: 'GET',
            success: function(data) {
                // Decode and show the returned data nicely.
            },
            error: function() {
                alert('error');
            }
        });

         */

        if(null!=$user){

            return $response->withStatus(200)->withHeader('Location', '/admin')->withHeader('token',$token );

            return $response->withStatus(201)->withHeader("Content-Type", "application/json")->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

        }else{
            $args['login_error']=true;
            return $this->loginView($request, $response, $args);
        }

    }

    public function loginView($request, $response, $args){
        return $this->ci->renderer->render($response, self::$TPL_DIR.'login.phtml', $args);
    }

    public function indexView($request, $response, $args){

//        if (false === $this->ci->token->hasScope(["todo.all", "todo.list"])) {
//            throw new ForbiddenException("Token not allowed to list todos.", 403);
//        }

        return $this->ci->renderer->render($response, self::$TPL_DIR.'admin.phtml', $args);
    }

    public function __invoke($request, $response, $args) {
        //your code
        //to access items in the container... $this->ci->get('');
    }

}