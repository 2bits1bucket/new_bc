<?php

namespace Auth;

use Slim\Container as Container;

use \Auth\Models\Group as Group;

class GroupController {

    protected $ci;

    public function __construct(Container $ci) {
        $this->ci = $ci;
    }

    public function getList($request, $response, $args){

        $list = Group::all();

        return self::jsonResponse($response, $list);
    }

    public function create($request, $response, $args){

        $group = new Group();

        $data = $request->getParams();

        $group->name = $data['name'];
        $group->desc = $data['desc'];

        $group->save();

        return self::jsonResponse($response, $group);
    }

    public function update($request, $response, $args){

        $group = Group::find(1); //$args['id'] ?

        $data = $request->getParams();

        $group->name = $data['name'];
        $group->desc = $data['desc'];

        $group->save();

        return self::jsonResponse($response, $group);
    }

    public function delete($request, $response, $args){

        $group = Group::find(1);

        $group->active = false;

        $group->save();

        return self::jsonResponse($response, $group);
    }

    public static function jsonResponse($response, $data){
        return $response->withHeader("Content-Type", "application/json")->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
    }

    public function __invoke($request, $response, $args) {
        //to access items in the container... $this->ci->get('');
    }

}