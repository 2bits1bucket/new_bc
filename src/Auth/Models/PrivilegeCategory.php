<?php

namespace Auth\Models;

use Illuminate\Database\Eloquent\Model;

class PrivilegeCategory extends Model
{

    /*
        id	name	menu	icon	scope
     */
    protected $table = 'privilege_category';

    public function getDates()
    {
        return array();
    }

    public function privileges()
    {
        return $this->hasMany('Auth\Models\Privilege', 'category_id');
    }

}

