<?php

namespace Auth\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    protected $hidden = ['passwd','token','token_expired'];

    public function getDates()
    {
        return array();
    }
}