<?php

namespace Auth\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Created for View - READ ONLY
 * Class Privilege
 * @package models\Auth
 */
class Privilege extends Model
{

    /*
        category_id,
        category_name,
        category_in_menu,
        category_icon,
        scope,
        id,
        name,
        desc,
        icon,
        sub_scope,
        user_available,
        in_menu,
        user_id
     */
    protected $table = 'user_privileges';

    public function getDates()
    {
        return array();
    }

}

/*

CREATE VIEW user_privileges AS
SELECT
    pc.id as category_id,
    pc.name as category_name,
    pc.menu as category_in_menu,
    pc.icon as category_icon,
    pc.scope as scope,
    p.id as id,
    p.name as name,
    p.desc as desc,
    p.icon as icon,
    p.scope as sub_scope,
    p.user_available as user_available,
    p.menu as in_menu,
    ug.user_id
    FROM user_group ug
    LEFT JOIN group_privilege gp ON ug.group_id = gp.group_id
    LEFT JOIN privileges p ON gp.privilege_id = p.id
    LEFT JOIN privilege_category pc ON p.category_id = pc.id
UNION
SELECT
    pc.id as category_id,
    pc.name as category_name,
    pc.menu as category_in_menu,
    pc.icon as category_icon,
    pc.scope as scope,
    p.id as id,
    p.name as name,
    p.desc as desc,
    p.icon as icon,
    p.scope as sub_scope,
    p.user_available as user_available,
    p.menu as in_menu,
    up.user_id
    FROM user_privilege up
    LEFT JOIN privileges p ON up.privilege_id = p.id
    LEFT JOIN privilege_category pc ON p.category_id = pc.id
*/