<?php

namespace Auth;

use Slim\Container as Container;
use Firebase\JWT\JWT;
use Tuupola\Base62;
use DateTime;

use \Auth\Models\User as User;

/*
    https://www.sitepoint.com/php-authorization-jwt-json-web-tokens/
    https://github.com/alexanderscott/backbone-login

        $.ajax({
            beforeSend: function(request){
                request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
            },

    http://stackoverflow.com/questions/29121112/how-to-implement-token-based-authentication-securely-for-accessing-the-website
 */
class AuthController {

    protected $ci;

    public function __construct(Container $ci) {
        $this->ci = $ci;
    }

    public function doLogin($request, $response, $args){

        $user = AuthService::userByAuth($request);

        if(null!=$user){

            $scopes = AuthService::getUserScopes($user->id);

            $now = new DateTime();
            $future = new DateTime("now +2 hours");

//            $server = $request->getServerParams();

            $jti = Base62::encode(random_bytes(16));

            $payload = [
                "iat" => $now->getTimeStamp(),
                "exp" => $future->getTimeStamp(),
                "jti" => $jti,
                "sub" => $user->username,
                "scope" => $scopes
            ];

            $secret = getenv("JWT_SECRET");
            $token = JWT::encode($payload, $secret, "HS256");

            $data["status"] = "ok";
            $data["user"]= $user;
            $data["token"] = $token;

            //$user->last_login_from
            //$user->last_login_user_agent

            $user->token_expired = $future;
            $user->token = $token;

//            $user->save();

            return $response->withStatus(201)->withHeader("Content-Type", "application/json")->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

        }else{
            return $response->withStatus(400)->write('NO_USER'); //- bad login or password FIXME THAT STUFF
        }

    }

    public function validToken($request, $response, $args) {

        if(count($request->getHeader('Authorization'))==0) return $response->withStatus(400)->write('NO_AUTH_HEADER');

        preg_match("/Token token=\"(.*)\"/" ,$request->getHeader('Authorization')[0], $matches);

        if(count($matches)!=2) return $response->withStatus(400)->write('NO_HEADER_TOKEN');

        $token = $matches[1];

        //$user = User::where('token', $token)->first(); //TODO expired_date

        //if(null==$user) return $response->withStatus(400)->write('NO_USER_FOR_TOKEN='.$token);

        $user['scopes'] = ['bc.accounts'];

        $data['user'] = ['user_id'=>1, 'username'=>'geekseller'];

        return $response->withHeader("Content-Type", "application/json")->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }

    public static function privilege($request, $response, $args){


        $user_id = 1; //make decode token to user id ! -> get by sql ? //security basics

//        $data = AuthService::getUserMenuPrivileges($user_id);

        /*

        category_id,
        category_name,
        category_in_menu,
        category_icon,
        scope,
        id,
        name,
        desc,
        icon,
        sub_scope,
        user_available,
        in_menu,
        user_id

        */

        $data[] = [
            'category_id'=>1,
            'category_name'=>'name',
            'category_in_menu'=>'in_menu_name',
            'category_icon'=>'fa fa-pen',
            'scope' => 'bc.account',
            'id' =>1 ,
            'name' => 'Accounts',
            'desc' => ' some ',
            'icon' => 'fa fa-user',
            'sub_scope',
            'user_available'=> true,
            'in_menu' => true,
            'user_id'=>1
        ];

        return $response->withHeader("Content-Type", "application/json")->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }

    public static function privileges($request, $response, $args)
    {

        $privilege = [];

        $privilege['id'] = 2;
        $privilege['name'] = 'Accounts';
        $privilege['desc'] = ' Big Commerce Accounys';
        $privilege['icon'] = '';
        $privilege['sub_scope'] = 'accounts';
        $privilege['user_id'] = 1;
        $privilege['user_available'] = true;
        $privilege['in_menu'] = true;

        $category = [];

        $category['id'] = 1;
        $category['name'] = 'Big Commerce';
        $category['in_menu'] = true;
        $category['scope'] = 'bc';
        $category['icon'] = 'fa-users';
        $category['privileges'][] = $privilege;

        $data[] =$category;

        return $response->withHeader("Content-Type", "application/json")->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
    }

    public function doLogout($request, $response, $args) {
        return $response->withStatus(200)->withJson(['status', 'success']);
    }

    public function loginView($request, $response, $args) {

    }

    public function homeView($request, $response, $args) {
        var_dump($request);
    }

    public function __invoke($request, $response, $args) {
        //your code
        //to access items in the container... $this->ci->get('');
    }

}