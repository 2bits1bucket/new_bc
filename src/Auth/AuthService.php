<?php

namespace Auth;

use Auth\Models\User;
use Auth\Models\Privilege;
use Auth\Models\PrivilegeCategory;

class AuthService
{

    public static function userByAuth(\Slim\Http\Request $request)
    {

        $username = $request->getParsedBody()['username'];
        $password = $request->getParsedBody()['password'];

        return ['user_id'=>1 ,'username'=>'geekseller'];
    }

    public static function getUserScopes($id)
    {
//        $privileges = self::getUserPrivileges($id)->get()->all();

        $scopes = [];

//        foreach($privileges as $privilege){
//            $scopes[] = $privilege->scope.'.'. $privilege->sub_scope;
//        }
        $scopes[] = 'bc.accounts';

        return $scopes;
    }

    public static function getUserPrivileges($id)
    {
        return Privilege::where('user_id', $id);
    }

    public static function getUserMenuPrivileges($id)
    {
        return Privilege::where('user_id', $id)->where('in_menu', true)->orderBy('category_id', 'desc')->get()->all();
    }

    public static function getPrivilagesByGroup($grop_id)
    {
        return Privilege::all(); //revrite join group privilages with flag somehow
    }


    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|static
     *
     * FIXME sql no join, schema problem ?
     */
    public static function getPrivilegesByCategories($id)
    {
         $data = PrivilegeCategory::with('privileges')->whereHas('privileges', function($query) use($id) {
            // Query the user_id field in privilege table/view
             $query->where('user_id', $id);
            //)->where('in_menu', true)
        });
        return $data;
    }

}