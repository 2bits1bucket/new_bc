<?php

namespace Auth;

use Slim\Container as Container;
use DateTime;

use \Auth\Models\User as User;

class UserController {

    protected $ci;

    public function __construct(Container $ci) {
        $this->ci = $ci;
    }

    public function getList($request, $response, $args){

        $list = User::all(); //TODO  with groups

        return self::jsonResponse($response, $list);
    }

    public function getById($request, $response, $args){
        
        $user = User::find(1); //$args['id'] ?

        return self::jsonResponse($response, $user);
    }

    public function create($request, $response, $args){

        $user = new User();

        $data = $request->getParams();

        $user->username = $data['username'];
        $user->firstname = $data['firstname'];
        $user->lastname = $data['lastname'];
        $user->passwd = md5($data['passwd']);
        $user->active = true;
        $user->email = $data['email'];
        $user->phone = $data['phone'];

        $user->save();

        return self::jsonResponse($response, $user);
    }

    public function update($request, $response, $args){

        $user = User::find(1); //$args['id'] ?

        $user = new User();

        $data = $request->getParams();

        $user->username = $data['username'];
        $user->firstname = $data['firstname'];
        $user->lastname = $data['lastname'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];

        $user->save();

        return self::jsonResponse($response, $user);
    }

    public function deactivate($request, $response, $args){

        $user = User::find(1);

        $user->active = false;

        $user->save();

        return self::jsonResponse($response, $user);
    }

    public function activate($request, $response, $args){

        $user = User::find(1);

        $user->active = true;

        $user->save();

        return self::jsonResponse($response, $user);
    }

    public static function jsonResponse($response, $data){
        return $response->withHeader("Content-Type", "application/json")->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
    }

    public function __invoke($request, $response, $args) {
        //to access items in the container... $this->ci->get('');
    }

}