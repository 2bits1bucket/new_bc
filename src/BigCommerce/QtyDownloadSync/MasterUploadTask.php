<?php


namespace BigCommerce\QtyDownloadSync;


use BigCommerce\Models\BCAccount;
use BigCommerce\Services\ProductService;

class MasterUploadTask extends \ConcurrentCurlPool\MultiStepTask
{

	protected $Account;
	protected $curl_handle;
	protected $QS;
	protected $products;
	protected $productService;
	protected $productsBySku = [];
	private $nextTask;

	function __construct(BCAccount $Account, $products, ProductService $productService)
	{
		$this->Account = $Account;
        $this->products = $products;
        $this->productService = $productService;
	}

	private
	function apiAuth() : string
	{
		return $_ENV['API_MASTER_INVENTORY_LOGIN'] .':' .$_ENV['API_MASTER_INVENTORY_PASSWORD'];
	}


	private
	function apiUrlInventorySet() : string
	{
		return $_ENV['API_MASTER_URL_BASE'] .'inventory/v1/set.php';
	}

	public function BCAccount(){
	    return $this->Account;
    }

	function requestContent() : string
	{
		$user_id = $this->Account->user_id;
		$master_node = $this->Account->qty_download_master_node;
		$products = [];

		foreach ($this->products as $rcd) {

            $sku = ($rcd['inventory_tracking']=='variant') ? $rcd['sku'] : $rcd['prod_sku'];
            $inventory_level = ($rcd['inventory_tracking']=='variant') ? $rcd['inventory_level'] : $rcd['prod_inv_lvl'];

            $products[$sku] = [
                $master_node => [ 'type' => 'set', 'qty' => $inventory_level ]
            ];

            $this->productsBySku[$sku] = $rcd;
		}

		$payload = compact('user_id', 'products');

		return json_encode(compact('meta', 'payload'));
	}


	private
	function createCurlRequest() # : resource
	{
		$h = curl_init($this->apiUrlInventorySet());
		curl_setopt($h, CURLOPT_RETURNTRANSFER, true);

	# whaaat? the docs says its supposed tobe DIGEST auth, but no?
#		curl_setopt($h, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
		curl_setopt($h, CURLOPT_USERPWD, $this->apiAuth());

		curl_setopt($h, CURLOPT_POST, 0);
		curl_setopt($h, CURLOPT_POSTFIELDS, $this->requestContent());
		curl_setopt($h, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json' ]);
		curl_setopt($h, CURLOPT_RETURNTRANSFER, true);

		return $h;
	}


	function taskCurlHandle() # : resource
	{
		if ($this->curl_handle === null)
			$this->curl_handle = $this->createCurlRequest();
		return $this->curl_handle;
	}


	function taskRequestProcessed()
	{
		$O = json_decode($this->taskResponse()->body());
		if ($O->error !== null)
			throw new \RuntimeException('Master.GS API returned error ' .$O->error->message);

		// $this->productsBySku[$sku];

        $payload = $O->payload;

        foreach ($payload as $rcdSku => $rcdV) {
//            $quantities = (int)current($rcdV);  $node_id = (int)key($rcdV) ;

//            if((int)current($rcdV) > 0){
            if(isset($this->productsBySku[$rcdSku])){
                $product = $this->productsBySku[$rcdSku];
                $this->productService->setVariantAsSync($product['id']);
//            }
            }else{
                $this->log(sprintf('[Acc:%d] [WARNING] unknown SKU %s from master ',$this->Account->bc_account_pk, $rcdSku));
            }

        }

        //	$this->responseSanityCheck(); TODO FIXME
        //	$this->submitAudit(); TODO FIXME
	}


	function submitAudit()
	{
		$O = json_decode($this->taskResponse()->body());
		$map = [];
		foreach ($O->payload as $sku => $Rcd)
				# objects in format SKU->(string)master_node_id->(string)inventory_level
			foreach ($Rcd as $masterNode => $inventory_level)
					$map[$sku] = $inventory_level;

		//TODO if FIXME sku
		foreach ($this->Quantities->quantityRecords() as $rcd)
			\Audit::action('cronjob', $this->Account->user_id, [
				'type' => 'qty_download',
				'object' => 'product',
				'sku' => $rcd['sku'],
				'comment' => sprintf('sent to Master: %d, response from Master: %d',
					$rcd['inventory_level'], $map[$rcd['sku']]) ]);
	}


	function responseSanityCheck()
	{
		$O = json_decode($this->taskResponse()->body());
		$map = [];
		/*
		foreach ($O->payload as $sku => $Rcd)
				# objects in format SKU->(string)master_node_id->(string)inventory_level
			foreach ($Rcd as $masterNode => $inventory_level)
					$map[$sku] = $inventory_level;

		foreach ($this->Quantities->quantityRecords() as $rcd)
				# oddly enough, the inventory_level is returned as a string rather than int
				# probably case of PDO(MySQL) using the old 'pdo_mysql' driver
				# rather than the new 'pdo_mysqlnd'
			if ((int)$map[$rcd['sku']] !== $rcd['inventory_level'])
				throw new \RuntimeException(sprintf('Master.GS API returned wrong level for SKU %s (sent %d, got %d in response), possibly the SKU is not known on the Master.GS',
					$rcd['sku'], $rcd['inventory_level'], $map[$rcd['sku']]));
	*/
	}


	function hasNextTask() : bool
	{
		return false;
	}

	function nextTask() : \ConcurrentCurlPool\Task
	{
		return null;
	}
}
