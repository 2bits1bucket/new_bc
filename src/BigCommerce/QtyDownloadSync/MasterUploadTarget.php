<?php


namespace BigCommerce\QtyDownloadSync;


class MasterUploadTarget
{
	function uploadTask(\BCAccount $Account, QuantityStore $QuantityStore) : MasterUploadTask
	{
		$Quantities = $QuantityStore->QuantitiesForTransfer($Account);
		if ($Quantities->isEmptyP())
			return new \ConcurrentCurlPool\NullTask();
		else
			return new MasterUploadTask($this, $Account, $QuantityStore, $Quantities);
	}
}
