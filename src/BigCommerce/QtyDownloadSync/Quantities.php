<?php


namespace BigCommerce\QtyDownloadSync;

use BigCommerce\Models\BCAccount;

class Quantities
{
	protected $Account;
	protected $records;
	protected $data;

	function __construct(BCAccount $Account, array $data = null)
	{
		$this->Account = $Account;
		$this->data = $data;
	}

	function quantityRecords() : \Generator
	{
//		$Stats = new \Stats();

		if ($this->records !== null) {
			foreach ($this->records as $rcd)
				yield $rcd;
			return; }

		foreach ($this->data as $Product) {
			switch ($Product->inventory_tracking) {
			case 'none':
//				++$Stats->skipped_no_tracking;
				break;
			case 'product':
				if (count($Product->variants) < 1)
					printf('Warning: acc %d, product %d:"%s", has ZERO variants',
						$this->Account->bc_account_pk, $Product->id, $Product->sku );
				if (count($Product->variants) > 1)
					printf('Warning: acc %d, product %d:"%s", inventory tracking "%s", but has multiple (%d) variants',
						$this->Account->bc_account_pk, $Product->id, $Product->sku,
						$Product->inventory_tracking, count($Product->variants) );

					# technically there should be only one variant here...
					# but we have encountered products with 'product' inventory tracking, and yet
					# multiple variants
//				++$Stats->products;
				foreach ($Product->variants as $Variant) {
//					++$Stats->product_records;
					yield [
						'bc_account_pk' => $this->Account->bc_account_pk,
						'product_id' => $Product->id,
						'variant_id' => $Variant->id,
						'product_sku' => $this->nullOnEmpty($Product->sku),
						'variant_sku' => $this->nullOnEmpty($Variant->sku),
						'inventory_tracking' => $Product->inventory_tracking,
						'inventory_level' => $Variant->inventory_level,
						'date_modified' => $Product->date_modified,
					]; }
				break;
			case 'variant':
//				++$Stats->products;
				foreach ($Product->variants as $Variant) {
//					++$Stats->variant_records;
					yield [
						'bc_account_pk' => $this->Account->bc_account_pk,
						'product_id' => $Product->id,
						'variant_id' => $Variant->id,
						'product_sku' => $this->nullOnEmpty($Product->sku),
						'variant_sku' => $this->nullOnEmpty($Variant->sku),
						'inventory_tracking' => $Product->inventory_tracking,
						'inventory_level' => $Variant->inventory_level,
						'date_modified' => $Product->date_modified,
					]; }
				break;
			default:
				throw new \LogicException(); } }

#			echo $Stats->asHTML();
	}


	function fromRawRecords(array $records) : self
	{
		$ret = new self($this->Account);
		$ret->records = $records;
		return $ret;
	}


	function isEmptyP() : bool
	{
		return empty($this->records) && (empty($this->data) || empty($this->data->valid()));
	}


	function count() : int
	{
		if (!empty($this->data))
			throw new \LogicException('called ->count() on instance with a Generator; results are undefined');
		return count($this->records);
	}

    /**
     * bblib function
     *
     * @param $stringOrArray
     * @return array|null
     */
    private function nullOnEmpty($stringOrArray)
    {
        if (is_array($stringOrArray)) {
            $ret = [];
            foreach ($stringOrArray as $k => $v)
                if ($v === '')
                    $ret[$k] = null;
                else
                    $ret[$k] = $v;
            return $ret; }

        if ($stringOrArray === '')
            return null;
        else
            return $stringOrArray;
    }

}
