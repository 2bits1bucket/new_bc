<?php


namespace BigCommerce\QtyDownloadSync;

use BigCommerce\BcApiClient\PartialBCAPIv3;
use BigCommerce\Models\BCAccount;

class BCDownloadTask extends \ConcurrentCurlPool\MultiStepTask
{
	protected $Account;
	protected $curl_handle;

	protected $BCAPI;
	protected $Transfer;
	protected $Response;
	protected $nextPage;
	protected $page;
	protected $QuantityStore;
	protected $date_modified_range;
	protected $debug_tracing = false;

	const CUTOFF_GUARD_TIME_SECONDS = 1;
	const DEFAULT_LIMIT = 250;
		# apparently they count from 1, rather than from 0.
		# of course they made 0 work too...
	const START_PAGE = 1;


	function __construct(BCAccount $Account, QuantityStore $QuantityStore, int $continueAtPage = null)
	{
		$this->Account = $Account;
		$this->QuantityStore = $QuantityStore;
		$this->page = $continueAtPage ?? $QuantityStore->currentPage($Account) ?? static::START_PAGE;

		$this->date_modified_min = $QuantityStore->cutoffForAccount($Account);

		$isFirstTaskInChain = ($continueAtPage === null);

		if ($isFirstTaskInChain)
			$this->QuantityStore->markAsStarted($Account, $this->date_modified_min);

	}


	function __destruct()
	{
		curl_close($this->curl_handle);
	}


	function BCAccount() : BCAccount
	{
		return $this->Account;
	}


	private
	function generateApiRequest() # : resource
	{
		$this->BCAPI = new PartialBCAPIv3($this->Account);
		$options = [
			'include_fields' => 'inventory_level,inventory_tracking,date_modified,sku',
				# i don't know how to limit the fields returned in the 'variants' subsresource
			'include' => 'variants',

				# sort field & order is important for data consistency, see comment for this whole class
			'sort' => 'id',
			'direction' => 'asc',

			'page' => $this->page,
			'limit' => static::DEFAULT_LIMIT,

			'date_modified:min' => ($this->date_modified_min - static::CUTOFF_GUARD_TIME_SECONDS),
		];

		$this->Transfer = $this->BCAPI->getProducts($options);

		return $this->Transfer->curl_handle();
	}


	function taskCurlHandle() # : resource
	{
		if ($this->curl_handle === null) {
			$this->curl_handle = $this->generateApiRequest();
		}
		return $this->curl_handle;
	}


	function taskRequestProcessed()
	{
		$this->Response = $this->Transfer->BCAPIResponse();
		$this->nextPage = $this->Response->metaNextPage();

		$this->QuantityStore->markAsProgress($this->Account, $this->page, $this->date_modified_min);
	}


	function Quantities() : Quantities
	{
		if ($this->debug_tracing)
		printf('Account %d, got %d products',
			$this->Account->bc_account_pk, count($this->Response->data()) );

		return new Quantities($this->Account, $this->Response->data());
	}


	function hasNextTask() : bool
	{
		return ($this->nextPage !== null);
	}


	function nextTask() : \ConcurrentCurlPool\Task
	{
		return new static($this->Account, $this->QuantityStore, $this->nextPage);
	}


	function signalTaskChainCompleted()
	{
		$this->QuantityStore->markAsCompleted($this->Account);
	}
}
