<?php


namespace BigCommerce\QtyDownloadSync;

use BigCommerce\Models\BCAccount;
use \PDB\PDB;

class QuantityStore
{
	protected $db;

    function __construct(PDB $db)
	{
		$this->db = $db;
	}

	function storeQuantities(Quantities $Quantities) : Quantities
	{

		static $qInject;
		static $qCrop;

		if ($qInject === null)
			$qInject = $this->db->prepare('
				-- import the recently added products
			INSERT INTO qty_download_product_inventory
				(bc_account_pk, product_pk, product_variant_pk, product_id, variant_id,
					product_sku, variant_sku)

			SELECT bc_account_pk, product_pk, product_variant_pk,
				(product.bulk_data->>$$id$$)::INT, (product_variant.bulk_data->>$$id$$)::INT,
					product.sku, product_variant.bulk_data->>$$sku$$

			FROM product
			JOIN product_variant USING(product_pk)
			LEFT JOIN qty_download_product_inventory USING(bc_account_pk, product_pk, product_variant_pk)
			WHERE qty_download_product_inventory.bc_account_pk IS NULL ' );


		if ($qCrop === null)
			$qCrop = $this->db->prepare('
				-- thop he no-longer exsisting products
			WITH candidates AS (
				SELECT product_pk, product_variant_pk
				FROM qty_download_product_inventory
				LEFT JOIN product USING (bc_account_pk, product_pk)
				LEFT JOIN product_variant USING(product_pk, product_variant_pk)
				WHERE product_variant.product_pk IS NULL )

			DELETE FROM qty_download_product_inventory AS del_me
			USING candidates
			WHERE del_me.product_pk = candidates.product_pk
				AND del_me.product_variant_pk = candidates.product_variant_pk ' );


		$qCrop->execute();
		$qInject->execute();

        $sql = '
			INSERT INTO qty_download_product_inventory
				(bc_account_pk, product_id, variant_id, product_sku, variant_sku,
					inventory_tracking, inventory_level, date_modified, upload_stamp,
					updated_stamp)
			VALUES
				(:bc_account_pk, :product_id, :variant_id, :product_sku, :variant_sku,
					:inventory_tracking, :inventory_level, :date_modified, NULL,
					NOW() )
			ON CONFLICT
				(bc_account_pk, product_id, variant_id)
			DO UPDATE SET

				product_sku = EXCLUDED.product_sku,
				variant_sku = EXCLUDED.variant_sku,
				inventory_tracking = EXCLUDED.inventory_tracking,
				inventory_level = EXCLUDED.inventory_level,
				date_modified = EXCLUDED.date_modified,
				upload_stamp = EXCLUDED.upload_stamp,
				updated_stamp = EXCLUDED.updated_stamp ';


/*
        $sql = '
			UPDATE qty_download_product_inventory
			SET product_sku = :product_sku,
				variant_sku = :variant_sku,
				inventory_tracking = :inventory_tracking,
				inventory_level = :inventory_level,
				date_modified = :date_modified,
				upload_stamp = NULL,
				updated_stamp = updated_stamp
			WHERE bc_account_pk = :bc_account_pk
				AND product_id = :product_id
				AND variant_id = :variant_id ';
*/
		$q = $this->db->prepare($sql);

		$toReturn = [];
		foreach ($Quantities->quantityRecords() as $rcd) {
			$q->execute($rcd);
			if ($q->rowCount() > 0)
				$toReturn[] = $rcd; }

			# it is safe to return more data than necessary (i.e., also the quantities that were NOT changed
		return $Quantities->fromRawRecords($toReturn);
	}

	function cutoffForAccount(BCAccount $Account) : int
	{
		$rcd = $this->db->one('
			WITH data AS (
				SELECT :bc_account_pk::INT AS bc_account_pk
			),
			products_pick AS (
				SELECT bc_account_pk, extract(epoch from MAX(date_modified)) AS products_cutoff
				FROM data
				LEFT JOIN qty_download_product_inventory USING(bc_account_pk)
				GROUP BY bc_account_pk
			)

			SELECT (completion_stamp IS NOT NULL) AS previous_run_completed,
				used_cutoff,
				products_cutoff

			FROM data
			LEFT JOIN qty_download_meta USING(bc_account_pk)
			LEFT JOIN products_pick USING(bc_account_pk)',

			[ 'bc_account_pk' => $Account->bc_account_pk ] );


		if ($rcd['previous_run_completed'])
			$rcd['cutoff'] = $rcd['products_cutoff'];
		else
			$rcd['cutoff'] = $rcd['used_cutoff'];

		return $rcd['cutoff'] ?? 0;
	}


	function currentPage(BCAccount $Account) : ?int
	{
		$rcd = $this->db->one('
				-- MIN() to get NULL in case of no matching records
			SELECT MIN(page) AS page
			FROM qty_download_meta
			WHERE bc_account_pk = :bc_account_pk
				AND completion_stamp IS NULL',
			[ 'bc_account_pk' => $Account->bc_account_pk ] );

		return $rcd['page'];
	}


	function markAsStarted(BCAccount $Account, int $used_cutoff)
	{
		static $q;

		if ($q === null)
			$q = $this->db->prepare('
				INSERT INTO qty_download_meta
					(bc_account_pk, completion_stamp, page, used_cutoff)
				VALUES
					(:bc_account_pk, NULL, NULL, :used_cutoff)

				ON CONFLICT (bc_account_pk)
				DO UPDATE SET
					completion_stamp = EXCLUDED.completion_stamp,
					page = EXCLUDED.page,
					used_cutoff = EXCLUDED.used_cutoff' );

		$q->execute([ 'bc_account_pk' => $Account->bc_account_pk, 'used_cutoff' => $used_cutoff] );

		if ($q->rowCount() !== 1)
			printf('Warning: %s() expected exactly 1 changed row, got %d (bc_account_pk: %d)',
				__FUNCTION__, $q->rowCount(), $Account->bc_account_pk );
	}


	function markAsCompleted(BCAccount $Account)
	{
		static $q;

		if ($q === null)
			$q = $this->db->prepare('
				UPDATE qty_download_meta
				SET completion_stamp = NOW()
				WHERE bc_account_pk = :bc_account_pk' );

		$q->execute([ 'bc_account_pk' => $Account->bc_account_pk ] );

		if ($q->rowCount() !== 1)
			printf('Warning: %s() expected exactly 1 changed row, got %d (bc_account_pk: %d)',
				__FUNCTION__, $q->rowCount(), $Account->bc_account_pk );
	}


	function markAsProgress(BCAccount $Account, int $page, $used_cutoff)
	{
		static $q;

		if ($q === null)
			$q = $this->db->prepare('
				UPDATE qty_download_meta
				SET page = :page, used_cutoff = :used_cutoff
				WHERE bc_account_pk = :bc_account_pk' );


		$bc_account_pk = $Account->bc_account_pk;

		$q->execute(compact('page', 'used_cutoff', 'bc_account_pk'));

		if ($q->rowCount() !== 1)
			printf('Warning: %s() expected exactly 1 changed row, got %d (bc_account_pk: %d)',
				__FUNCTION__, $q->rowCount(), $Account->bc_account_pk );
	}


	function QuantitiesForTransfer(BCAccount $Account) : Quantities
	{
		static $q;

		if ($q === null)
            $q = $this->db->prepare('
				SELECT bc_account_pk, product_id, variant_id, product_sku, variant_sku,
					inventory_tracking, inventory_level, date_modified
				FROM qty_download_product_inventory
				WHERE bc_account_pk = :bc_account_pk
					AND upload_stamp IS NULL ' );

        $q->execute([ 'bc_account_pk' => $Account->bc_account_pk]);

		$Q = new Quantities($Account);

		return $Q->fromRawRecords($q->fetchAll());
	}


/*
	function setProductInventoryUploaded($rcd){

	    $sql =

        $q->execute([ 'bc_account_pk' => $Account->bc_account_pk]);

        $q = $this->db->prepare('
				SELECT bc_account_pk, product_id, variant_id, product_sku, variant_sku,
					inventory_tracking, inventory_level, date_modified
				FROM qty_download_product_inventory
				WHERE bc_account_pk = :bc_account_pk
					AND upload_stamp IS NULL ' );

        $q->execute([ 'bc_account_pk' => $Account->bc_account_pk]);
    }

*/

}
