<?php

namespace BigCommerce\QtyDownloadSync;

use BigCommerce\BcApiClient\PartialBCAPIv3;
use BigCommerce\Models\BCAccount;
use BigCommerce\Services\ProductService;
use ConcurrentCurlPool\Pool;

class BCProductDownloadTask extends \ConcurrentCurlPool\MultiStepTask
{
	protected $Account;
	protected $curl_handle;
	protected $totalPages = 1;

	protected $BCAPI;
	protected $Transfer;
	protected $Response;
	protected $nextPage;
	protected $page;
	protected $productService;
	protected $date_modified_range;
	protected $debug_tracing = false;
	protected $pool;
    protected $products;

    public $restartedCount = 0;
    private $maxRestartsCount = 10;

	const CUTOFF_GUARD_TIME_SECONDS = 1;
	const DEFAULT_LIMIT = 250;
		# apparently they count from 1, rather than from 0.
		# of course they made 0 work too...
	const START_PAGE = 1;

	function __construct(BCAccount $Account,
                         ProductService $productService,
                         Pool $pool,
                         $page = null)
	{
		$this->Account = $Account;
		$this->productService = $productService;
		$this->page = $page ?? static::START_PAGE;
        $this->pool = $pool;
	}

	function __destruct()
	{
	    if($this->curl_handle!=null)
		    curl_close($this->curl_handle);
	}


	function BCAccount() : BCAccount
	{
		return $this->Account;
	}

	private
	function generateApiRequest() # : resource
	{
		$this->BCAPI = new PartialBCAPIv3($this->Account);

		$options = [
//		    'include_fields' => 'inventory_level,inventory_tracking,date_modified,sku,images',
            'sort' => 'id',
            'direction' => 'asc',
		    'include' => 'variants, images',
		    'page' => $this->page,
		    'limit' => static::DEFAULT_LIMIT,
		];

		$this->Transfer = $this->BCAPI->getProducts($options);

		return $this->Transfer->curl_handle();
	}

	function taskCurlHandle() # : resource
	{
		if ($this->curl_handle === null) {
			$this->curl_handle = $this->generateApiRequest();
		}
		return $this->curl_handle;
	}

    function getPage(){
        return $this->page;
    }

	function taskRequestProcessed()
	{
		$this->Response = $this->Transfer->BCAPIResponse();

        $this->totalPages = $this->Response->meta()->pagination->total_pages;

		//we already have first page results take care of !
		if($this->page == 1 && $this->totalPages>1){

		    $this->log(
		        sprintf('[Acc:%d] Main ProductDownload Tasks found %d pages',
                    $this->BCAccount()->bc_account_pk,
                    $this->totalPages
                )
            );

		    for($i = 2; $i<=$this->totalPages; $i++){
		        $this->pool->addTask(new static($this->BCAccount(), $this->productService, $this->pool, $i));
            }

        }

        $this->products = $this->storeProducts();

        $this->log(
            sprintf('[Acc:%d] Main ProductDownload Tasks found %d products',
                $this->BCAccount()->bc_account_pk,
                count($this->products)
            )
        );

	}

    /**
     * @return array - of saved products
     */
	function storeProducts()
	{

	    $acc_id = $this->Account->bc_account_pk;

        $products = [];

        foreach ($this->Response->data() as $product){
            $saved_product = $this->productService->persistProduct($product, $acc_id, $this->page);
            $products[] = $saved_product;
        }

		return $products;
	}

    /**
     * https://developer.bigcommerce.com/api/#api-status-codes
     * @param \Throwable $e
     */
    function taskExceptionHandler(\Throwable $e)
    {

        $msg = '[Acc:%d][TASK EXCEPTION] %s ';

        $this->log(sprintf($msg, $this->Account->bc_account_pk, $e->getMessage(), $this->page));

        //request time outed
        if($e->getCode() == 408){

            if($this->restartedCount < $this->maxRestartsCount){

                $task = new static($this->BCAccount(), $this->productService, $this->pool, $this->page);
                $task->restartedCount = $this->restartedCount++;
                $this->pool->addTask($task);

                $this->log(sprintf('[Acc:%d] Restart task for page: %d',
                    $this->Account->bc_account_pk, $this->page));

            }else{
                $this->log(sprintf('[Acc:%d] [ERROR] Failed restart task %d times for page %d ',
                    $this->Account->bc_account_pk, $this->maxRestartsCount, $this->page));
            }

        }else{
            if(isset($e->xdebug_message)){
                echo $e->xdebug_message;
            }else{
                echo $e->getFile().PHP_EOL;
                echo $e->getTraceAsString();
            }
        }

    }

	function hasNextTask() : bool
	{
		return false;
	}


	function nextTask() : \ConcurrentCurlPool\Task
	{
		return null;
	}

}
