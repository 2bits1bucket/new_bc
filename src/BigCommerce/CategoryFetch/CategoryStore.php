<?php


namespace BigCommerce\CategoryFetch;


use BigCommerce\Models\BCAccount;
use PDB\PDB;

class CategoryStore
{
	protected $db;

	function __construct(PDB $db)
	{
		$this->db = $db;
	}


	function accountsForRefresh() : \Generator
	{
		$a = $this->db->many('
			SELECT bc_account.*
			FROM bc_account
			LEFT JOIN category_tree USING(bc_account_pk)
			WHERE (category_tree.refresh_stamp < (NOW() - INTERVAL $$ 12 HOUR $$))
				OR category_tree.refresh_stamp IS NULL
			GROUP BY bc_account_pk
		');

		foreach ($a as $rcd)
			yield new BCAccount($rcd);
	}


	private
	function _storeTree(CategoryTree $Tree)
	{
		static $insertQ;

		if ($insertQ === null)
			$insertQ = $this->db->prepare('
				INSERT INTO tmp_category_tree
					(bc_account_pk, category_id, parent_id, name, refresh_stamp)
				VALUES
					(:bc_account_pk, :category_id, :parent_id, :name, NOW()) ' );

		$this->db->exec('
			CREATE TEMPORARY TABLE IF NOT EXISTS tmp_category_tree
			( LIKE category_tree)
			ON COMMIT DELETE ROWS');

		foreach ($Tree as $rcd){
            $ret = $insertQ->execute($rcd);
        }

		# UPSERT
		$q0 = $this->db->exec('
			INSERT INTO category_tree
				(bc_account_pk, category_id, parent_id, name, refresh_stamp)
			SELECT bc_account_pk, category_id, parent_id, name, refresh_stamp
			FROM tmp_category_tree
			ON CONFLICT (bc_account_pk, category_id)
			DO UPDATE SET
				name = EXCLUDED.name,
				refresh_stamp = EXCLUDED.refresh_stamp' );

		//  TODO UPDATE them

/*
		$q1 = $this->db->exec('
			WITH for_delete AS (
				SELECT bc_account_pk, category_id
				FROM category_tree
				LEFT JOIN tmp_category_tree USING(bc_account_pk, category_id)
				WHERE tmp_category_tree.bc_account_pk IS NULL )

			DELETE FROM product_category AS T
			USING for_delete AS F
			WHERE (T.bc_account_pk, T.category_id) IN (F) ');


        $q2 = $this->db->exec('
            WITH for_delete AS (
                SELECT bc_account_pk, category_id
                FROM category_tree
                LEFT JOIN tmp_category_tree USING(bc_account_pk, category_id)
                WHERE tmp_category_tree.bc_account_pk IS NULL )

            DELETE FROM category_pathname AS T
            USING for_delete AS F
            WHERE (T.bc_account_pk, T.category_id) IN (F) ');


        $q3 = $this->db->exec('
            WITH for_delete AS (
                SELECT bc_account_pk, category_id
                FROM category_tree
                LEFT JOIN tmp_category_tree USING(bc_account_pk, category_id)
                WHERE tmp_category_tree.bc_account_pk IS NULL )

            DELETE FROM category_tree AS T
            USING for_delete AS F
            WHERE (T.bc_account_pk, T.category_id) IN (F) ');
*/
	}


	private
	function _storeTreeOLD(CategoryTree $Tree)
	{
		static $qInsertA;
		static $qPopulateB;
		static $qPurgeA;
		static $qPurgeB;

		if ($qInsertA === null)
			$qInsertA = $this->db->prepare('
				INSERT INTO category_tree
					(bc_account_pk, category_id, parent_id, name)
				VALUES
					(:bc_account_pk, :category_id, :parent_id, :name) ' );

		if ($qPopulateB === null)
			$qPopulateB = $this->db->prepare('
				WITH RECURSIVE
					data AS (
						 SELECT :bc_account_pk::INT AS bc_account_pk ),

					t(bc_account_pk, category_id, pathname, parent_id) AS (
							SELECT bc_account_pk, category_id, name AS pathname, category_id AS parent_id
							FROM data
							JOIN category_tree USING(bc_account_pk)
							WHERE parent_id IS NULL
						UNION ALL
							SELECT bc_account_pk, category_tree.category_id,  pathname || $$ > $$ || name AS pathname, category_tree.category_id AS parent_id
							FROM data
							JOIN category_tree USING(bc_account_pk)
							JOIN t USING(bc_account_pk, parent_id) )

				INSERT INTO category_pathname
					(bc_account_pk, category_id, pathname)
				SELECT bc_account_pk, category_id, pathname
				FROM t ' );

		if ($qPurgeA === null)
			$qPurgeA = $this->db->prepare('
				DELETE FROM category_pathname
				WHERE bc_account_pk = :bc_account_pk ' );
		if ($qPurgeB === null)
			$qPurgeB = $this->db->prepare('
				DELETE FROM category_tree
				WHERE bc_account_pk = :bc_account_pk ' );

		$qPurgeA->execute([ 'bc_account_pk' => $Tree->BCAccount()->bc_account_pk ]);
		$qPurgeB->execute([ 'bc_account_pk' => $Tree->BCAccount()->bc_account_pk ]);

		foreach ($Tree as $rcd)
			$qInsertA->execute($rcd);

		$qPopulateB->execute([ 'bc_account_pk' => $Tree->BCAccount()->bc_account_pk ]);
	}

	function storeTree(CategoryTree $Tree)
	{
		try {
			$this->db->beginTransaction();
			$this->_storeTree($Tree);
			$this->db->commit();
		} catch (\Throwable $e) {
			$this->db->rollback();
			throw $e;
		}
	}

}

