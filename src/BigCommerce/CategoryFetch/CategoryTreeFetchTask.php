<?php


namespace BigCommerce\CategoryFetch;


use BigCommerce\BcApiClient\PartialBCAPIv3;
use BigCommerce\Models\BCAccount;

class CategoryTreeFetchTask extends \ConcurrentCurlPool\Task
{
	protected $Account;
	protected $curl_handle;
	protected $API;
	protected $Transfer;
	protected $Response;


	function __construct(BCAccount $Account)
	{
		$this->Account = $Account;
	}


	private
	function generateCurlRequest() # : resource
	{
		$this->API = new PartialBCAPIv3($this->Account);

		$this->Transfer = $this->API->getCategoryTree();

		return $this->Transfer->curl_handle();
	}


	function taskCurlHandle() # : resource
	{
		if ($this->curl_handle === null)
			$this->curl_handle = $this->generateCurlRequest();
		return $this->curl_handle;
	}


	function taskRequestProcessed()
	{
		$this->Response = $this->Transfer->BCAPIResponse();
	}


	function CategoryTree()// : CategoryTree
	{
        $data = json_decode($this->Response->body(), $assoc = true)['data'];

        if($data !=null){
           return new CategoryTree($this->Account, $data);
        }

	    return null;
	}

	function getAccount(){
	    return $this->Account;
    }
}
