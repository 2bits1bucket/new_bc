<?php


namespace BigCommerce\CategoryFetch;


use BigCommerce\Models\BCAccount;

class CategoryTree implements \IteratorAggregate
{
	protected $Account;
	protected $categories;


	function __construct(BCAccount $Account, array $categories)
	{
		$this->Account = $Account;
		$this->categories = $categories;
	}


	private
	function a(array $a) : \Generator
	{
		foreach ($a as $rcd) {
			yield [
				'bc_account_pk' => $this->Account->bc_account_pk,
				'category_id' => $rcd['id'],
				'parent_id' => ($rcd['parent_id'] === 0 ) ? null : $rcd['parent_id'],
				'name' => $rcd['name'] ];

			yield from $this->a($rcd['children']); }
	}


	function categoryIterator() : \Generator
	{
		yield from $this->a($this->categories);
	}


	function BCAccount() : \BCAccount
	{
		return $this->Account;
	}


	function getIterator() : \Generator
	{
		return $this->categoryIterator();
	}
}
