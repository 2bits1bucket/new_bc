<?php

namespace BigCommerce\Services;

use \PDB\PDB;

class ProductService {

    private static $instance = null;

    private $deleteOldVariants = false;

    private $db;

    private $product_data_keys = [
        'id', 'bc_account_pk', 'bc_product_id','sku', 'name',
        'inventory_tracking','inventory_level','created_at',
        'updated_at','date_modified','data_bulk','main_variant_id',
        'master_sync', 'master_sync_at'
    ];

    private $variant_data_keys = [
        'id', 'product_id', 'bc_product_id', 'variant_id',
        'sku', 'data_bulk', 'created_at', 'updated_at', 'inventory_level', 'master_sync'
    ];

    private $product_category_keys = [
        'product_id', 'category_id', 'bc_account_pk'
    ];

    private function __construct(PDB $db){
        $this->db = $db;
    }

    public static function getInstance($db) {
        if (!isset(self::$instance)) {
            self::$instance = new ProductService($db);
        }
        return self::$instance;
    }

    private function updateProduct($product, $id){

        $data = [
            'sku' => $product->sku,
            'inventory_tracking' => $product->inventory_tracking,
            'inventory_level' => $product->inventory_level,
            'date_modified' => $product->date_modified,
            'data_bulk' => json_encode($product),
            'id' => $id
        ];

        $sql = ' UPDATE bc_product SET 
                  sku = :sku, 
                  inventory_tracking = :inventory_tracking, 
                  inventory_level = :inventory_level, 
                  date_modified = :date_modified, 
                  data_bulk = :data_bulk,
                  updated_at = NOW(),
                  master_sync = false
                WHERE id = :id RETURNING * ';

        $q = $this->db->prepare($sql);

        $q->execute($data);

        return $q->fetch();
    }

    private function insertProduct($product, $acc_id){

        $data = [
            'bc_account_pk' => $acc_id,
            'bc_product_id' => $product->id,
            'sku' => $product->sku,
            'name' => $product->name,
            'inventory_tracking' => $product->inventory_tracking,
            'inventory_level' => $product->inventory_level,
            'date_modified' => $product->date_modified,
            'data_bulk' => json_encode($product)
        ];

        $sql = ' INSERT INTO bc_product (bc_account_pk, bc_product_id, sku, name, inventory_tracking, inventory_level, date_modified, data_bulk) 
                VALUES (:bc_account_pk, :bc_product_id, :sku, :name, :inventory_tracking, :inventory_level, :date_modified, :data_bulk) RETURNING * ';

        $q = $this->db->prepare($sql);

        $q->execute($data);

        return $q->fetch();
    }

    private function updateVariant($variant, $id){

        $data = [
            'sku' => $variant->sku,
            'data_bulk' => json_encode($variant),
            'inventory_level' => $variant->inventory_level,
            'id' => $id
        ];

        $sql = ' UPDATE bc_product_variant SET 
                    sku = :sku, 
                    data_bulk = :data_bulk, 
                    updated_at = NOW(), 
                    inventory_level = :inventory_level,
                    master_sync = false
                WHERE id = :id RETURNING * ';

        $q = $this->db->prepare($sql);

        $q->execute($data);

        return $q->fetch();
    }

    public function setVariantAsSync($id){

        $sql = ' UPDATE bc_product_variant SET 
                    updated_at = NOW(), 
                    master_sync = true
                WHERE id = :id RETURNING * ';

        $q = $this->db->prepare($sql);

        $q->execute(['id' => $id]);

        return $q->fetch();
    }

    private function insertVariant($variant, $product_id){

        $data = [
            'product_id' => $product_id,
            'bc_product_id' => $variant->product_id,
            'variant_id' => $variant->id,
            'sku' => $variant->sku,
            'data_bulk' =>json_encode($variant),
            'inventory_level' => $variant->inventory_level
        ];

        $sql = ' INSERT INTO bc_product_variant (product_id, bc_product_id, variant_id, sku, data_bulk, inventory_level) 
                VALUES (:product_id, :bc_product_id, :variant_id, :sku, :data_bulk, :inventory_level) RETURNING * ';

        $q = $this->db->prepare($sql);

        $q->execute($data);

        return $q->fetch();
    }

    private function unsetCategories($product_id){

        $sql = ' DELETE FROM bc_product_category WHERE product_id = :product_id; ';

        $q = $this->db->prepare($sql);

        $q->execute(['product_id' => $product_id]);

    }

    private function unsetVariant($id){

        $sql = ' DELETE FROM bc_product_variant WHERE id = :id; ';

        $q = $this->db->prepare($sql);

        $q->execute(['id' => $id]);
    }

    private function setProductCategory($acc_id, $product_id, $category_id){

        $sql = ' INSERT INTO bc_product_category ( product_id, category_id, bc_account_pk) 
        VALUES ( :product_id, :category_id, :bc_account_pk ); ';

        $q = $this->db->prepare($sql);

        $q->execute(['product_id' => $product_id,
                    'category_id' => $category_id,
                    'bc_account_pk' => $acc_id]);

    }

    public function persistProduct($product, $acc_id, $page) // : BcProduct
    {

        $saved = $this->byBcIdAndAccId($product->id, $acc_id);

        if($saved!=null){

            $saved = $this->updateProduct($product, $saved['id']);

            $old_variants = $this->getVariantsByProductId($saved['id']);

            foreach ((array)$product->variants as $variant) {
                if(isset($old_variants[$variant->id])){
                    $this->updateVariant($variant, $old_variants[$variant->id]['id']);
                    unset($old_variants[$variant->id]);
                }else{
                    $this->insertVariant($variant, $saved['id']);
                }
            }

            if($this->deleteOldVariants && count($old_variants)>0){
                foreach ($old_variants as $oV){
                    $this->unsetVariant($oV['id']);
                }
            }

            $this->unsetCategories($saved['id']);

        } else {

            $saved = $this->insertProduct($product, $acc_id);

            foreach ((array)$product->variants as $variant) {
                $this->insertVariant($variant, $saved['id']);
            }

        }

        if(isset($product->categories)){
            foreach ((array)$product->categories as $category_id) {
                $this->setProductCategory($acc_id, $saved['id'], $category_id);
            }
        }

        return $saved;
    }

    public function byBcIdAndAccId($bc_id, $acc_id){

        try {

            $sql = ' SELECT * FROM bc_product WHERE bc_product_id = :bc_product_id AND
                      bc_account_pk = :bc_account_pk LIMIT 1; ';

            $data = ['bc_product_id' => $bc_id, 'bc_account_pk' => $acc_id];

            $rcd = $this->db->one($sql, $data);

            return $rcd;
        } catch (\PDB\RowNotFoundException $exception){
            return null;
        }

    }

    public function findProductById($id){

        try {

            $sql = ' SELECT * FROM bc_product WHERE id = :id LIMIT 1; ';

            $data = ['id' => $id];

            $rcd = $this->db->one($sql, $data);

            return $rcd;
        } catch (\PDB\RowNotFoundException $exception){
            return null;
        }
    }

    public function findProductBySkuAndAccId($sku, $acc_id){
        try {

            $sql = ' SELECT * FROM bc_product WHERE sku = :sku AND
                      bc_account_pk = :bc_account_pk LIMIT 1; ';

            $data = ['sku' => $sku, 'bc_account_pk' => $acc_id];

            $rcd = $this->db->one($sql, $data);

            return $rcd;
        } catch (\PDB\RowNotFoundException $exception){
            return null;
        }
    }


    /**
     * @param $product_id
     * @return array - key is bc variant id
     */
    public function getVariantsByProductId($product_id){

        $sql = ' SELECT * FROM bc_product_variant WHERE product_id = :product_id; ';

        $data = ['product_id' => $product_id];

        $rcd = $this->db->many($sql, $data);

        $data = [];
        foreach ((array)$rcd as $r){
            $data[$r['variant_id']] = $r;
        }

        return $data;

    }

    public function findVariantsForSyncByAcc($acc_id){

        $sql = ' SELECT V.*, P.inventory_tracking, P.bc_account_pk, 
                        P.sku AS prod_sku, P.inventory_level AS prod_inv_lvl 
                 FROM bc_product_variant AS V 
                 LEFT JOIN bc_product AS P ON V.product_id = P.id 
                 WHERE 
                  bc_account_pk = :bc_account_pk AND 
                  V.master_sync = false AND
                  P.inventory_tracking != :inventory_tracking; ';

        $rcd = $this->db->many($sql, ['bc_account_pk' => $acc_id, 'inventory_tracking'=>'none']);

        $data = [];
        foreach ((array)$rcd as $r){
            $data[] = $r;
        }

        return $data;
    }

    // TODO pagination / LIMIT
    public function findVariantsByAcc($acc_id){

        $sql = ' SELECT V.*, P.inventory_tracking, P.bc_account_pk, 
                        P.sku AS prod_sku, P.inventory_level AS prod_inv_lvl 
                 FROM bc_product_variant AS V 
                 LEFT JOIN bc_product AS P ON V.product_id = P.id 
                 WHERE 
                  bc_account_pk = :bc_account_pk AND 
                  P.inventory_tracking != :inventory_tracking; ';

        $rcd = $this->db->many($sql, ['bc_account_pk' => $acc_id, 'inventory_tracking'=>'none']);

        $data = [];
        foreach ((array)$rcd as $r){
            $data[] = $r;
        }

        return $data;
    }

    public function findVariantsBySkuAndAcc($sku, $acc_id){
        try {

            $sql = ' SELECT V.*, P.inventory_tracking, P.bc_account_pk, 
                            P.sku AS prod_sku, P.inventory_level AS prod_inv_lvl 
                     FROM bc_product_variant AS V 
                     LEFT JOIN bc_product AS P ON V.product_id = P.id 
                     WHERE bc_account_pk = :bc_account_pk AND V.sku = :sku ;  ';

            $data = ['sku' => $sku, 'bc_account_pk' =>$acc_id];

            $rcd = $this->db->one($sql, $data);

            return $rcd;
        } catch (\PDB\RowNotFoundException $exception){
            return null;
        }
    }


    public function getLastModifyDateByAcc(){

        $sql = ' SELECT 
                  bc_account_pk, 
                  MAX(date_modified) AS last_product_date 
                FROM bc_product 
                GROUP BY bc_account_pk ';

        $rcd = $this->db->many($sql);

        $data = [];
        foreach ((array)$rcd as $r){
            $data[$r['bc_account_pk']] = $r['last_product_date'];
        }

        return $data;
    }

    private function objFromRequest($Product){ // as object ?

        $data = [
            'bc_product_id' => $Product->id,
            'sku' => $Product->sku,
            'name' => $Product->name,
            'inventory_tracking' => $Product->inventory_tracking,
            'inventory_level' => $Product->inventory_level,
            'date_modified' => $Product->date_modified,
            'data_bulk' => json_encode($Product),
            'categories' => $Product->categories
            ];

        $variants = [];

        switch ($Product->inventory_tracking) {
            case 'none':
                //  skipped_no_tracking; -vo variants
                break;
            case 'product':
                /*
                if (count($Product->variants) < 1)
                    printf('Warning: acc %d, product %d:"%s", has ZERO variants',
                        $this->Account->bc_account_pk, $Product->id, $Product->sku );
                if (count($Product->variants) > 1)
                    printf('Warning: acc %d, product %d:"%s", inventory tracking "%s", but has multiple (%d) variants',
                        $this->Account->bc_account_pk, $Product->id, $Product->sku,
                        $Product->inventory_tracking, count($Product->variants) );

                # technically there should be only one variant here...
                # but we have encountered products with 'product' inventory tracking, and yet
                # multiple variants
                */
                foreach ($Product->variants as $Variant) {
                    $variants[] =[
                        'bc_product_id' => $Product->id,
                        'variant_id' => $Variant->id,
                        'sku' => $Variant->sku,
                        'data_bulk' => json_encode($Variant),
                        'inventory_level' => $Variant->inventory_level
                    ];
                }
                break;
            case 'variant':
                foreach ($Product->variants as $Variant) {
                    $variants[] =[
                        'bc_product_id' => $Product->id,
                        'variant_id' => $Variant->id,
                        'sku' => $Variant->sku,
                        'data_bulk' => json_encode($Variant),
                        'inventory_level' => $Variant->inventory_level
                    ];
                }
                break;
            default:
                throw new \LogicException();
        }

        $data['variants'] = $variants;

        return $data;
    }



}