<?php

namespace BigCommerce\Services\Csv;

class JetCsvProductExporter extends CsvProductExporter
{
	function __construct(/*args*/)
	{
		call_user_func_array('parent::__construct', func_get_args());

		$this->variantHandler = new JetVariantHandler();
	}


	function outputHeaders()
	{
		$rcd = [ 'SKU', 'productName', 'Parent SKU' ];
		$this->outputCsvRecord($rcd);
	}


	protected
	function csvRcd(array $product_rcd) : array
	{
		return [ $product_rcd['sku'], $product_rcd['name'], $product_rcd['Parent SKU'] ];
	}


	function fileName() : string
	{
		return sprintf('BC-products-JET-%s.csv', date('Y-m-d-H-i'));
	}
}
