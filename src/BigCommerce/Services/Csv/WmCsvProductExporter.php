<?php
namespace BigCommerce\Services\Csv;

class WmCsvProductExporter extends CsvProductExporter
{
	function outputHeaders()
	{
		$rcd = [ 'Merchant SKU', 'Product title' ];
		$this->outputCsvRecord($rcd);
	}


	protected
	function csvRcd(array $product_rcd) : array
	{
		return [ $product_rcd['sku'], $product_rcd['name'] ];
	}


	function fileName() : string
	{
		return sprintf('BC-products-WM-%s.csv', date('Y-m-d-H-i'));
	}
}
