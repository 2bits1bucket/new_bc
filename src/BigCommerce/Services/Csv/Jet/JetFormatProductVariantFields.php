<?php


namespace ProductExporter;


use ProductExporter\FieldTransform as T;


class JetFormatProductVariantFields extends FormatProductVariantFields
{
	function asTabularRecords() : \Traversable
	{
		foreach ($this->fieldsToOutput() as $k => $v)
			yield $k;
	}


	function XfieldsToOutput() : \Traversable	#<OutputField>
	{
		$a = [ 'foo' => 'bar', 'aa' => 'bb' ];
		foreach ($a as $k => $v)
			yield $k => new FieldTransform\ConstValue($v);
	}


	function tabularNames() : array
	{
		return $this->Fields->tabularNames();
	}


	private
	function _bcImage(InputProductOrVariant $P, string $name, int $n, string $sscanfAdd = null) : T\FieldTransform
	{
		if ($sscanfAdd !== null) {
			$add = 0;
			$v = sscanf($name, $sscanfAdd, $add);
			if ($v !== 1)
				throw new \LogicException(sprintf('expected exactly one conversion, got %d, for "%s"', $v, $sscanfAdd));
			$n += $add; }
		return new T\ObjectPath($P, [ 'product_image_data', $n, 'url_zoom' ]);
	}


	private
	function _category(InputProductOrVariant $P, string $name, int $n, string $sscanfAdd = null)
	{
		if ($sscanfAdd !== null) {
			$add = 0;
			$v = sscanf($name, $sscanfAdd, $add);
			if ($v !== 1)
				throw new \LogicException(sprintf('expected exactly one conversion, got %d, for "%s"', $v, $sscanfAdd));
			$n += $add; }
		return new T\ObjectPath($P, [ 'product_category_pathname_data', $n, 'pathname' ]);
	}


	private
	function _tabularFieldValue(string $name, InputProductVariant $V, InputProduct $P) : T\FieldTransform
	{
		switch ($name) {
		case 'Merchant SKU':
			return new T\CopyText($V, 'sku');
		case 'Product title':
			return new T\CopyText($P, 'name');
		case 'Parent SKU':
			return new T\ConstValue($V->getRelationship('DesignatedParent'));
		case 'Price':
			return new T\InheritOnNullOrEmptyString(
				new T\CopyText($V, 'price'),
				new T\CopyText($P, 'price') );
		case 'Description':
			return new T\CopyText($P, 'description');
		case 'UPC code':
			return new T\CopyText($V, 'upc');
		case 'GTIN-14 code':
			return new T\CopyText($V, 'gtin');
		case 'UPC code':
			return new T\CopyText($V, 'upc');

		case 'Product tax code':
			return new T\CopyText($P, 'product_tax_code');

		case 'Shipping weight (lbs)':
			return new T\InheritOnNullOrEmptyString(
				new T\CopyText($V, 'weight'),
				new T\CopyText($P, 'weight') );
		case 'SKU width (inches)':
			return new T\InheritOnNullOrEmptyString(
				new T\CopyText($V, 'width'),
				new T\CopyText($P, 'width') );
		case 'SKU length (inches)':
			return new T\InheritOnNullOrEmptyString(
				new T\CopyText($V, 'depth'),
				new T\CopyText($P, 'depth') );
		case 'SKU height (inches)':
			return new T\InheritOnNullOrEmptyString(
				new T\CopyText($V, 'height'),
				new T\CopyText($P, 'height') );

		case 'Category 1':
		case 'Category 2':
		case 'Category 3':
		case 'Category 4':
		case 'Category 5':
			return new T\OnMissingFieldOffset(
				$this->_category($P, $name, -1, 'Category %d'),
				new T\ConstValue(null) );

		case 'Main image':
			return new T\OnMissingFieldOffset(
				$this->_bcImage($P, $name, 0),
				new T\ConstValue(null) );
		case 'Alternate image 1':
		case 'Alternate image 2':
		case 'Alternate image 3':
		case 'Alternate image 4':
		case 'Alternate image 5':
		case 'Alternate image 6':
		case 'Alternate image 7':
		case 'Alternate image 8':
			return new T\OnMissingFieldOffset(
				$this->_bcImage($P, $name, 0, 'Alternate image %d'),
				new T\ConstValue(null) );
		default:
			throw new \Exception('field not supported: \'' .$name .'\''); }
	}


	function tabularFieldValue(string $name, InputProductVariant $IPV) : T\FieldTransform
	{
		$ret = $this->_tabularFieldValue($name, $IPV, $IPV->Product());
		return $ret;
	}
}
