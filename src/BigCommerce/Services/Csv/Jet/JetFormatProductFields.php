<?php


namespace ProductExporter;


class JetFormatProductFields extends FormatProductFields
{

	function asTabularRecords() : \Traversable
	{
		yield $this->tabularNames();
	}


	function VariantFields() : FormatProductVariantFields
	{
		return new JetFormatProductVariantFields($this);
	}


	function tabularNames() : array
	{
		return [
            'Merchant SKU',
            'Product title',
            'Parent SKU',
            'Price',
            /*'Multipack quantity',
            /*'Category path',
            'Brand',
            'Manufacturer',
            'Mfr Part Number',
            'Country of origin',	*/
            'Description',
            'Product tax code',
            'Shipping weight (lbs)',
            /*'Package length (inches)',
            'Package width (inches)',
            'Package height (inches)',*/
            'SKU length (inches)',
            'SKU width (inches)',
            'SKU height (inches)',
            'UPC code',
            'GTIN-14 code',
            /*'ISBN-10 code',
            'ISBN-13 code'
            'EAN code',*/

            'Category 1',
            'Category 2',
            'Category 3',
            'Category 4',
            'Category 5',

            /* 'ASIN',

            /*'Proposition 65',
            'Cautionary statements',
            'Legal disclaimer',
            'Safety warning',
            'Bullet 1',
            'Bullet 2',
            'Bullet 3',
            'Bullet 4',
            'Bullet 5',*/
            'Main image',
            /*'Swatch Image',*/
            'Alternate image 1',
            'Alternate image 2',
            'Alternate image 3',
            'Alternate image 4',
            'Alternate image 5',
            'Alternate image 6',
            'Alternate image 7',
            'Alternate image 8',
            /*'Amazon SKU',
            'MAP Price',
            'MAP Implementation',
            'MSRP',
            'Days to Return',
            'Return Address 1',
            'Return Address 2',
            'Return City',
            'Return State',
            'Return Zip Code',
            /*
            'Attr 19: Format',
            'Attr 30: Language',
            'Attr 50: Size',
            'Attr 119: Color',
            'Attr 671808244348869: Book Box Type',
            'Attr 270: Award Winners',
            'Attr 595037652856550: Smart Home Category',
            'Attr 282: Certifications',
            'Attr 45: Width',
            'Attr 266: Installation Type',
            'Attr 592474252399254: Warranty',
            'Attr 565622408707200: Model Number',
            'Attr 513728829016090: Dimensions',
            'Attr 411340040469828: Weight',
            'Attr 504389797806388: Product Features',
            'Attr 171: Make',
            'Attr 172: Model',
            'Attr 173: Year',
            'Attr 2: Color',
            'Attr 44: Power Source',
            'Attr 36: Form',
            'Attr 310: Skin Type',
            'Attr 118: Fragrance',
            'Attr 37: Shop by Feature',
            'Attr 31: Age Range',
            'Attr 34: Gender',
            'Attr 54: Optical Zoom',
            'Attr 128: Megapixels',
            'Attr 209726597972336: Camera Type',
            'Attr 601254359818598: Min. Focal Length',
            'Attr 597764850475021: Removable Media',
            'Attr 597764851521216: AF Points',
            'Attr 597764849712699: Focus Type',
            'Attr 601258659392716: Dimensions',
            'Attr 597764850374026: Port Type',
            'Attr 597764849800419: Weight',
            'Attr 601221408238538: Camera Family',
            'Attr 601256380626579: Max. Focal Length',
            'Attr 597764851057733: Depth of Waterproof',
            'Attr 597764849629757: Burst Mode (Max FPS)',
            'Attr 597764850765617: Video Resolution',
            'Attr 597764851189583: Image Processor',
            'Attr 597764849980692: Screen Resolution',
            'Attr 597764849533355: Package Type',
            'Attr 597764850673488: Sensor Type',
            'Attr 597764850061990: Screen Size',
            'Attr 596945388683843: Model',
            'Attr 597764849882613: Battery Type',
            'Attr 597764850196470: Screen Type',
            'Attr 597764851366207: Max ISO',
            'Attr 597764850284746: Veiwfinder Type',
            'Attr 597764849407032: Features',
            'Attr 597764850569828: Sensor Size',
            'Attr 513842408435658: Pattern',
            'Attr 513836127554920: Style Number',
            'Attr 513087275890107: Stylistic Features',
            'Attr 513030905061412: Additional Sizes',
            'Attr 513050456804286: Fabric Technology',
            'Attr 5: Import Designation',
            'Attr 513088167158834: Pocket Descriptions',
            'Attr 513832200813586: Material Type',
            'Attr 513802469470713: Special Features',
            'Attr 105: Additional Sizes',
            'Attr 278008171333939: Care Instructions',
            'Attr 513833092405444: Apparel Composition',
            'Attr 513824787181174: Apparel Style',
            'Attr 513098696886945: Leg Style',
            'Attr 513044444979655: Fit Type',
            'Attr 1: Country of Origin',
            'Attr 513051828099915: Closure Type',
            'Attr 32: Age Group',
            'Attr 33: Target Audience',
            'Attr 586408038921029: Saddle Cover',
            'Attr 607929196044506: Recommended Use',
            'Attr 586513884992152: Number of Speeds',
            'Attr 607931293964746: Max Cog Size',
            'Attr 589796986617542: Brake System',
            'Attr 595831443827034: Material',
            'Attr 578718386040009: Pedals',
            'Attr 607291449424483: Cleat Design',
            'Attr 586330227216508: Travel',
            'Attr 596007346340882: Size',
            'Attr 595843169825490: Width',
            'Attr 595843455148966: Length',
            'Attr 595827417425936: Dimensions',
            'Attr 607930586354508: Derailleur Type',
            'Attr 595824793467416: Weight',
            'Attr 607930983354256: Cage Length',
            'Attr 129: Furniture Finish',
            'Search Terms',
            'JET Status',	*/
		];
	}
}
