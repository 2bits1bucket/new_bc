<?php


namespace ProductExporter;


class JetFormat extends Format implements \IteratorAggregate
{
	function fileNamePart() : string
	{
		return 'JET';
	}


	function asTabularRecords() : \Traversable
	{
		foreach ($this->Products as $P) {
			assert($P instanceof InputProduct);
#			$OP = new JetOutputProduct($this->Fields(), $P);

			$DesignatedParentSku = null;
			foreach ($P->variants() as $n => $V) {
				$OV = new JetOutputProductVariant($this->Fields()->VariantFields(), $V);
				$OV->InputProductVariant()->setRelationship('DesignatedParent', $DesignatedParentSku);
				yield $OV;
				if ($DesignatedParentSku === null)
					$DesignatedParentSku = $V->fieldValue('sku'); } }
	}


	function getIterator() : \Traversable
	{
		return $this->generator();
	}


	function Fields() : FormatProductFields
	{
		return new JetFormatProductFields();
	}
}
