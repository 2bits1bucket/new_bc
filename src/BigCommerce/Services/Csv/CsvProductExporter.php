<?php

namespace BigCommerce\Services\Csv;

abstract
class CsvProductExporter extends FileProductExporter
{

	static
	function factory(int $bc_account_pk, string $payload_format, $db) : CsvProductExporter
	{
		switch ($payload_format) {
		case 'jet':
			return new JetCsvProductExporter($bc_account_pk, $db);
		case 'wm':
			return new WmCsvProductExporter($bc_account_pk, $db);
		default:
			throw new Exception(sprintf('unsupported payload format "%s"', $payload_format)); }
	}

	function outputStart()
	{
		$this->outputCsvRecord($this->variantHandler->tabularNames());
	}

    #Deprecated
	function outputEnd() {}


	abstract
	protected
	function csvRcd(array $productRcd) : array;

    #Deprecated
	function outputHttpMimeType()
	{

	}

    protected
    function outputCsvRecord(array $rcd)
    {
        $v = fputcsv($this->output_handle, $rcd);
        if ($v === false)
            throw new \Exception('failed to generate or send CSV data: ' .error_get_last()['message']);
    }

	function outputDataQuant()
	{
		foreach ($this->fetchDataQuant() as $productRcd){
            $record = $this->variantHandler->records($productRcd);
            $this->outputCsvRecord($record);
        }
	}

}

