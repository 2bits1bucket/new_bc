<?php
namespace BigCommerce\Services\Csv;

abstract
class ProductExporter
{
	const PRODUCT_FETCH_QUANT = 100;


	protected $bc_account_pk;
	private $cursor;
	protected $done;
	protected $db;


	protected
	function __construct(int $bc_account_pk, $db)
	{
		$this->bc_account_pk = $bc_account_pk;
		$this->db = $db;
	}


	abstract
	function outputStart();


	abstract
	function outputDataQuant();


	abstract
	function outputEnd();


	private
	function initCursor() : \PDOStatement
	{
		$db = $this->db;

		if ($this->cursor !== null)
			throw new \Exception('already have a cursor open');

		$cursorName = get_class($this);

			# transaction to handle the subsequent cursor in a simple way
		$db->beginTransaction();

		$this->cursor = $db->prepare('
			DECLARE ' .$db->escapeName($cursorName) .'
				INSENSITIVE NO SCROLL
			CURSOR
				WITHOUT HOLD
			FOR
			    SELECT  V.*, P.*, 
                        P.sku AS prod_sku, P.inventory_level AS prod_inv_lvl, 
                        V.data_bulk AS vjson, P.data_bulk AS pjson
                FROM bc_product_variant AS V
                LEFT JOIN bc_product P ON V.product_id = P.id 
                WHERE P.bc_account_pk = :bc_account_pk
                ORDER BY P.id ASC');

		$this->cursor->execute([ 'bc_account_pk' => $this->bc_account_pk ]);

			# apparently FETCH statement doesn't support parameters
		$q = $db->prepare('
			FETCH FORWARD ' .(int)static::PRODUCT_FETCH_QUANT .'
			FROM ' .$db->escapeName($cursorName) );
		return $q;
	}


	protected
	function fetchDataQuant() : array
	{
		static $q;

		if ($this->cursor === null)
			$q = $this->initCursor();

		$q->execute();
		$ret = $q->fetchAll();
		$this->done(empty($ret));
		$q->closeCursor();
		return $ret;
	}


	function done(bool $v = null) : ?bool
	{
		if ($v !== null)
			$this->done = $v;
		return $this->done;
	}
}
