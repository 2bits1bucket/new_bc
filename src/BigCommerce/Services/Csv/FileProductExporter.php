<?php
namespace BigCommerce\Services\Csv;

abstract
class FileProductExporter extends ProductExporter
{
	const HTTP_OUTPUT_TO_BROWSER = 'php://output';
	const HTTP_OUTPUT_TEMP= 'php://temp';
	const HTTP_OUTPUT_STREAM = 'php://memory';

	protected $output_handle;
	protected $output;

	function setOutput($outputTo)
	{
		switch ($outputTo) {
		case static::HTTP_OUTPUT_TO_BROWSER:
			$this->output = static::HTTP_OUTPUT_TO_BROWSER;
			$this->output_handle = fopen(static::HTTP_OUTPUT_TO_BROWSER, 'a');
			return;
		case static::HTTP_OUTPUT_STREAM:
            $this->output = static::HTTP_OUTPUT_STREAM;
            $this->output_handle = fopen(static::HTTP_OUTPUT_STREAM, 'r+');
            return;
        case static::HTTP_OUTPUT_TEMP:
            $this->output = static::HTTP_OUTPUT_TEMP;
            $this->output_handle = fopen(static::HTTP_OUTPUT_TEMP, 'r+');
            return;
		throw new \Exception('output mode not supported: ' .$outputTo); }
	}

    function getStream(){
	    return $this->output_handle;
    }

	abstract
	function fileName() : string;
}
