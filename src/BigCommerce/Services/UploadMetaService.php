<?php

namespace BigCommerce\Services;

use \BigCommerce\Models\BCAccount as BCAccount;
use \PDB\PDB;

class UploadMetaService
{

    private static $instance = null;
    protected $db;
    protected $productService;
    protected $qty_upload_queue = [
        'bc_account_pk', 'product_id', 'variant_id',
        'sku', 'quantity', 'created_at', 'updated_at', 'uploaded_time', 'download_node_id', 'inventory_tracking'];

    protected $table = 'qty_upload_queue';

    private $qty_sync_schema = [
        'qty_sync_fetch_master_upload_to_marketplace',
        'qty_sync_download_marketplace_forward_to_master',
        'qty_sync_none'
    ];

    private function __construct(PDB $pdb, ProductService $productService)
    {
        $this->db=$pdb;
        $this->productService = $productService;
    }

    public static function getInstance($db, ProductService $productService) {
        if (!isset(self::$instance)) {
            self::$instance = new UploadMetaService($db, $productService);
        }
        return self::$instance;
    }

    public function addProductToUploadQueue($product, $quantity, $node_id, $acc_id){

        $data = [
            'bc_account_pk' => $acc_id,
            'sku' => $product['sku'],
            'quantity' =>$quantity,
            'download_node_id' => $node_id,
            'product_id' => $product['bc_product_id'],
            'variant_id' => $product['variant_id'],
            'inventory_tracking' => $product['inventory_tracking']
        ];

        $sql = ' INSERT INTO qty_upload_queue ( bc_account_pk, sku, quantity, download_node_id, 
                                            product_id, variant_id, inventory_tracking, updated_at) 
                   VALUES ( :bc_account_pk, :sku, :quantity, :download_node_id, 
                            :product_id, :variant_id, :inventory_tracking, NOW()) 
                   ON CONFLICT (bc_account_pk, product_id, variant_id) 
                   DO UPDATE SET 
                    quantity = :quantity,
                    download_node_id = :download_node_id,
                    product_id = :product_id,
                    inventory_tracking = :inventory_tracking,
                    variant_id = variant_id,
                    updated_at = NOW(),
                    uploaded_time = NULL 
				RETURNING * ';

        $q = $this->db->prepare($sql);

        $q->execute($data);

        return $q->fetch();
    }

    public function addToUploadQueue($sku, $quantity, $node_id, $acc_id)
    {

        $data = [
            'bc_account_pk' => $acc_id,
            'sku' => $sku,
            'quantity' =>$quantity,
            'download_node_id' => $node_id
        ];

        $sql = ' INSERT INTO qty_upload_queue ( bc_account_pk, sku, quantity, download_node_id) 
              VALUES ( :bc_account_pk, :sku, :quantity, :download_node_id ) 
              ON CONFLICT (bc_account_pk, sku) 
              DO UPDATE SET  
                quantity = :quantity,
                download_node_id = :download_node_id,
                uploaded_time = NULL 
                RETURNING * ';

        $q = $this->db->prepare($sql);

        $q->execute($data);

        return $q->fetch();
    }

    public function mergeProduct($masterProduct, $acc_id) : bool
    {

        $sku = $masterProduct['sku'];

        $variant = $this->productService->findVariantsBySkuAndAcc($sku, $acc_id);

        if($variant!=null){

            $data = [
                'product_id' => $variant['bc_product_id'],
                'variant_id' => $variant['variant_id'],
                'inventory_tracking' => $variant['inventory_tracking'],
                'id' => $masterProduct['id']
            ];

            $sql = ' UPDATE qty_upload_queue SET 
                  product_id = :product_id,
                  variant_id = :variant_id,
                  inventory_tracking = :inventory_tracking,
                  updated_at = NOW()
                WHERE id = :id ';

            $q = $this->db->prepare($sql);

            $updated = $q->execute($data);

            return $updated;
        }

        return false;
    }

    public function getUploadQueue(){

        $sql = 'SELECT * FROM qty_upload_queue 
                WHERE 
                uploaded_time IS NULL AND 
                updated_at IS NOT NULL; ';

        $rcd = $this->db->many($sql);

        $data = [];

        foreach ((array)$rcd as $r){
            $data[] = $r;
        }

        return $data;
    }

    public function getAccountsTimePeriod()
    {

        $sql = 'SELECT 
                  bc_account_pk, 
                  MAX(created_at) AS last_entry_date 
                FROM qty_upload_queue 
                GROUP BY bc_account_pk; ';

        $rcd = $this->db->many($sql);

        $data = [];
        foreach ((array)$rcd as $r){
            $data[$r['bc_account_pk']] = $r['last_entry_date'];
        }

        return $data;
    }

    public function setAsSync($syncProductId)
    {

        $data = ['id' => $syncProductId];

        $sql = ' UPDATE qty_upload_queue  SET 
                  uploaded_time = NOW()
                WHERE id = :id ';

        $q = $this->db->prepare($sql);

        $q->execute($data);

        return $q;
    }

}