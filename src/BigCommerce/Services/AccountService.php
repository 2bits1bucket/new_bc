<?php

namespace BigCommerce\Services;

use \BigCommerce\Models\BCAccount as BCAccount;
use \PDB\PDB;

class AccountService
{

    protected $db;

    private $qty_sync_schema = [
        'qty_sync_fetch_master_upload_to_marketplace',
        'qty_sync_download_marketplace_forward_to_master',
        'qty_sync_none'
    ];

    public function __construct(PDB $pdb)
    {
        $this->db=$pdb;
    }

    public function getAccounts()
    {

        $sql= 'SELECT * FROM bc_account ORDER BY bc_account_pk ASC ';

        $accounts = [];
        foreach ($this->db->many($sql) as $rcd){
            $accounts[]=new BCAccount($rcd);
        }

        return $accounts;
    }

    public function byId(int $bc_account_pk) : BCAccount
    {
        $sql = ' SELECT * FROM bc_account WHERE bc_account_pk = :bc_account_pk ';

        $rcd = $this->db->one($sql, ['bc_account_pk' => $bc_account_pk ] );

        return new BCAccount($rcd);
    }

    public function updateAccount($acc){

        $upload_nodes = explode(',', $acc['qty_upload_master_nodes']);
        $upload_nodes = array_map('trim', $upload_nodes);

        $data = [
            'friendly_name' => $acc['friendly_name'],
            'store_hash' => $acc['store_hash'],
            'client_id' => $acc['client_id'],
            'client_secret' => $acc['client_secret'],
            'access_token' => $acc['access_token'],
            'qty_download_master_node' => $acc['qty_download_master_node'],
            'qty_sync_schema' => $acc['qty_sync_schema'],
            'qty_upload_master_nodes' =>  json_encode((array)$upload_nodes),
            'bc_account_pk' => $acc['bc_account_pk']
        ];

        $sql = ' UPDATE bc_account SET 
                  friendly_name = :friendly_name, 
                  store_hash = :store_hash, 
                  client_id = :client_id, 
                  client_secret = :client_secret, 
                  qty_download_master_node = :qty_download_master_node, 
                  access_token = :access_token,
                  qty_sync_schema = :qty_sync_schema, 
                  qty_upload_master_nodes = :qty_upload_master_nodes
                  WHERE bc_account_pk = :bc_account_pk
              RETURNING * ';

        $q = $this->db->prepare($sql);

        $q->execute($data);

        return $q->fetch();
    }

    public function createAccount($acc){

        $upload_nodes = explode(',', $acc['qty_upload_master_nodes']);
        $upload_nodes = array_map('trim', $upload_nodes);

        $data = [
            'user_id' => 1,
            'friendly_name' => $acc['friendly_name'],
            'store_hash' => $acc['store_hash'],
            'client_id' => $acc['client_id'],
            'client_secret' => $acc['client_secret'],
            'access_token' => $acc['access_token'],
            'qty_download_master_node' => $acc['qty_download_master_node'],
            'qty_sync_schema' => $acc['qty_sync_schema'],
            'qty_upload_master_nodes' =>  json_encode((array)$upload_nodes)
        ];

        $sql = ' INSERT INTO bc_account ( user_id, friendly_name, store_hash, 
                             client_id, client_secret, qty_download_master_node, access_token,
                             qty_sync_schema, qty_upload_master_nodes) 
                VALUES (:user_id, :friendly_name, :store_hash, 
                             :client_id, :client_secret, :qty_download_master_node, :access_token,
                             :qty_sync_schema, :qty_upload_master_nodes) RETURNING * ';

        $q = $this->db->prepare($sql);

        $q->execute($data);

        return $q->fetch();
    }

    public function getAccountsForOrderSync()
    {
        return $this->getBySyncSchema('qty_sync_fetch_master_upload_to_marketplace');
    }


    public function getAccountsForDownloadSync()
    {
        return $this->getBySyncSchema('qty_sync_download_marketplace_forward_to_master');
    }


    public function getAccountsForUploadSync()
    {
        return $this->getBySyncSchema('qty_sync_fetch_master_upload_to_marketplace');
    }


    private function getBySyncSchema($schema){

        if(!in_array($schema, $this->qty_sync_schema)) return [];

        $sql= 'SELECT * FROM bc_account WHERE qty_sync_schema = :qty_sync_schema';

        $data = [ 'qty_sync_schema' => $schema ];

        $accounts = [];
        foreach ($this->db->many($sql, $data) as $rcd){
            $accounts[] = new BCAccount($rcd);
        }

        return $accounts;
    }


}