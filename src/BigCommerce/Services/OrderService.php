<?php

namespace BigCommerce\Services;

use \PDB\PDB;

class OrderService {

    private static $instance = null;

    private $db;

    function __construct(PDB $db){
        $this->db = $db;
    }

    public static function getInstance($db) {
        if (!isset(self::$instance)) {
            self::$instance = new OrderService($db);
        }
        return self::$instance;
    }

    function saveOrder($orderData, int $bc_account_pk){

        $data = [
            'bc_account_pk' => $bc_account_pk,
            'order_id' =>  $orderData->id,
            'date_created' => $orderData->date_created,
            'date_modified' => ($orderData->date_modified) ? $orderData->date_modified : null,
            'date_shipped' => ($orderData->date_shipped) ? $orderData->date_shipped: null,
            'status_id' =>$orderData->status_id,
            'status_name' =>$orderData->status,
            'bulk_data' =>json_encode($orderData)
        ];

        $sql = ' INSERT INTO bc_orders (bc_account_pk, order_id, date_created, date_modified, date_shipped,  status_id, status_name, bulk_data) 
                VALUES (:bc_account_pk, :order_id, :date_created, :date_modified, :date_shipped, :status_id, :status_name, :bulk_data ) RETURNING * ';

        $q = $this->db->prepare($sql);

        $q->execute($data);

        return $q->fetch();
    }

    function saveOrderProducts($product, $order_id){

        $data = [
            'order_id' => $order_id,
            'bc_order_id' => $product->order_id,
            'bc_product_id' => $product->id,
            'product_sku' => $product->sku,
            'quantity' => $product->quantity,
        ];

        $sql = ' INSERT INTO bc_order_products (order_id, bc_order_id, bc_product_id, product_sku, quantity ) 
                VALUES (:order_id, :bc_order_id, :bc_product_id, :product_sku, :quantity) RETURNING * ';

        $q = $this->db->prepare($sql);

        $q->execute($data);

        return $q->fetch();
    }

    function getOrderLastDateByAcc(){

        $sql = 'SELECT 
                  bc_account_pk, 
                  MAX(date_modified) AS last_order_date 
                FROM bc_orders 
                GROUP BY bc_account_pk; ';

        $rcd = $this->db->many($sql);

        $data = [];
        foreach ((array)$rcd as $r){
            $data[$r['bc_account_pk']] = $r['last_order_date'];
        }

        return $data;
    }

    function setOrderAsSync($id, $master_node_id){

        $sql = ' UPDATE bc_orders SET 
                  master_sync = :master_sync,
                  master_node_id = :master_node_id,
                  updated_at = NOW()
                WHERE id = :id ';

        $data = ['master_sync' => true, 'id' => $id, 'master_node_id' => $master_node_id];

        $q = $this->db->prepare($sql);

        $q->execute($data);

    }

    function findOrderProductById($product_id){
        try {

            $sql = ' SELECT * FROM bc_order_products WHERE bc_product_id = :bc_product_id LIMIT 1 ';
            $data = ['bc_product_id' => $product_id];
            $rcd = $this->db->one($sql, $data);

            return $rcd;
        } catch (\PDB\RowNotFoundException $exception){
            return null;
        }
    }

    function findByBcOrderId($bc_order_id){
        try {

            $sql = ' SELECT * FROM bc_orders WHERE order_id = :order_id LIMIT 1 ';
            $data = ['order_id' => $bc_order_id];
            $rcd = $this->db->one($sql,$data);

            return $rcd;
        } catch (\PDB\RowNotFoundException $exception){
            return null;
        }
    }

}