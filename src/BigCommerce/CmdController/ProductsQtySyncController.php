<?php

namespace BigCommerce\CmdController;
use BigCommerce\QtyUploadSync\BCUploadFastTask;
use BigCommerce\Services\AccountService;
use BigCommerce\Services\UploadMetaService;
use ConcurrentCurlPool\Pool;
use Slim\Container;

/**
 * Gets products/variants from qty_upload_queue
 * Sync it to BC inventory -- LIMIT result to handle 5 minutes !?
 * run every 5 min
 *
 * @package BigCommerce\CmdController
 */
class ProductsQtySyncController
{

    protected $ci;
    protected $accountService;
    protected $metaService;

    const CONCURRENT_SLOTS = 10;

    public function __construct(Container $ci, AccountService $accountService, UploadMetaService $metaService) {
        $this->ci = $ci;
        $this->accountService = $accountService;
        $this->metaService = $metaService;
    }

    public function runQtySync()
    {

        $accounts = $this->accountService->getAccountsForUploadSync();

        $Pool = new Pool(20);

        foreach ($accounts as $Account){
            $uploadMeta = $this->metaService->getUploadQueue($Account->bc_account_pk); // TODO LIMIT ?

            $this->log(sprintf('[Acc:%d][UploadQueue] found %d products for upload to BC inventory ',
                $Account->bc_account_pk,
                count($uploadMeta)));

            foreach ($uploadMeta as $product){
                $Pool->addTask(new BCUploadFastTask($Account, $product));
            }

        }

        while ($Pool->hasTasksP()) {
            foreach ($Pool->select() as $Task) {

                $acc_id = $Task->BCAccount()->bc_account_pk;

                if ($Task instanceof BCUploadFastTask) {
                    $syncProduct = $Task->getProduct();

                    $this->log(sprintf('[Acc:%d][BCUploadFastTask] sync %s successful',
                        $acc_id,
                        $syncProduct['sku']));

                    $this->metaService->setAsSync($syncProduct['id']);
                }

                if ($Task instanceof \ConcurrentCurlPool\MultiStepTask) {
                    if ($Task->hasNextTask()) {
                        $Pool->addTask($Task->nextTask());
                    }
                }

            }
        }

    }

    function log($val) {
        echo microtime().' | ';
        printf($val.PHP_EOL);
        if (ob_get_level())
            ob_flush();
        flush();
    }

}