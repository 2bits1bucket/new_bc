<?php

namespace BigCommerce\CmdController;

use BigCommerce\QtyDownloadSync\BCFastDownloadTask;
use BigCommerce\Services\AccountService;
use BigCommerce\Services\ProductService;
use ConcurrentCurlPool\Pool as Pool;
use Slim\Container as Container;

/**
 * run every 5 minutes
 * Reads products from bc by last modified date
 * Update products/variants in database.
 * Sets master_sync to false - for UploadSyncController;
 *
 * Class QtyDownloadSyncController
 * @package BigCommerce\CmdController
 */
class QtyDownloadSyncController
{

    protected $ci;
    protected $accountService;
    protected $productService;

    public function __construct(Container $ci, AccountService $accountService, ProductService $productService) {
        $this->ci = $ci;
        $this->accountService = $accountService;
        $this->productService = $productService;
    }

    public function runQtySync()
    {

        $Pool = new Pool(10);

        $accounts = $this->accountService->getAccountsForDownloadSync();
        $dateFrom = $this->productService->getLastModifyDateByAcc();

        $this->log('QtyDownloadSyncController START found '.count($accounts).' accounts');

        //

        foreach ($accounts as $Acc) {
            if(isset($dateFrom[$Acc->bc_account_pk])){

                $Pool->addTask(
                    new BCFastDownloadTask(
                        $Acc,
                        $this->productService,
                        $Pool,
                        null,
                        $dateFrom[$Acc->bc_account_pk]
                    )
                );

            }
        }

        while ($Pool->hasTasksP()) {
            foreach ($Pool->select() as $Task) {
                if ($Task instanceof BCFastDownloadTask){

                    $this->log(
                        sprintf('[Acc:%d] BCFastDownloadTask completed for %d page',
                            $Task->BCAccount()->bc_account_pk,
                            $Task->getPage())
                    );

                }else{
                    $var = get_class($Task);
                    $this->log($var);
                }

            }
        }

        $this->log('QtyDownloadSyncController END.');

    }

    function log($val) {
        echo microtime().' | ';
        printf($val.PHP_EOL);
        if (ob_get_level())
            ob_flush();
        flush();
    }

}