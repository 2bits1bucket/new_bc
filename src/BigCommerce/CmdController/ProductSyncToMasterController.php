<?php

namespace BigCommerce\CmdController;

use BigCommerce\QtyDownloadSync\BCFastDownloadTask;
use BigCommerce\QtyDownloadSync\MasterUploadTask;
use BigCommerce\Services\AccountService;
use BigCommerce\Services\ProductService;
use ConcurrentCurlPool\Pool as Pool;
use Slim\Container as Container;

/**
 * run every 5 minutes
 *
 * get entries from bc_product_variant join bc_product - where master_sync = false
 * upload it to master node ! -from database ! 260 ! set master_sync = true
 *
 * Limit database
 *
 * Class QtyDownloadSyncController
 * @package BigCommerce\CmdController
 */
class ProductSyncToMasterController
{

    protected $ci;
    protected $accountService;
    protected $productService;

    public function __construct(Container $ci, AccountService $accountService, ProductService $productService) {
        $this->ci = $ci;
        $this->accountService = $accountService;
        $this->productService = $productService;
    }

    public function runQtySync()
    {

        $Pool = new Pool(10);

        $accounts = $this->accountService->getAccountsForDownloadSync();

        $this->log(' ProductSyncToMasterController START ');

        foreach ($accounts as $Acc) {

            //- TODO LIMIT ?
            $products = $this->productService->findVariantsForSyncByAcc($Acc->bc_account_pk);

            if($products){
                $this->log(
                    sprintf('[Acc:%d] Fount %d products to Sync',
                        $Acc->bc_account_pk, count($products))
                );
                $Pool->addTask(new MasterUploadTask($Acc, $products, $this->productService));
            }

        }

        while ($Pool->hasTasksP()) {
            foreach ($Pool->select() as $Task) {

                if ($Task instanceof MasterUploadTask){
                    $this->log(
                        sprintf('[Acc:%d] MasterUploadTask completed.',
                            $Task->BCAccount()->bc_account_pk)
                    );
                }else{
                    $var = get_class($Task);
                    $this->log($var);
                }

            }
        }

        $this->log(' ProductSyncToMasterController END ');

    }

    function log($val) {
        echo microtime().' | ';
        printf($val.PHP_EOL);
        if (ob_get_level())
            ob_flush();
        flush();
    }

}