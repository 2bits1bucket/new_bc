<?php
/**
 * Created by PhpStorm.
 * User: masakra
 * Date: 24.01.18
 * Time: 13:06
 */

namespace BigCommerce\CmdController;

use BigCommerce\Services\AccountService;
use BigCommerce\Services\OrderService;
use Slim\Container as Container;
use ConcurrentCurlPool\Pool as Pool;
use BigCommerce\QtyOrderSync\BCOrdersTask;
use BigCommerce\QtyOrderSync\BCLastOrderDateTask;
use BigCommerce\QtyOrderSync\BCOrdersProductsTask;
use BigCommerce\QtyOrderSync\ChoseMasterNodeTask;

class MultiCurlTestController
{

    protected $ci;
    protected $accountService;
    protected $orderService;


    public function __construct(Container $ci, AccountService $accountService, OrderService $orderService) {
        $this->ci = $ci;
        $this->accountService = $accountService;
        $this->orderService = $orderService;
    }

    public function multiCurlTest(){

        $rustart = getrusage();

        $Accounts = $this->accountService->getAccounts();

        $pool_depth = 10;
        $Pool = new Pool($pool_depth);

        $DEFAULT_TIME_RANGE = '-1 days';
        $dateFrom = date(\DateTime::RFC1123, strtotime($DEFAULT_TIME_RANGE));

        foreach ($Accounts as $A) {
            $this->log("Creating order task for: ".$A->bc_account_pk);
            $Pool->addTask(new BCOrdersTask($A, $this->orderService, $dateFrom));
        }

        while ($Pool->hasTasksP() ){ //and !$Timer->expiredP()
            foreach ($Pool->select() as $Task) { //$Pool->select($Timer->remaining())
                if ($Task instanceof BCOrdersTask){

                    $orders = $Task->getprocessedOrders();

                    $this->log('acc id: '.$Task->Account->bc_account_pk.
                        'order count '.count($orders));

                    foreach ($orders as $order) {
                        $this->log('creating products task for acc id: '.$Task->Account->bc_account_pk.
                            ' order id '.$order['order_id']);
                        $Pool->addTask(new BCOrdersProductsTask($Task->Account, $this->orderService, $order));
                    }
                }

                if ($Task instanceof BCOrdersProductsTask ){
                    $products = $Task->getprocessedProducts();

                    $this->log('acc id: '.$Task->Account->bc_account_pk.
                        ' order id '.$Task->order['order_id'].' products count  '.count($products));

//                    $taskOrder = $Task->getOrder();
//                    $Pool->addTask(new ChoseMasterNodeTask($Task->Account, $taskOrder, $products, $this->orderService));
                }

                if ($Task instanceof \ConcurrentCurlPool\MultiStepTask) {
                    if ($Task->hasNextTask()) {
                        $this->log('CONCURRENT CURL TASK has next');
                        $Pool->addTask($Task->nextTask());
                    } else {
//                        ++$GeneralStats->task_chains_completed;
                    }
                }

            }

        }

        $this->log('### ITS END !');

        $ru = getrusage();

        $this->log("This process used " . $this->rutime($ru, $rustart, "utime") ." ms for its computations");
        $this->log("It spent " . $this->rutime($ru, $rustart, "stime") ." ms in system calls");
        $this->log('For pool depth: '.$pool_depth);
//        $Pool->getStatInfo(); -- log tasks

    }

    private function log($val) {
        echo microtime().' | ';
        printf($val.PHP_EOL);
        if (ob_get_level())
            ob_flush();
        flush();
    }

    private function rutime($ru, $rus, $index) {
        return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
            -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
    }

}