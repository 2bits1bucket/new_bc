<?php

namespace BigCommerce\CmdController;

use BigCommerce\QtyUploadSync\MasterDownloadFullTask;
use BigCommerce\Services\AccountService;
use BigCommerce\Services\ProductService;
use BigCommerce\Services\UploadMetaService;
use ConcurrentCurlPool\Pool as Pool;
use Slim\Container as Container;

/**
 *
 * FULL upload sku from master !
 * gets full list and add it too refresh  qty_upload_queue
 *
 * not implemented by dexen
 *
 * TODO pagination !
 *
 * ProductsQtySyncController - syncs it to BC Inventory
 *
 * @package BigCommerce\CmdController
 */
class QtyFullUploadSyncController
{

    protected $ci;
    protected $accountService;
    protected $metaService;
    protected $productService;

    const CONCURRENT_SLOTS = 10;

    public function __construct(Container $ci,
                                AccountService $accountService,
                                ProductService $productService,
                                UploadMetaService $metaService) {
        $this->ci = $ci;
        $this->accountService = $accountService;
        $this->metaService = $metaService;
        $this->productService = $productService;
    }

    public function runFor($request, $response, $args)
    {

        if (isset($args['id'])) {

            $acc_id = $args['id'];

            $this->log('ProductListDownloadController START found for account: ' . $acc_id);

            $Pool = new Pool(10);

            $account = $this->accountService->byId($acc_id);

            $Pool->addTask(new MasterDownloadFullTask($account, $this->productService, $this->metaService));

            while ($Pool->hasTasksP()) {
                foreach ($Pool->select() as $Task) {

                    $acc_id = $Task->BCAccount()->bc_account_pk;

                    if ($Task instanceof MasterDownloadFullTask) {

                        $count = $Task->addToQueue();

                        $this->log(sprintf('[Acc:%d][MasterDownloadFullTask] got %d products added to Upload Queue ',
                            $acc_id, $count));

                    }

                    if ($Task instanceof \ConcurrentCurlPool\MultiStepTask) {
                        if ($Task->hasNextTask()) {
                            $Pool->addTask($Task->nextTask());
                        }
                    }

                }

            }

            $this->log('QtyFullUploadSyncController END.');

        } else {
            $this->log('[ERROR] no account id provided');
        }

    }

    public function runQtySync()
    {

        $accounts = $this->accountService->getAccountsForUploadSync();

        $this->log(sprintf('[START] QtyFullUploadSyncController found %d Account ', count($accounts)));

        $nrConcurrentSlots = 10;

        $Pool = new Pool($nrConcurrentSlots);

        foreach ($accounts as $Account) {
           $Pool->addTask(new MasterDownloadFullTask($Account, $this->productService, $this->metaService));
        }

        while ($Pool->hasTasksP()) {
            foreach ($Pool->select() as $Task) {

                $acc_id = $Task->BCAccount()->bc_account_pk;

                if ($Task instanceof MasterDownloadFullTask) {

                    $count = $Task->addToQueue();

                    $this->log(sprintf('[Acc:%d][MasterDownloadFullTask] got %d products added to Upload Queue ',
                        $acc_id, $count));

                }

                if ($Task instanceof \ConcurrentCurlPool\MultiStepTask) {
                    if ($Task->hasNextTask()) {
                        $Pool->addTask($Task->nextTask());
                    }
                }

            }

        }

        $this->log('QtyFullUploadSyncController END.');
    }

    function log($val) {
        echo microtime().' | ';
        printf($val.PHP_EOL);
        if (ob_get_level())
            ob_flush();
        flush();
    }

}