<?php

namespace BigCommerce\CmdController;

use BigCommerce\Services\AccountService;
use BigCommerce\Services\OrderService;
use Slim\Container as Container;
use BigCommerce\BcApiClient\BCAPIv2;

class SingleThreadTestController
{

    protected $ci;
    protected $accountService;
    protected $orderService;

    public function __construct(Container $ci, AccountService $accountService, OrderService $orderService) {
        $this->ci = $ci;
        $this->accountService = $accountService;
        $this->orderService = $orderService;
    }

    public function testSingleThread(){

        $rustart = getrusage();

        $Accounts = $this->accountService->getAccounts();

        foreach ($Accounts as $account) {

            $API = new BCAPIv2($account);

            $this->processAccountOrders($API);

        }
        $this->log('### ITS END !');

        $ru = getrusage();

        $this->log("This process used " . $this->rutime($ru, $rustart, "utime") ." ms for its computations");
        $this->log("It spent " . $this->rutime($ru, $rustart, "stime") ." ms in system calls");

    }

    public function processAccountOrders(BCAPIv2 $API){

        $DEFAULT_TIME_RANGE = '-1 days';

        $dateFrom = date(\DateTime::RFC1123, strtotime($DEFAULT_TIME_RANGE));

        $acc_id = $API->account()->bc_account_pk;

        $this->log("START PROCESSING FOR ACC ID: ".$acc_id);

        $orders = [];

        try {
            $orders = $API->getSyncOrderTasks($dateFrom);
        }catch (\BigCommerce\BcApiClient\BCAPIv2Exception $exception){
            $this->log('[Exception]'. $acc_id.' :'. $exception->getMessage());
        }

        $this->log("ORDERS COUNT: ".count($orders));

        foreach((array)$orders as $order){

            if($this->statusCheck($order->status_id)){

                $saved_order = $this->orderService->findByBcOrderId($order->id);

                $order_was_not_null_flag = ($saved_order==null) ? false : true;

                if($saved_order==null || $saved_order['master_sync'] == false){

                    $saved_order = ($saved_order==null) ?
                        $this->orderService->saveOrder($order, $acc_id) :
                        $saved_order;

                    $products = json_decode($API->getOrderProducts($order->id)->body());

                    $this->log('acc id: '.$acc_id.' order id '.$order->id.' products count  '.count($products));

                    foreach ((array)$products as $product){

                        $old_product = ($order_was_not_null_flag) ?
                            $this->orderService->findOrderProductById($product->id) :
                            null;

                        $saved_product = ($old_product==null) ?
                            $this->orderService->saveOrderProducts($product, $saved_order['id']):
                            $old_product;

                    }

                    //SYNC ORDER TO MASTER NODE
                }

            }

        }

    }


    private function statusCheck(int $status){

        $AWAIT_FULFILL_STATUS = 9;  // Awaiting Fulfillment
        $AWAIT_SHIPMENT_STATUS = 8; // Awaiting Shipment

        if($status == $AWAIT_FULFILL_STATUS ||
            $status == $AWAIT_SHIPMENT_STATUS){
            return true;
        }

        return true;
    }

    
    private function log($val) {
        echo microtime().' | ';
        printf($val.PHP_EOL);
        if (ob_get_level())
            ob_flush();
        flush();
    }


    private function rutime($ru, $rus, $index) {
        return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
            -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
    }


}