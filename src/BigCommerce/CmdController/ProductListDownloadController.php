<?php

namespace BigCommerce\CmdController;

use BigCommerce\QtyDownloadSync\BCProductDownloadTask;
use BigCommerce\Services\AccountService;
use BigCommerce\Services\ProductService;
use ConcurrentCurlPool\Pool as Pool;
use Slim\Container as Container;

/**
 * full_bc_products_persist
 *
 * Downloads full product list with variants from BC inventory stores(inserts/updates) it to database
 * set master_sync flag ready to send to master, For every account
 */
class ProductListDownloadController
{

    protected $ci;
    protected $accountService;
    protected $productService;

    public function __construct(Container $ci, AccountService $accountService, ProductService $productService) {
        $this->ci = $ci;
        $this->accountService = $accountService;
        $this->productService = $productService;
    }

    public function runFor($request, $response, $args){

        if(isset($args['id'])){

            $acc_id = $args['id'];

            $this->log('ProductListDownloadController START found for account: '.$acc_id);

            $Pool = new Pool(10);

            $account = $this->accountService->byId($acc_id);

            $Pool->addTask(
                new BCProductDownloadTask(
                    $account,
                    $this->productService,
                    $Pool,
                    null
                )
            );

            while ($Pool->hasTasksP()) {
                foreach ($Pool->select() as $Task) {
                    if ($Task instanceof BCProductDownloadTask){

                        $this->log(
                            sprintf('[Acc:%d] BCProductDownloadTask completed for %d page',
                                $Task->BCAccount()->bc_account_pk,
                                $Task->getPage())
                        );

                    }else{
                        $var = get_class($Task);
                        $this->log('[WARNING] Unknown Task '.$var);
                    }
                }
            }

            $this->log('ProductListDownloadController END.');

        }else{
            $this->log('[ERROR] no account id provided');
        }

    }

    public function runQtySync()
    {

        $Pool = new Pool(10);

        $accounts = $this->accountService->getAccounts();

        $this->log('ProductListDownloadController START found '.count($accounts).' accounts');

        foreach ($accounts as $Acc) {

            $Pool->addTask(
                new BCProductDownloadTask(
                    $Acc,
                    $this->productService,
                    $Pool,
                    null
                )
            );
        }

        while ($Pool->hasTasksP()) {
            foreach ($Pool->select() as $Task) {
                if ($Task instanceof BCProductDownloadTask){

                    $this->log(
                        sprintf('[Acc:%d] BCProductDownloadTask completed for %d page',
                            $Task->BCAccount()->bc_account_pk,
                            $Task->getPage())
                    );

                }else{
                    $var = get_class($Task);
                    $this->log('[WARNING] Unknown Task '.$var);
                }
            }
        }
        $this->log('ProductListDownloadController END.');


    }

    function log($val) {
        echo microtime().' | ';
        printf($val.PHP_EOL);
        if (ob_get_level())
            ob_flush();
        flush();
    }

}

