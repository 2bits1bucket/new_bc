<?php

namespace BigCommerce\CmdController;

use BigCommerce\QtyUploadSync\BCUploadFastTask;
use BigCommerce\QtyUploadSync\MasterDownloadTask;
use BigCommerce\Services\AccountService;
use BigCommerce\Services\UploadMetaService;
use ConcurrentCurlPool\Pool as Pool;
use Slim\Container as Container;

/**
 * Get products sku/quantity - from Master from last modified date
 * matches/merges products form database
 *
 * ProductsQtySyncController - syncs it to BC Inventory
 *
 * Class QtyUploadSyncController
 * @package BigCommerce\CmdController
 */
class QtyUploadSyncController
{

    protected $ci;
    protected $accountService;
    protected $metaService;

    const CONCURRENT_SLOTS = 10;

    public function __construct(Container $ci, AccountService $accountService, UploadMetaService $metaService) {
        $this->ci = $ci;
        $this->accountService = $accountService;
        $this->metaService = $metaService;
    }

    public function runQtySync()
    {

        $accounts = $this->accountService->getAccountsForUploadSync();

        $this->log(sprintf('[START] QtyUploadSyncController found %d Account ', count($accounts)));

        $nrConcurrentSlots = 10;

        $Pool = new Pool($nrConcurrentSlots);

        $accountsTimePeriod = $this->metaService->getAccountsTimePeriod();

        foreach ($accounts as $Account) {

            if (isset($accountsTimePeriod[$Account->bc_account_pk])) {
                $time =  time() - strtotime($accountsTimePeriod[$Account->bc_account_pk]);
                $Pool->addTask(new MasterDownloadTask($Account, $time));
            }

        }

        while ($Pool->hasTasksP()) {
            foreach ($Pool->select() as $Task) {

                $acc_id = $Task->BCAccount()->bc_account_pk;

                if ($Task instanceof MasterDownloadTask) {

                    $payload = (array) $Task->getPayload();

                    $this->log(sprintf('[Acc:%d][MasterDownload] got %d products added to Upload Queue ',
                        $acc_id,
                        count($payload)));

                    foreach ($payload as $rcdSku => $rcdV) {

                        $uploadMeta = $this->metaService->addToUploadQueue($rcdSku, (int)current($rcdV), (int)key($rcdV), $acc_id);

                        if(!$this->metaService->mergeProduct($uploadMeta, $acc_id)){
                            $this->log(sprintf('[Acc:%d][mergeProduct] cant find product for SKU: %s ',
                                $acc_id,
                                $rcdSku));
                        };

                    }

                }

                if ($Task instanceof \ConcurrentCurlPool\MultiStepTask) {
                    if ($Task->hasNextTask()) {
                        $Pool->addTask($Task->nextTask());
                    }
                }

            }
        }
    }

    function log($val) {
        echo microtime().' | ';
        printf($val.PHP_EOL);
        if (ob_get_level())
            ob_flush();
        flush();
    }

}