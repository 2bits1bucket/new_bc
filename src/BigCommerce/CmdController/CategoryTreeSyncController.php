<?php

namespace BigCommerce\CmdController;

use BigCommerce\CategoryFetch\CategoryStore;
use BigCommerce\CategoryFetch\CategoryTreeFetchTask;
use BigCommerce\QtyDownloadSync\QuantityStore;
use BigCommerce\Services\AccountService;
use ConcurrentCurlPool\Pool as Pool;
use Slim\Container as Container;

class CategoryTreeSyncController
{

    protected $ci;
    protected $accountService;
    protected $quantityStore;
//    protected

    public function __construct(Container $ci) { //, AccountService $accountService, QuantityStore $quantityStore
        $this->ci = $ci;
//        $this->accountService = $accountService;
//        $this->quantityStore = $quantityStore;
    }

    public function runQtySync()
    {

        $db = $this->ci->get('pdb');

        $nrConcurrentSlots = 10;

        $Pool = new Pool($nrConcurrentSlots);
        $CategoryStore = new CategoryStore($db);

        foreach ($CategoryStore->accountsForRefresh() as $Account) {
            if($Account->bc_account_pk == 8){ //my test account
                $this->log(sprintf('[Acc:%d] CategoryTreeFetchTask ', $Account->bc_account_pk));
                $Pool->addTask(new CategoryTreeFetchTask($Account));
            }
        }

        while ($Pool->hasTasksP()) {
            foreach ($Pool->select() as $Task) {
                try {
                    if ($Task instanceof CategoryTreeFetchTask) {
                        $this->log(sprintf('[Acc:%d] CategoryTreeFetchTask Completed ', $Task->getAccount()->bc_account_pk));
                        $tree = $Task->CategoryTree();
                        if($tree!=null){
                            $CategoryStore->storeTree($tree);
                        }
                    }
                    if ($Task instanceof \ConcurrentCurlPool\MultiStepTask){
                        $this->log(sprintf('[Acc:%d] Has more tasks ', $Task->getAccount()->bc_account_pk));
                        if ($Task->hasNextTask()) {
                            $Pool->addTask($Task->nextTask());
                        }
                    }
                }catch (Throwable $e) {
                    $this->log(sprintf('[Acc:%d] Exception msg: %s ', $Task->getAccount()->bc_account_pk, $e->getMessage()));
                }
            }
        }

    }

    function log($val) {
        echo microtime().' | ';
        printf($val.PHP_EOL);
        if (ob_get_level())
            ob_flush();
        flush();
    }

}