<?php

namespace BigCommerce\CmdController;

use BigCommerce\QtyOrderSync\BCLastOrderDateTask;
use BigCommerce\QtyOrderSync\BCOrdersProductsTask;
use BigCommerce\QtyOrderSync\BCOrdersTask;
use BigCommerce\QtyOrderSync\ChoseMasterNodeTask;
use BigCommerce\Services\AccountService;
use BigCommerce\Services\OrderService;
use ConcurrentCurlPool\Pool as Pool;
use Slim\Container as Container;

/**
 *
 * 1st run it finds LastOrderDate
 * on 2bd run it search for new orders since last date
 * if orders exist - save and upload to Master node
 * Master node is found by for each order ChoseMasterNodeTask
 *
 * TODO/FIXME move
 * Program  sync products qty per order processed
 *
 * Class QtyOrderSyncController
 * @package BigCommerce\CmdController
 */
class QtyOrderSyncController
{

    protected $ci;
    protected $accountService;
    protected $orderService;

    public function __construct(Container $ci, AccountService $accountService, OrderService $orderService) {
        $this->ci = $ci;
        $this->accountService = $accountService;
        $this->orderService = $orderService;
    }

    public function runQtySync(){

        $Accounts = $this->accountService->getAccountsForOrderSync();

        $nrConcurrentSlots = 10;

        $Pool = new Pool($nrConcurrentSlots);

        $lastOrderDateByAcc = $this->orderService->getOrderLastDateByAcc();

        $this->log('QtyOrderSyncController START ');

        foreach ($Accounts as $A) {
            if(isset($lastOrderDateByAcc[$A->bc_account_pk])){

                $Pool->addTask(new BCOrdersTask($A, $this->orderService, $lastOrderDateByAcc[$A->bc_account_pk]));
                $this->log(
                    sprintf('[Acc:%d] BCOrdersTask since: %s ',
                        $A->bc_account_pk,
                        $lastOrderDateByAcc[$A->bc_account_pk]));

            } else {
                $Pool->addTask(new BCLastOrderDateTask($A, $this->orderService));
                $this->log(
                    sprintf('[Acc:%d] BCLastOrderDateTask ',
                        $A->bc_account_pk));
            }
        }

        while ($Pool->hasTasksP() ){
            foreach ($Pool->select() as $Task) {

                if ($Task instanceof BCOrdersTask){

                    $orders = $Task->getprocessedOrders();

                    $this->log(
                        sprintf('[Acc:%d] BCOrdersTask found %d orders ',
                            $Task->Account->bc_account_pk, count($orders)));

                    foreach ($orders as $order) {
                        $Pool->addTask(new BCOrdersProductsTask($Task->Account, $this->orderService, $order));
                    }
                }

                if ($Task instanceof BCOrdersProductsTask ){
                    $products = $Task->getprocessedProducts();
                    $taskOrder = $Task->getOrder();

                    $this->log(
                        sprintf('[Acc:%d] BCOrdersProductsTask found %d products entries ',
                            $Task->Account->bc_account_pk, count($products)));

                    $Pool->addTask(new ChoseMasterNodeTask($Task->Account, $taskOrder, $products, $this->orderService));
                }

                if ($Task instanceof \ConcurrentCurlPool\MultiStepTask) {
                    if ($Task->hasNextTask()) {
                        $Pool->addTask($Task->nextTask());
                        $this->log(
                            sprintf('[Acc:%d] Creating concurrent task ',
                                $Task->Account->bc_account_pk));
                    }
                }

            }

        }

        $this->log('QtyOrderSyncController END. ');

    }

    function log($val) {
        echo microtime().' | ';
        printf($val.PHP_EOL);
        if (ob_get_level())
            ob_flush();
        flush();
    }

}