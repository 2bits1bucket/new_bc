<?php

namespace BigCommerce\Models;

class BCAccount {

	protected $User = null;
	protected $record;

    protected $fields = [   'bc_account_pk', 'user_id', 'friendly_name',
                            'store_hash', 'client_id', 'client_secret',
                            'access_token', 'qty_download_master_node', 'qty_sync_schema',
                            'qty_upload_master_nodes' ];

	function __construct(array $record)
	{
        $this->record = $record;

		foreach ($this->fields as $field){
            $this->{$field} = $record[$field] ?? null;
        }

		$this->qty_upload_master_nodes = json_decode($this->qty_upload_master_nodes ?? '[]');

		if (!is_array($this->qty_upload_master_nodes)){
            throw new \LogicException('expected array, got ' .gettype($this->qty_upload_master_nodes));
        }
	}


	function name() : string
	{
		if ($this->friendly_name === null)
			return 'BC Account #' .$this->bc_account_pk;
		return $this->friendly_name;
	}

}
