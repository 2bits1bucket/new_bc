<?php


class BCProductVariant {
	private $rcd;
	private $Product;
	private $bulk_data;



	private
	function __construct(BCProduct $Product)
	{
		$this->Product = $Product;
	}


	static
	function fromRcdProduct(array $rcd, BCProduct $Product) : self
	{
		$ret = new self($Product);
		$ret->rcd = $rcd;
		$ret->bulk_data = json_decode($rcd['bulk_data']);
		return $ret;
	}


	function Product() : BCProduct
	{
		return $this->Product;
	}


	function __get(string $key)
	{
		if ($key === 'bulk_data')
			throw new Exception('direct access to bulk_data not supported');
		if (property_exists($this->bulk_data, $key))
			return $this->bulk_data->{$key};
		if (array_key_exists($key, $this->rcd))
			return $this->rcd[$key];
		else
			throw new Exception('unsupported field ' .$key);
	}


	function field(string $key)
	{
		return $this->__get($key);
	}


	function hasField(string $key)
	{
		return array_key_exists($key, $this->rcd) || property_exists($this->bulk_data, $key);
	}
}
