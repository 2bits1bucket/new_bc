<?php


class BCProduct {

	private $rcd;
	private $bulk_data;
		# three possible states:
		# NULL - data NOT FETCHED FROM THE DATABASE, attempt to access MUST cause Exception
		# empty array - data fetched from the database, no related images
		# array with data - data fetched from the database, has some images
#	private $bulk_data->product_image_data = null;

	static
	function fromRcd(array $rcd) : BCProduct
	{
		$ret = new BCProduct();
		$ret->rcd = $rcd;
		$ret->bulk_data = json_decode($rcd['bulk_data']);
		if (array_key_exists('product_image_record_agg', $rcd))
			$ret->bulk_data->product_image_data = json_decode($rcd['product_image_record_agg']) ?? [];
		else
			$ret->bulk_data->product_image_data = null;
		if (array_key_exists('product_category_pathname_record_agg', $rcd))
			$ret->bulk_data->product_category_pathname_data =
				json_decode($rcd['product_category_pathname_record_agg']) ?? [];
		else
			$ret->bulk_data->product_category_pathname_data = null;
		return $ret;
	}


	function __get(string $key)
	{
		if ($key === 'bulk_data')
			throw new Exception('direct access to bulk_data not supported');
		if (property_exists($this->bulk_data, $key))
			return $this->bulk_data->{$key};
		if (array_key_exists($key, $this->rcd))
			return $this->rcd[$key];
		else
			throw new Exception('unsupported field ' .$key);
	}


	function field(string $key)
	{
		return $this->__get($key);
	}


	function hasField(string $key)
	{
		return array_key_exists($key, $this->rcd) || property_exists($this->bulk_data, $key);
	}


	function has_product_image_data_fetched()
	{
		return $this->bulk_data->product_image_data !== null;
	}


	function product_images()
	{
		if (!$this->has_product_image_data_fetched())
			throw new LogicException('product image data was not fetched from DB for this product');

		return $this->bulk_data->product_image_data;
	}
}
