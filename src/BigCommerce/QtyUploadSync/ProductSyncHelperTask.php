<?php


namespace BigCommerce\QtyUploadSync;


use BigCommerce\BcApiClient\PartialBCAPIv3;
use BigCommerce\Models\BCAccount;

class ProductSyncHelperTask extends \ConcurrentCurlPool\MultiStepTask
{
	protected $Account;
	protected $cutoff;
	protected $continueAtPage;
	protected $curl_handle;
	protected $nextPage;


	const CUTOFF_GUARD_TIME_SECONDS = 1;
	const DOWNLOAD_QUANT = 1000;


	function __construct(BCAccount $Account, int $cutoff, int $continueAtPage = null)
	{
		$this->Account = $Account;
		$this->cutoff = $cutoff;
		$this->continueAtPage = $continueAtPage;
	}

	# sort => date_modified, direction => asc
	function productDownloadOptions() : array
	{
		$ret = [
			'limit' => static::DOWNLOAD_QUANT,
			'sort' => 'date_modified',
			'direction' => 'asc',
			'include' => 'variants',
			'include_fields' => 'sku,date_modified,inventory_tracking',

				# apparently filtering by a range is supported, as per
				# https://forum.bigcommerce.com/s/question/0D51300003ZJi2xCAD/using-the-v3-catalog-api-to-retrieve-modified-products
			'date_modified:min' => ($this->cutoff - static::CUTOFF_GUARD_TIME_SECONDS) ];

		if ($this->continueAtPage !== null)
			$ret['page'] = $this->continueAtPage;

		return $ret;
	}


	function generateApiRequest() # : resource
	{
		$this->BCAPI = new PartialBCAPIv3($this->Account);

		$this->Transfer = $this->BCAPI->getProducts($this->productDownloadOptions());

		return $this->Transfer->curl_handle();
	}


	function taskCurlHandle() # : resource
	{
		if ($this->curl_handle === null)
			$this->curl_handle = $this->generateApiRequest();

		return $this->curl_handle;
	}


	function taskRequestProcessed() 
	{
		$this->Response = $this->Transfer->BCAPIResponse();
		$this->nextPage = $this->Response->metaNextPage();
	}


	function nextTask() : \ConcurrentCurlPool\Task
	{
		return new static($this->Account, $this->cutoff, $this->nextPage);
	}


	function ProductSync() : ProductSync
	{
		return new ProductSync($this->Account, $this->Response->data());
	}


	function hasNextTask() : bool
	{
		return ($this->nextPage !== null);
	}
}
