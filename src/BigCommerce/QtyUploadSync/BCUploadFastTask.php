<?php


namespace BigCommerce\QtyUploadSync;


use BigCommerce\BcApiClient\PartialBCAPIv3;
use BigCommerce\Models\BCAccount;

class BCUploadFastTask extends \ConcurrentCurlPool\MultiStepTask
{
	protected $Account;

	protected $curl_handle;
	protected $BCAPI;
	protected $product;
	protected $Response;

	function __construct(BCAccount $Account, $product)
	{
		$this->Account = $Account;
		$this->product = $product;
	}

	private
	function generateApiRequest() # : resource
	{
		$this->BCAPI = new PartialBCAPIv3($this->Account);

		$rcd = $this->product;

		switch ($rcd['inventory_tracking']) {
		case 'none';
		case '':
				# it's unfortunate
				# but right now we have no way of skipping the request
				# FIXME - change the \ConcurrentCurlPool\Pool / \ConcurrentCurlPool\Task API
				# so that we can indicate no work is necessary
			$this->Transfer = $this->BCAPI->emptyRequest();
			break;
		case 'variant':
			$this->Transfer = $this->BCAPI->updateVariant($rcd['product_id'], $rcd['variant_id'],
				[ 'inventory_level' => $rcd['quantity'] ] );
			break;
		case 'product':
			$this->Transfer = $this->BCAPI->updateProduct($rcd['product_id'],
				[ 'inventory_level' => $rcd['quantity'] ] );
			break;
		default:
			throw new \Exception(sprintf('unsupported .inventory_tracking "%s"', $rcd['inventory_tracking'])); }

		return $this->Transfer->curl_handle();
	}

    public function getProduct(){
	    return $this->product;
    }

	function taskCurlHandle() # : resource
	{
		if ($this->curl_handle === null)
			$this->curl_handle = $this->generateApiRequest();

		return $this->curl_handle;
	}


	function taskRequestProcessed()
	{
		$this->Response = $this->Transfer->BCAPIResponse();

	}

    function taskExceptionHandler(\Throwable $e)
    {

        $msg = '[Acc:%d][TASK EXCEPTION] %s ';

        $this->log(sprintf($msg, $this->Account->bc_account_pk, $e->getMessage()));

        //request time outed
        if($e->getCode() == 408){

            //TODO restart tasks ?

        }else{
            if(isset($e->xdebug_message)){
                echo $e->xdebug_message;
            }else{
                echo $e->getFile().PHP_EOL;
                echo $e->getTraceAsString();
            }
        }

    }

	function hasNextTask() : bool
	{
		return false;
	}


	function nextTask() : \ConcurrentCurlPool\Task
	{
		return null;
	}


    function BCAccount() : BCAccount
    {
        return $this->Account;
    }

}

