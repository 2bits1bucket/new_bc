<?php

namespace BigCommerce\QtyUploadSync;


use BigCommerce\Models\BCAccount;
use PDB\PDB;

class QuantityStore
{
	protected $db;

		# fetch quantities up to one month back
	const DEFAULT_QTY_DOWNLOAD_CUTOFF_SECONDS = 31 * 24 * 3600;


	function __construct(PDB $db)
	{
		$this->db = $db;
	}


	function cutoffForQtyDownload(BCAccount $Account) : int
	{
		$rcd = $this->db->one('
			SELECT extract(epoch FROM NOW() - MAX(insert_stamp))::INT AS cutoff
			FROM qty_upload_queue
			WHERE bc_account_pk = :bc_account_pk ',
			[ 'bc_account_pk' => $Account->bc_account_pk ] );

		return $rcd['cutoff'] ?? static::DEFAULT_QTY_DOWNLOAD_CUTOFF_SECONDS;
	}


	function cutoffForProductUpload(BCAccount $Account) : int
	{
		$rcd = $this->db->one('
			SELECT extract(epoch FROM MAX(date_modified)) AS cutoff
			FROM qty_upload_product_map
			WHERE bc_account_pk = :bc_account_pk',
			[ 'bc_account_pk' => $Account->bc_account_pk ] );

		return $rcd['cutoff'] ?? 0;
	}


	function applyProductSync(ProductSync $ProductSync)
	{
		$db = $this->db;

		$q = $db->prepare('
			INSERT INTO qty_upload_product_map
				(bc_account_pk, product_id, variant_id,
					product_sku, variant_sku, date_modified, inventory_tracking)
				VALUES
				(:bc_account_pk, :product_id, :variant_id,
					:product_sku, :variant_sku, :date_modified, :inventory_tracking)

			ON CONFLICT (bc_account_pk, product_id, variant_id)
				DO UPDATE SET
					product_sku = EXCLUDED.product_sku,
					variant_sku = EXCLUDED.variant_sku,
					date_modified = EXCLUDED.date_modified,
					inventory_tracking = EXCLUDED.inventory_tracking
			');

		$db->beginTransaction();

			foreach ($ProductSync->variants() as $rcd)
				$q->execute($rcd);

		$db->commit();
	}


	function storeQuantities(Quantities $Quantities)
	{
		static $qry;

		if ($qry === null)
			$qry = $this->db->prepare('
				INSERT INTO qty_upload_queue
					(bc_account_pk, product_id, variant_id, quantity, insert_stamp)
				SELECT bc_account_pk, product_id, variant_id, :sum, NOW()
				FROM qty_upload_product_map
				WHERE bc_account_pk = :bc_account_pk
					AND (
							(product_sku = :sku AND inventory_tracking = $$product$$)
						OR
							(variant_sku = :sku AND inventory_tracking = $$variant$$) )

				ON CONFLICT (bc_account_pk, product_id, variant_id)
				DO UPDATE SET
					quantity = EXCLUDED.quantity,
					insert_stamp = NOW(),
						-- important: reset the record to the NEED UPLOAD state
					upload_stamp = NULL' );

		$this->db->beginTransaction();

			foreach ($Quantities as $rcd) {
				$qry->execute($rcd);
				if ($qry->rowCount() !== 1) {
					$msg = sprintf('Product not found: "%s", account %d',
						$rcd['sku'], $rcd['bc_account_pk'] );

if (true)
    echo $msg;
else
						throw new \Exception($msg); } }

		$this->db->commit();
	}


	protected
	function pickARecordForUpload(\BCAccount $Account) : array
	{
		static $qry;

		if ($qry === null)
			$qry = $this->db->prepare('
				SELECT qty_upload_queue.*, inventory_tracking
				FROM qty_upload_queue
				LEFT JOIN qty_upload_product_map
					USING(bc_account_pk, product_id, variant_id)
				WHERE bc_account_pk = :bc_account_pk
					AND upload_stamp IS NULL
				LIMIT 1' );

		return $qry->one([ 'bc_account_pk' => $Account->bc_account_pk ]);
	}


	function markAsUploaded(array $rcd)
	{
		static $qry;

		if ($qry === null)
			$qry = $this->db->prepare('
				UPDATE qty_upload_queue
				SET upload_stamp = NOW()
				WHERE bc_account_pk = :bc_account_pk
					AND product_id = :product_id
					AND variant_id = :variant_id' );

		$qry->execute(array_limited_subset($rcd, [ 'bc_account_pk', 'product_id', 'variant_id' ]));

		if ($qry->rowCount() !== 1)
			throw new \LogicException('wanted to delete exactly one record, affected ' .$qry->rowCount());
	}


	protected
	function QuantityHandler(BCAccount $Account) : ?QuantityHandler
	{
		try {
			return new QuantityHandler($this, $this->pickARecordForUpload($Account)); }
		catch (\PDB\RowNotFoundException $e) {
			return null; }
	}


	function uploadTask(BCAccount $Account) : \ConcurrentCurlPool\Task
	{
		$H = $this->QuantityHandler($Account);
		 if ($H === null)
			return new \ConcurrentCurlPool\NullTask();
		 else
			return new BCUploadTask($Account, $H);
	}

}
