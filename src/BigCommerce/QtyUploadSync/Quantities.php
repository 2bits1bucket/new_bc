<?php


namespace BigCommerce\QtyUploadSync;


use BigCommerce\Models\BCAccount;

class Quantities implements \IteratorAggregate
{
	protected $a;
	protected $Account;


	function __construct(BCAccount $Account, array $a)
	{
		$this->Account = $Account;
		$this->a = $a;
	}


	private
	function generator() : \Generator
	{
		$bc_account_pk = $this->Account->bc_account_pk;

		foreach ($this->a as $sku => $rcd) {
			$sum = array_sum($rcd);
			yield compact('bc_account_pk', 'sku', 'sum'); }
	}


	function getIterator() : \Generator
	{
		return $this->generator();
	}


	function isEmptyP() : bool
	{
		return empty($this->a);
	}


	function count() : int
	{
		return count($this->a);
	}
}
