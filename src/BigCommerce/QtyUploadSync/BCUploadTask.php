<?php


namespace BigCommerce\QtyUploadSync;


use BigCommerce\BcApiClient\PartialBCAPIv3;
use BigCommerce\Models\BCAccount;

class BCUploadTask extends \ConcurrentCurlPool\MultiStepTask
{
	protected $Account;
	protected $QuantityHandler;
	protected $curl_handle;
	protected $BCAPI;
	private $nextTask;


	function __construct(BCAccount $Account, QuantityHandler $QuantityHandler)
	{
		$this->Account = $Account;
		$this->QuantityHandler = $QuantityHandler;
	}


	private
	function generateApiRequest() # : resource
	{
		$this->BCAPI = new PartialBCAPIv3($this->Account);

		$rcd=  $this->QuantityHandler->quantityRecord();

		switch ($rcd['inventory_tracking']) {
		case 'none';
		case '':
				# it's unfortunate
				# but right now we have no way of skipping the request
				# FIXME - change the \ConcurrentCurlPool\Pool / \ConcurrentCurlPool\Task API
				# so that we can indicate no work is necessary
			$this->Transfer = $this->BCAPI->emptyRequest();
			break;
		case 'variant':
			$this->Transfer = $this->BCAPI->updateVariant($rcd['product_id'], $rcd['variant_id'],
				[ 'inventory_level' => $rcd['quantity'] ] );
			break;
		case 'product':
			$this->Transfer = $this->BCAPI->updateProduct($rcd['product_id'],
				[ 'inventory_level' => $rcd['quantity'] ] );
			break;
		default:
			throw new \Exception(sprintf('unsupported .inventory_tracking "%s"', $rcd['inventory_tracking'])); }

		return $this->Transfer->curl_handle();
	}


	function taskCurlHandle() # : resource
	{
		if ($this->curl_handle === null)
			$this->curl_handle = $this->generateApiRequest();

		return $this->curl_handle;
	}


	function taskRequestProcessed()
	{
		$this->Response = $this->Transfer->BCAPIResponse();

		$this->QuantityHandler->onSuccessfulUpload();
	}


	function hasNextTask() : bool
	{
		return !($this->nextTask() instanceof \ConcurrentCurlPool\NullTask);
	}


	function nextTask() : \ConcurrentCurlPool\Task
	{
		if ($this->nextTask === null)
			$this->nextTask = $this->QuantityHandler->nextTask($this->Account);
		return $this->nextTask;
	}
}
