<?php


namespace BigCommerce\QtyUploadSync;


use BigCommerce\Models\BCAccount;

/**
 *
 * As in https://docs.google.com/document/d/1mNvl8nWVylqy96n9rSNtu3vD1dAG0yukSb8CTI2KCMY/edit
 *
 * Class MasterDownloadTask
 * @package BigCommerce\QtyUploadSync
 */
class MasterDownloadTask extends \ConcurrentCurlPool\Task
{

	protected $curl_handle;
	protected $Account;
	protected $time_period_seconds;
	const GRACE_TIME_SECONDS = 70;


	function __construct(BCAccount $Account, int $time_period_seconds)
	{
		$this->Account = $Account;
		$this->time_period_seconds = $time_period_seconds;
	}


	private
	function requestData() : string
	{

		$meta = null;

		$user_id = $this->Account->user_id;

			# round() to force into integer format, as per the API specs
		$master_node_ids = array_map('round',
			array_values($this->Account->qty_upload_master_nodes) );

		$time_period = [
			'period' => $this->time_period_seconds + static::GRACE_TIME_SECONDS,
			'unit' => 'SECOND' ];

		$market = sprintf('BigCommerce|%d', $this->Account->bc_account_pk);

		$payload = compact('user_id', 'master_node_ids', 'time_period', 'market');

		return json_encode(compact('meta', 'payload'));
	}


	private
	function apiUrlInventorySet() : string
	{
		return $_ENV['API_MASTER_URL_BASE'] .'inventory/v1/get_latest.php';
	}


	private
	function apiAuth() : string
	{
		return $_ENV['API_MASTER_INVENTORY_LOGIN'] .':' .$_ENV['API_MASTER_INVENTORY_PASSWORD'];
	}


	private
	function createCurlRequest() # : resource
	{
		$h = curl_init($this->apiUrlInventorySet());

		curl_setopt($h, CURLOPT_RETURNTRANSFER, true);

			# whaaat? the docs says its supposed tobe DIGEST auth, but no?
		# curl_setopt($h, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
		curl_setopt($h, CURLOPT_USERPWD, $this->apiAuth());

		curl_setopt($h, CURLOPT_POST, 0); //TODO 1 need to be post ?
		curl_setopt($h, CURLOPT_POSTFIELDS, $this->requestData());
		curl_setopt($h, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json' ]);
		curl_setopt($h, CURLOPT_RETURNTRANSFER, true);

		return $h;
	}

	function taskCurlHandle()
	{
		if ($this->curl_handle === null)
			$this->curl_handle = $this->createCurlRequest();
		return $this->curl_handle;
	}

    function taskExceptionHandler(\Throwable $e)
    {

        $msg = '[Acc:%d][TASK EXCEPTION] %s ';

        $this->log(sprintf($msg, $this->Account->bc_account_pk, $e->getMessage(), $this->page));

        if($e->getCode() == 408){

           //   --  restart task ! --

        }else{
            if(isset($e->xdebug_message)){
                echo $e->xdebug_message;
            }else{
                echo $e->getFile().PHP_EOL;
                echo $e->getTraceAsString();
            }
        }

    }


	function taskRequestProcessed()
	{
		$O = json_decode($this->taskResponse()->body());
		if ($O->error !== null)
			throw new \RuntimeException('Master.GS API returned error ' .$O->error->message);
	}


	function getPayload()
	{
		$data = json_decode($this->taskResponse()->body, $assoc = true);
		if ($data === null)
			throw new \RuntimeException('failed to parse the response: ' .json_last_error_msg());

		if (!is_array($data['payload']))
			throw new \RuntimeException('missing the required ->payload');

	    return $data['payload'];
	}


	function BCAccount() : BCAccount
	{
		return $this->Account;
	}

}

