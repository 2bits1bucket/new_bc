<?php


namespace BigCommerce\QtyUploadSync;


use BigCommerce\Models\BCAccount;

class ProductSync
{
	protected $data;
	protected $Account;

	
	function __construct(BCAccount $Account, array $data)
	{
		$this->Account = $Account;
		$this->data = $data;
	}


	function variants() : \Generator
	{
		foreach ($this->data as $pRcd)
			foreach ($pRcd->variants as $vRcd)
				yield [
					'bc_account_pk' => $this->Account->bc_account_pk,
					'product_id' => $pRcd->id,
					'variant_id' => $vRcd->id,
					'product_sku' => nullOnEmpty($pRcd->sku),
					'variant_sku' => nullOnEmpty($vRcd->sku),
					'date_modified' => $pRcd->date_modified,
					'inventory_tracking' => $pRcd->inventory_tracking ];
	}


}
