<?php


namespace BigCommerce\QtyUploadSync;


use BigCommerce\Models\BCAccount;

class BCUploadTarget
{
	function uploadTask(BCAccount $Account, Quantities $Quantities) : \ConcurrentCurlPool\Task
	{
		return new BCUploadTask($Account, $Quantities);
	}
}
