<?php


namespace BigCommerce\QtyUploadSync;


use BigCommerce\Models\BCAccount;

class QuantityHandler
{
	protected $Store;
	protected $rcd;


	function __construct(QuantityStore $Store, array $rcd)
	{
		$this->Store = $Store;
		$this->rcd = $rcd;
	}


	function quantityRecord() : array
	{
		return $this->rcd;
	}


	function onSuccessfulUpload()
	{
		$this->Store->markAsUploaded($this->rcd);
	}


	function nextTask(BCAccount $Account) : \ConcurrentCurlPool\Task
	{
		return $this->Store->uploadTask($Account);
	}
}
