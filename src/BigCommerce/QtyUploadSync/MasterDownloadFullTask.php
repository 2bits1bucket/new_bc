<?php

namespace BigCommerce\QtyUploadSync;

use BigCommerce\Models\BCAccount;
use BigCommerce\Services\ProductService;
use BigCommerce\Services\UploadMetaService;

/**
 * as in https://docs.google.com/document/d/1HcFLK2atyCLfYPYInyj4sosQXUggiMomwwdl3vXlPS8/edit
 *
 * Class MasterDownloadFullTask
 * @package BigCommerce\QtyUploadSync
 */
class MasterDownloadFullTask extends \ConcurrentCurlPool\Task
{

	protected $curl_handle;
	protected $Account;
	protected $productService;
	protected $metaService;
	protected $products;
	protected $productsBySku;

	function __construct(BCAccount $Account, ProductService $productService, UploadMetaService $metaService)
	{
		$this->Account = $Account;
		$this->metaService = $metaService;
		$this->productService = $productService;

		$this->products = $productService->findVariantsByAcc($Account->bc_account_pk);
	}

	private
	function requestData()
	{
	    $skus = [];

	    foreach ($this->products as $product) {
            $skus[] = $product['sku'];
            $this->productsBySku[$product['sku']] = $product;
        }

        $data = [
            'user_id' => $this->Account->user_id,
            'master_node_ids' => $this->Account->qty_upload_master_nodes,
            'skus' =>  $skus
        ];

		return $data;
	}

	private
	function apiUrlInventorySet() : string
	{
		return $_ENV['API_MASTER_URL_BASE'] .'inventory/v1/get.php';
	}

	private
	function apiAuth() : string
	{
		return $_ENV['API_MASTER_INVENTORY_LOGIN'] .':' .$_ENV['API_MASTER_INVENTORY_PASSWORD'];
	}


	private
	function createCurlRequest() # : resource
	{
		$h = curl_init($this->apiUrlInventorySet());

		curl_setopt($h, CURLOPT_RETURNTRANSFER, true);

			# whaaat? the docs says its supposed tobe DIGEST auth, but no?
		# curl_setopt($h, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
		curl_setopt($h, CURLOPT_USERPWD, $this->apiAuth());

		curl_setopt($h, CURLOPT_POST, 1);
		curl_setopt($h, CURLOPT_POSTFIELDS, http_build_query($this->requestData()));
//		curl_setopt($h, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json' ]);
		curl_setopt($h, CURLOPT_RETURNTRANSFER, true);

		return $h;
	}

	function taskCurlHandle()
	{
		if ($this->curl_handle === null)
			$this->curl_handle = $this->createCurlRequest();
		return $this->curl_handle;
	}

    function taskExceptionHandler(\Throwable $e)
    {

        $msg = '[Acc:%d][TASK EXCEPTION] %s ';

        $this->log(sprintf($msg, $this->Account->bc_account_pk, $e->getMessage()));

        if($e->getCode() == 408){

           //   --  restart task ! --

        }else{
            if(isset($e->xdebug_message)){
                echo $e->xdebug_message;
            }else{
                echo $e->getFile().PHP_EOL;
                echo $e->getTraceAsString();
            }
        }

    }


	function taskRequestProcessed()
	{
		$O = json_decode($this->taskResponse()->body());
		if ($O->error !== null)
			throw new \RuntimeException('Master.GS API returned error ' .$O->error->message);
	}


	function addToQueue()
	{
		$data = json_decode($this->taskResponse()->body, $assoc = true);

		if ($data === null)
			throw new \RuntimeException('failed to parse the response: ' .json_last_error_msg());

		if (!is_array($data['payload']))
			throw new \RuntimeException('missing the required ->payload');

		$c=0;
        foreach ($data['payload'] as $rcdSku => $rcdV) {

            $acc_id = $this->Account->bc_account_pk;

            if(isset($this->productsBySku[$rcdSku])){

                $quantity = (int)current($rcdV);
                $node_id = (int)key($rcdV);

                $this->metaService->addProductToUploadQueue($this->productsBySku[$rcdSku], $quantity, $node_id, $acc_id);
                $c++;
            }else{
                //log it shouldn't happen
            }

        }

	    return $c;
	}


	function BCAccount() : BCAccount
	{
		return $this->Account;
	}

}

