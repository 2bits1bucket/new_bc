<?php
namespace BigCommerce\QtyOrderSync;

use BigCommerce\BcApiClient\PartialBCAPIv2;
use BigCommerce\Models\BCAccount;
use BigCommerce\Services\OrderService;

class BCOrdersProductsTask extends \ConcurrentCurlPool\MultiStepTask {

    public $Account;
    protected $curl_handle;

    protected $BCAPI;
    protected $Transfer;
    protected $Response;

    public $order;
    protected $orderService;

    protected $debug_tracing = false;

    function __construct(BCAccount $Account, OrderService $orderService, $order)
    {
        $this->Account = $Account;
        $this->order = $order;
        $this->orderService = $orderService;
    }

    function generateApiRequest() # : resource
    {
        $this->BCAPI = new PartialBCAPIv2($this->Account);
        $this->Transfer = $this->BCAPI->getOrderProducts($this->order['order_id']);
        return $this->Transfer->curl_handle();
    }

    function hasNextTask() : bool
    {
        return false;
    }

    function nextTask() : \ConcurrentCurlPool\Task
    {
        return null;
    }

    function taskCurlHandle() # : resource
    {
        if ($this->curl_handle === null) {
            $this->curl_handle = $this->generateApiRequest();
        }
        return $this->curl_handle;
    }

    function getProcessedProducts(){
        return $this->processedProducts;
    }

    function getOrder(){
        return $this->order;
    }

    function taskRequestProcessed()
    {

        $this->Response = $this->Transfer->BCAPIResponse();

        $productsRespond = json_decode($this->Response->body());

        foreach ($productsRespond as $product){

            $old_product = ($this->order['wasInDB']) ?
                $this->orderService->findOrderProductById($product->id) :
                null;

            $saved_product = ($old_product==null) ?
                $this->orderService->saveOrderProducts($product, $this->order['id']):
                $old_product;

            $this->processedProducts[] = $saved_product;

        }

    }

}