<?php

namespace BigCommerce\QtyOrderSync;

use BigCommerce\Models\BCAccount;

class ChoseMasterNodeTask extends \ConcurrentCurlPool\MultiStepTask
{
    protected $Target;
    protected $Account;
    protected $orderService;
    protected $order;
    protected $masterNodesByOrder;

    protected $products;
    protected $curl_handle;
    protected $QS;

    function __construct(BCAccount $Account, $order, $products, $orderService)
    {
        $this->Account = $Account;
        $this->products = $products;
        $this->order = $order;
        $this->orderService = $orderService;
    }

    private function apiAuth() : string
    {
        return $_ENV['API_GEEKSELLER_LOGIN'] . ':' . $_ENV['API_GEEKSELLER_PASSWORD'];
    }

    private
    function apiUrlInventorySet() : string
    {
        return $_ENV['API_GEEKSELLER_URL'] . 'master/v1/choose_order_node.php';
    }

    function requestContent() : string
    {
        $user_id = $this->Account->user_id;
        $market = 'BigCommerce|'.$this->Account->bc_account_pk;
        $orders = [];

        $orders['order_id'] = $this->order['order_id'];
        $orders['products'] = [];

        foreach ((array) $this->products as $product){
            //TODO product inventory_tracking CHECK ?
            $orders['products'][$product['product_sku']]=$product['quantity'];
        }

        $payload = compact('user_id', 'market', 'orders');

        return json_encode(compact('meta', 'payload'));
    }

    private
    function createCurlRequest() # : resource
    {

        $url = $this->apiUrlInventorySet();
        $h = curl_init($url);
        curl_setopt($h, CURLOPT_RETURNTRANSFER, true);

        # whaaat? the docs says its supposed tobe DIGEST auth, but no?
#		curl_setopt($h, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        curl_setopt($h, CURLOPT_USERPWD, $this->apiAuth());

        curl_setopt($h, CURLOPT_POST, 0);
        curl_setopt($h, CURLINFO_HEADER_OUT , 1);
        curl_setopt($h, CURLOPT_POSTFIELDS, $this->requestContent());
        curl_setopt($h, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json' ]);
        curl_setopt($h, CURLOPT_RETURNTRANSFER, true);

        return $h;
    }

    function taskCurlHandle() # : resource
    {
        if ($this->curl_handle === null)
            $this->curl_handle = $this->createCurlRequest();
        return $this->curl_handle;
    }

    function taskRequestProcessed()
    {
        $response = json_decode($this->taskResponse()->body());

        if($response==null){
            $this->log(sprintf('[Acc:%d] Empty response from ChoseMasterNode', $this->Account->bc_account_pk ));
            return;
        }

        if ($response->error !== null){
            throw new \RuntimeException('API.GSC API returned error ' . $response->error->message);
        }

        $ordersMasterNodes = [];
        foreach((array) $response->payload as $orders){
            foreach ((array) $orders as $order_id => $order) {
                $ordersMasterNodes[$order_id] = $order->master_node_id;
            }
        }

        $this->masterNodesByOrder = $ordersMasterNodes;
    }

    function hasNextTask() : bool
    {
        if(isset($this->masterNodesByOrder[$this->order['order_id']]) &&
            $this->masterNodesByOrder[$this->order['order_id']] > 0 ){
            return true;
        }
        return false;
    }

    function nextTask() : \ConcurrentCurlPool\Task
    {
        return new BCQtyMasterSyncTask(
            $this->Account,
            $this->masterNodesByOrder[$this->order['order_id']],
            $this->order,
            $this->products,
            $this->orderService
        );
    }

}

