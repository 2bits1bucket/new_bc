<?php

namespace BigCommerce\QtyOrderSync;

use ConcurrentCurlPool\Task;
use BigCommerce\Models\BCAccount;
use BigCommerce\Services\OrderService;
use BigCommerce\BcApiClient\PartialBCAPIv2;

class BCOrdersTask extends \ConcurrentCurlPool\MultiStepTask {

    public $Account;
    protected $curl_handle;

    protected $BCAPI;
    protected $Transfer;
    protected $Response;
    protected $nextPage;
    protected $page;
    protected $orderService;

    protected $date_modified_from;
    protected $debug_tracing = false;

    protected $processedOrders;

    const CUTOFF_GUARD_TIME_SECONDS = 1;
    const DEFAULT_LIMIT = 250;
    # apparently they count from 1, rather than from 0.
    # of course they made 0 work too...
    const START_PAGE = 1;

    function __construct(BCAccount $Account, OrderService $orderService, $dateFrom)
    {
        $this->Account = $Account;
        $this->date_modified_from = $dateFrom;
        $this->orderService = $orderService;
        $this->page = static::START_PAGE;
    }

    private
    function statusCheck(int $status){

        $AWAIT_FULFILL_STATUS = 9;  // Awaiting Fulfillment
        $AWAIT_SHIPMENT_STATUS = 8; // Awaiting Shipment

        if($status == $AWAIT_FULFILL_STATUS ||
            $status == $AWAIT_SHIPMENT_STATUS){
            return true;
        }

        return false;
    }


    function generateApiRequest() # : resource
    {
        $this->BCAPI = new PartialBCAPIv2($this->Account);

        $date = date(\DateTime::RFC1123, strtotime($this->date_modified_from));

        $params = [
            'min_date_modified' => $date,
            'sort'=>'date_modified:desc',
            'page' => $this->page,
            'limit' => static::DEFAULT_LIMIT,
        ];

        $this->Transfer = $this->BCAPI->getOrders($params);

        return $this->Transfer->curl_handle();
    }


    function hasNextTask() : bool
    {
        return ($this->nextPage !== null);
    }


    function nextTask() : \ConcurrentCurlPool\Task
    {
        return null;
    }

    function taskCurlHandle() # : resource
    {
        if ($this->curl_handle === null) {
            $this->curl_handle = $this->generateApiRequest();
        }
        return $this->curl_handle;
    }

    function getProcessedOrders(){
        return $this->processedOrders;
    }

    function taskExceptionHandler(\Throwable $e)
    {
        $msg = $e->getMessage();
        $this->log(sprintf($msg, $this->Account->bc_account_pk, $e->getMessage(), $this->page));
    }

    function taskRequestProcessed()
    {

        $this->Response = $this->Transfer->BCAPIResponse();
        $this->nextPage =  null; // $this->Response->metaNextPage();

        $ordersRespond = json_decode($this->Response->body());

        $orders = [];

        foreach ($ordersRespond as $order) {

            if($this->statusCheck($order->status_id)){

                $saved_order = $this->orderService->findByBcOrderId($order->id);

                $order_was_not_null_flag = ($saved_order==null) ? false : true;

                if($saved_order==null || $saved_order['master_sync'] == false) {

                    $saved_order = ($saved_order == null) ?
                        $this->orderService->saveOrder($order, $this->Account->bc_account_pk) :
                        $saved_order;

                    $saved_order['wasInDB'] = $order_was_not_null_flag;

                    $orders[] = $saved_order;

                }

            }
        }

        $this->processedOrders = $orders;
    }

}