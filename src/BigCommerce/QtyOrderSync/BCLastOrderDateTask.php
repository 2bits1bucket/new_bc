<?php

namespace BigCommerce\QtyOrderSync;

use ConcurrentCurlPool\Task;
use BigCommerce\Models\BCAccount;
use BigCommerce\Services\OrderService;
use BigCommerce\BcApiClient\PartialBCAPIv2;

class BCLastOrderDateTask extends \ConcurrentCurlPool\MultiStepTask {

    public $Account;
    protected $curl_handle;
    protected $dateFrom = null;

    protected $BCAPI;
    protected $Transfer;
    protected $Response;
    protected $orderService;

    protected $date_modified_from;
    protected $debug_tracing = false;

    protected $processedOrders;

    const CUTOFF_GUARD_TIME_SECONDS = 1;
    const DEFAULT_LIMIT = 1;
    # apparently they count from 1, rather than from 0.
    # of course they made 0 work too...
    const START_PAGE = 1;

    function __construct(BCAccount $Account, OrderService $orderService)
    {
        $this->Account = $Account;
        $this->orderService = $orderService;
    }

    function generateApiRequest() # : resource
    {
        $this->BCAPI = new PartialBCAPIv2($this->Account);

        $params = [
            'sort'=>'date_modified:desc',
            'page' => static::START_PAGE,
            'limit' => static::DEFAULT_LIMIT,
        ];

        $this->Transfer = $this->BCAPI->getOrders($params);

        return $this->Transfer->curl_handle();
    }

    function hasNextTask() : bool
    {
        return ($this->dateFrom !== null);
    }

    function nextTask() : Task
    {
      return new BCOrdersTask($this->Account, $this->orderService, $this->dateFrom);
    }

    function taskCurlHandle() # : resource
    {
        if ($this->curl_handle === null) {
            $this->curl_handle = $this->generateApiRequest();
        }
        return $this->curl_handle;
    }

    function taskRequestProcessed()
    {

        $this->Response = $this->Transfer->BCAPIResponse();
        $ordersRespond = json_decode($this->Response->body());

        if(isset($ordersRespond[0]->date_modified)){
            $order = $this->orderService->saveOrder($ordersRespond[0], $this->Account->bc_account_pk);
            $this->orderService->setOrderAsSync($order['id'], 0);
            $this->dateFrom = $ordersRespond[0]->date_modified;
        }

    }

}