<?php

namespace BigCommerce\QtyOrderSync;

use BigCommerce\Models\BCAccount;
use BigCommerce\Services\OrderService;

class BCQtyMasterSyncTask extends \ConcurrentCurlPool\MultiStepTask
{
    protected $Target;
    protected $Account;
    protected $orderService;
    protected $masterNodeId;
    protected $order;

    protected $products;
    protected $curl_handle;
    protected $QS;
    private $nextTask;


    function __construct(BCAccount $Account, $master_node_id, $order, $products, OrderService $orderService)
    {
        $this->Account = $Account;
        $this->products = $products;
        $this->masterNodeId = $master_node_id;
        $this->orderService = $orderService;
        $this->order = $order;
    }

    private
    function apiAuth() : string
    {
        return $_ENV['API_MASTER_INVENTORY_LOGIN'] . ':' . $_ENV['API_MASTER_INVENTORY_PASSWORD'];
    }

    private
    function apiUrlInventorySet() : string
    {
        return $_ENV['API_MASTER_URL_BASE'] . 'inventory/v1/set.php';
    }

    function requestContent() : string
    {
        $user_id = $this->Account->user_id;
        $master_node = $this->masterNodeId;

        $products = [];

        foreach ((array) $this->products as $product){
            $products[$product['product_sku']] = [
                $master_node => [ 'type' => 'add', 'qty' => -abs((int)$product['quantity'])]
            ];
        }

        $payload = compact('user_id', 'products');

        return json_encode(compact('meta', 'payload'));
    }

    private
    function createCurlRequest() # : resource
    {
        $h = curl_init($this->apiUrlInventorySet());
        curl_setopt($h, CURLOPT_RETURNTRANSFER, true);

        # whaaat? the docs says its supposed tobe DIGEST auth, but no?
#		curl_setopt($h, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        curl_setopt($h, CURLOPT_USERPWD, $this->apiAuth());

        curl_setopt($h, CURLOPT_POST, 0);
        curl_setopt($h, CURLOPT_POSTFIELDS, $this->requestContent());
        curl_setopt($h, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json' ]);
        curl_setopt($h, CURLOPT_RETURNTRANSFER, true);

        return $h;
    }


    function taskCurlHandle() # : resource
    {
        if ($this->curl_handle === null)
            $this->curl_handle = $this->createCurlRequest();
        return $this->curl_handle;
    }

    function taskRequestProcessed()
    {
        $O = json_decode($this->taskResponse()->body());
        if ($O->error !== null){
            throw new \RuntimeException('Master.GS API returned error ' .$O->error->message);
        }

        $this->orderService->setOrderAsSync($this->order['id'], $this->masterNodeId);

        $this->submitAudit();
    }

    function submitAudit()
    {

        $comment =sprintf('send to Master QTYs  for order_id: %d ', $this->order['order_id'] );

/*
        \Audit::action('cronjob', $this->Account->user_id, [
            'type' => 'bc_download',
            'object' => 'order',
            'object_id' => $this->order['order_id'],
            'comment' => $comment ]);
*/
    }

    function hasNextTask() : bool
    {
        return false;
    }

    function nextTask() : \ConcurrentCurlPool\Task
    {
        return null;
    }
}

