<?php

namespace BigCommerce\BcApiClient;

use BigCommerce\BcApiClient\BCAPIv2;
use BigCommerce\BcApiClient\BCAPITransfer;
use BigCommerce\BcApiClient\BCAPIResponse;

class PartialBCAPIv2Response extends BCAPIResponse
{
	protected $API;
	protected $Transfer;
	protected $body;

	function __construct(BCAPIv2 $API, BCAPITransfer $Transfer)
	{
		$this->API = $API;
		$this->Transfer = $Transfer;
	}


	function curl_handle() # : resource
	{
		return $this->Transfer->handle();
	}


	function BCAPIResponse() : BCAPIResponse
	{
		return $this->API->responseFromMultiTransfer($this->Transfer);
	}

}
