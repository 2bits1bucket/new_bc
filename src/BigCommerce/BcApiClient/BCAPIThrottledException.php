<?php
namespace BigCommerce\BcApiClient;

class BCAPIThrottledException extends BCAPITransientErrorException {}