<?php

namespace BigCommerce\BcApiClient;

class BCAPIException extends \Exception
{
	private $Api;

	function __construct(BCAPIv3 $Api, string $message, $error_code = 0)
	{
		$a = func_get_args();
		$this->Api = array_shift($a);
		$msg = array_shift($a);
		$msg = sprintf('%s (bc_account_id %d)', $msg, $this->Api->Account()->bc_account_pk);
		array_unshift($a, $msg);
//		call_user_func_array('parent::__construct', array($a, $error_code));

		parent::__construct($msg, $error_code);
	}
}
