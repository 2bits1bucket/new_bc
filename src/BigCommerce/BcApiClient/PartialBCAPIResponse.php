<?php
namespace BigCommerce\BcApiClient;

class PartialBCAPIResponse extends BCAPIResponse
{
	protected $API;
	protected $Transfer;
	protected $body;


	function __construct(BCAPIv3 $API, BCAPITransfer $Transfer)
	{
		$this->API = $API;
		$this->Transfer = $Transfer;
	}


	function curl_handle() # : resource
	{
		return $this->Transfer->handle();
	}


	function BCAPIResponse() : BCAPIResponse
	{
		return $this->API->responseFromMultiTransfer($this->Transfer);
	}
}
