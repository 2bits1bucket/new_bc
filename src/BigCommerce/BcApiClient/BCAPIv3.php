<?php
namespace BigCommerce\BcApiClient;

use BigCommerce\Models\BCAccount;

class BCAPIv3 {

	const url_base = 'https://api.bigcommerce.com/stores/{store_hash}/v3/';

	const HTTP_OK = 200;
	const HTTP_UNAUTHORIZED = 401;
	const HTTP_TOO_MANY_REQUESTS = 429;
	const HTTP_ISE = 500;
	const HTTP_SERVICE_UNAVAILABLE = 503;

	const RETRY_GET_ON_THTROTTLING_DEFAULT = 10;
	const RETRY_GET_ON_THTROTTLING_DEFAULT_SLEEP_SECONDS = 0.5;
	const RETRY_GET_ON_THTROTTLING_DEFAULT_TIMEOUT_SECONDS = 20;


	private $account;
	private $retryGetOnThrottling = self::RETRY_GET_ON_THTROTTLING_DEFAULT;
	private $retryGetOnThrottlingSleepSeconds = self::RETRY_GET_ON_THTROTTLING_DEFAULT_SLEEP_SECONDS;
	private $retryGetOnThrottlingTimeoutSeconds = self::RETRY_GET_ON_THTROTTLING_DEFAULT_TIMEOUT_SECONDS;


	function __construct(BCAccount $account)
	{
		$this->account = $account;

		if (empty($this->account->store_hash))
			throw new \BCAPIException('missing store_hash');
	}


	private
	function authHeaders() : array
	{
		return [
			'Accept: application/json',
			'X-Auth-Client: ' .$this->account->client_id,
			'X-Auth-Token: ' .$this->account->access_token,
		];
	}


	private
	function url(string $op, string $query = null) : string
	{
		if ($query !== null)
			$query = '?' .$query;
		return str_replace('{store_hash}', $this->account->store_hash, static::url_base) .$op .$query;
	}


	function post(string $op, string $payload) : BCAPIResponse
	{
		return $this->transfer('POST', $op, null, $payload);
	}


	function put(string $op, string $payload) : BCAPIResponse
	{
		return $this->transfer('PUT', $op, null, $payload);
	}


	function get(string $op, array $query = []) : BCAPIResponse
	{
		$Retries = null;

		while (true) {
			try {
				return $this->transfer('GET', $op, http_build_query($query)); }
			catch (BCAPIThrottledException $e) {
				if ($Retries === null)
					$Retries = new LinearBackoffRetry($this->retryGetOnThrottling, $this->retryGetOnThrottlingSleepSeconds, $this->retryGetOnThrottlingTimeoutSeconds);
				if ($Retries->anotherRetry()) {
					echo 'R'; xflush(); }
				else {
					$e->messageAppendRetriesTaken($Retries->retriesTaken());
					throw $e; } }
			catch (BCAPIISEException $e) {
				if ($Retries === null)
//					$Retries = new LinearBackoffRetry($this->retryGetOnThrottling, $this->retryGetOnThrottlingSleepSeconds, $this->retryGetOnThrottlingTimeoutSeconds);
				if ($Retries->anotherRetry()) {
					echo 'R'; xflush(); }
				else {
					$e->messageAppendRetriesTaken($Retries->retriesTaken());
					throw $e; } } }
	}


	private
	function headerHandler($Collector) : Callable
	{
		return (function($h, string $header)
			use($Collector)
		{
			$Collector->push($header);
			return strlen($header);
		})->bindTo($this);
	}


	private
	function rateLimitTrace(Collector $Headers) : string
	{
		$ret = '';

		$prefix = 'X-Rate-Limit-';
		$prefix_len = strlen($prefix);

		foreach ($Headers->asArray() as $h)
			if (strncmp($h, $prefix, $prefix_len) === 0)
				$a[substr(explode(':', $h, 2)[0], $prefix_len)] = explode(':', $h, 2)[1];

		foreach ($a as $k => $v)
			$ret .= $k .': ' .$v .PHP_EOL;

		return $ret;
	}


	protected
	function transfer_init(string $request, string $op, string $query = null, string $payload = null) : BCAPITransfer
	{
		$h = curl_init($this->url($op, $query));

		curl_setopt($h, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
		curl_setopt($h, CURLOPT_CUSTOMREQUEST, $request);
		curl_setopt($h, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($h, CURLOPT_HTTPHEADER, $this->authHeaders());

		curl_setopt($h, CURLOPT_HEADERFUNCTION, $this->headerHandler($Collector = new Collector()));

			# found in BC's PHP API implementation
			// Set to a blank string to make cURL include all encodings it can handle (gzip, deflate, identity) in the 'Accept-Encoding' request header and respect the 'Content-Encoding' response header
		curl_setopt($h, CURLOPT_ENCODING, '');

		if ($payload !== null)
			curl_setopt($h, CURLOPT_POSTFIELDS, $payload);

		return new BCAPITransfer($h, $Collector);
	}


	public
	function responseFromMultiTransfer(BCAPITransfer $Transfer) : BCAPIResponse
	{
		$Transfer->setBody(curl_multi_getcontent($Transfer->handle()));
		return $this->transfer_service($Transfer);
	}


	protected
	function transfer_service(BCAPITransfer $Transfer) : BCAPIResponse
	{
		$this->handleErrors($Transfer->body(), curl_getinfo($Transfer->handle(), CURLINFO_HTTP_CODE), $Transfer->Collector());

		return new BCAPIResponse($Transfer->Collector()->asArray(), $Transfer->body());
	}


	protected
	function transfer(string $request, string $op, string $query = null, string $payload = null) : BCAPIResponse
	{
		$Transfer = $this->transfer_init($request, $op, $query, $payload);
		$Transfer->setBody(curl_exec($Transfer->handle()));
		return $this->transfer_service($Transfer);
	}


	function handleErrors($body, int $http_code, Collector $Headers)
	{
		if ($body === false)
			throw new Exception(sprintf('curl returned error; HTTP status: %s', $http_code));

		switch ($http_code) {
		case static::HTTP_OK:
			return;
		case static::HTTP_TOO_MANY_REQUESTS:
			throw new BCAPIThrottledException(
				new BCAPIResponse($Headers->asArray(), $body),
				'Request throttled' );
		case static::HTTP_UNAUTHORIZED:
			throw new BCAPIAuthException('Invalid authentication credentials');
		case static::HTTP_ISE:
			throw new BCAPIISEException(
				new BCAPIResponse($Headers->asArray(), $body),$this,
				'Request error 500 Internal ');
		case static::HTTP_SERVICE_UNAVAILABLE:
			throw new BCAPIException($this, 'response 503 Service Unavailable', $http_code);
		default:
			throw new BCAPIException($this, sprintf('unexpected HTTP status: %s', $http_code), $http_code); }
	}


	function createCategory(string $name)
	{
		$a = compact('name');
		$a['parent_id'] = 0;
		return $this->post('catalog/categories', json_encode($a));
	}


	function testCredentials()
	{
		return ($this->get('catalog/products', [ 'limit' => 1 ]) !== '');
	}


		# a no-operation
	function emptyRequest()
	{
		return $this->getCatalogSummary();
	}


	function getCatalogSummary()
	{
		return $this->get('catalog/summary');
	}


	function getProducts(array $options = []) : BCAPIResponse
	{
		return $this->get('catalog/products', $options);
	}


	function getProductImages(int $product_id, array $options = []) : BCAPIResponse
	{
		return $this->get('catalog/products/' .U($product_id) .'/images', $options);
	}


	function getProductVariants(int $product_id, array $options = []) : BCAPIResponse
	{
		return $this->get('catalog/products/' .U($product_id) .'/variants', $options);
	}


	function createProduct(BCProduct $P) : BCAPIResponse
	{
		$data = json_decode($P->bulk_data);
		$data->sku = $P->sku;
		$data->name = $P->name;

		return $this->post('catalog/products', json_encode($data));
	}


	function updateVariant(int $product_id, int $variant_id, array $data) : BCAPIResponse
	{
		$url = sprintf('catalog/products/%d/variants/%d', $product_id, $variant_id);
		return $this->put($url, json_encode($data));
	}


	function updateProduct(int $product_id, array $data) : BCAPIResponse
	{
		$url = sprintf('catalog/products/%d', $product_id);
		return $this->put($url, json_encode($data));
	}


	function account() : BCAccount
	{
		return $this->account;
	}


	function getCategoryTree() : BCAPIResponse
	{
		return $this->get('catalog/categories/tree');
	}


	function getCategoryById(int $id) : BCAPIResponse
	{
		return $this->get(sprintf('catalog/categories/%d', $id));
	}
}
