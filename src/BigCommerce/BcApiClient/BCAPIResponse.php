<?php
namespace BigCommerce\BcApiClient;

class BCAPIResponse
{
	private $headers;
	private $body;

	private $parsed_body;


	function __construct(array $headers, string $body)
	{
		[ $this->headers, $this->body] = [ $headers, $body ];
	}


	function headers() : array
	{
		return $this->headers;
	}


	function body() : string
	{
		return $this->body;
	}


	function parsedBody() : \stdClass
	{
		if ($this->parsed_body === null)
			$this->parsed_body = json_decode($this->body);
		return $this->parsed_body;
	}


	function data()
	{
		return $this->parsedBody()->data;
	}


	function meta()
	{
		return $this->parsedBody()->meta;
	}


	function metaNextPage() : ?int
	{
		if (empty($this->meta()->pagination->links->next))
			return null;
		$str = $this->meta()->pagination->links->next;
		if (strpos($str, '?') === 0)
			$str = substr($str, 1);
		$a = [];
		parse_str($str, $a);
		if (empty($a['page']))
			throw new \LogicException('got [next] link, but without *page* component');
		return $a['page'];
	}
}
