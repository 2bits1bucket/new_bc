<?php

namespace BigCommerce\BcApiClient;

class PartialBCAPIv2 extends BCAPIv2
{
	protected
	function transfer(string $request, string $op, string $query = null, string $payload = null) : BCAPIResponse
	{
		return new PartialBCAPIv2Response($this, $this->transfer_init($request, $op, $query, $payload));
	}
}
