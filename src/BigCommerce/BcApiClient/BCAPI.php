<?php

namespace BigCommerce\BcApiClient;

class BCAPI //abstract ?
{


	protected $Account;


	function __construct(BCAccount $Account)
	{
		$this->Account = $Account;

		if (empty($this->Account->store_hash))
			throw new BCAPIException('missing store_hash');
	}

	private
	function authHeaders() : array
	{
		return [
			'Content-type: application/json',
			'Accept: application/json',
			'X-Auth-Client: ' .$this->Account->client_id,
			'X-Auth-Token: ' .$this->Account->access_token,
		];
	}

    function account() : BCAccount
    {
        return $this->Account;
    }

}
