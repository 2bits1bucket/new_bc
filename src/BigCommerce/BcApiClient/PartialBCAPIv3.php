<?php

namespace BigCommerce\BcApiClient;

class PartialBCAPIv3 extends BCAPIv3
{
	protected function transfer(string $request, string $op, string $query = null, string $payload = null) : BCAPIResponse
	{
		return new PartialBCAPIResponse($this, $this->transfer_init($request, $op, $query, $payload));
	}
}
