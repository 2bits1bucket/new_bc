<?php

namespace BigCommerce\BcApiClient;

use BigCommerce\Models\BCAccount;

class BCAPIv2
{
	const URL_BASE = 'https://api.bigcommerce.com';
	const URL_PATH = '/stores/%s/v2/%s';

	const HTTP_OK = 200;
    const HTTP_OK_BUT_EMPTY = 204;
	const HTTP_CREATED = 201;
	const HTTP_UNAUTHORIZED = 401;
	const HTTP_TOO_MANY_REQUESTS = 429;
	const HTTP_ISE = 500;

	protected $Account;

	function __construct(BCAccount $Account)
	{
		$this->Account = $Account;
		if (empty($this->Account->store_hash)){
            throw new BCAPIException('missing store_hash');
        }
	}


    function createOrder(array $data)
    {
        return $this->post('orders', json_encode($data));
    }


    /* Filters:
      ------------
        min_id	        int	        /api/v2/orders?min_id={value}
        max_id	        int	        /api/v2/orders?max_id={value}
        min_total	    decimal	    /api/v2/orders?min_total={value}
        max_total	    decimal	    /api/v2/orders?max_total={value}
        customer_id	    string	    /api/v2/orders?customer_id={value}
        email	        string	    /api/v2/orders?email={value}
        status_id	    int	        /api/v2/orders?status_id={value}
        cart_id	        string	    /api/v2/orders?cart_id={value}
        is_deleted	    string (‘true’ or 'false’)	/api/v2/orders?is_deleted={value}
        payment_method	    string	            /api/v2/orders?payment_method={value}
        min_date_created	dateTime or date	/api/v2/orders?min_date_created={value}
        max_date_created	dateTime or date	/api/v2/orders?max_date_created={value}
        min_date_modified	dateTime or date	/api/v2/orders?min_date_modified={value}
        max_date_modified	dateTime or date	/api/v2/orders?max_date_modified={value}
     */
    function getOrders($params)
    {
        return $this->get('orders', http_build_query($params));
    }

    function getOrdersCount()
    {
        return $this->get('orders/count', "");
    }

    function getOrderProducts(int $orderId)
    {
        $url = 'orders/'.$orderId.'/produts';
        return $this->get($url, "");
    }



    /**
     * Return Json array of orders from $date
     * Sorted by date_modified descending
     * @param $date DateTime::RFC1123
     * @return Json array orders
     */
    function getSyncOrderTasks($date){

        $params = [
            'min_date_modified' => $date,
            'sort'=>'date_modified:desc',
            'page'=>1,
            'limit' =>250

        ];

        $orders = $this->getOrders($params);

        $ordersData = json_decode($orders->body());

        return $ordersData;
    }

    // ---------------------------------------

    protected
    function get(string $url, string $query)
    {
        return $this->transfer('GET', $url, $query);
    }

    protected
	function put(string $url_object, string $payload)
	{
		return $this->transfer('PUT', $url_object, $payload);
	}


	protected
	function post(string $url_object, string $payload)
	{
		return $this->transfer('POST', $url_object, null, $payload);
	}


	protected
	function transfer_init(string $request, string $url_object, string $query = null, string $payload = null) : BCAPITransfer
	{
		$url_path = sprintf(static::URL_PATH,
			rawurlencode($this->Account->store_hash),
//			rawurlencode($url_object) ); //this is not good !
			$url_object );
		$url = static::URL_BASE .$url_path;

        if($query!==null){
            $url.='?'.$query;
        }

		$h = curl_init($url);

		curl_setopt($h, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
		curl_setopt($h, CURLOPT_CUSTOMREQUEST, $request);
		curl_setopt($h, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($h, CURLOPT_HTTPHEADER, $this->authHeaders());

		curl_setopt($h, CURLOPT_HEADERFUNCTION, $this->headerHandler($Collector = new Collector()));

			# found in BC's PHP API implementation
			// Set to a blank string to make cURL include all encodings it can handle (gzip, deflate, identity) in the 'Accept-Encoding' request header and respect the 'Content-Encoding' response header
		curl_setopt($h, CURLOPT_ENCODING, '');

		if ($payload !== null)
			curl_setopt($h, CURLOPT_POSTFIELDS, $payload);

		return new BCAPITransfer($h, $Collector);
	}

    public
    function responseFromMultiTransfer(BCAPITransfer $Transfer) : BCAPIResponse
    {
        $Transfer->setBody(curl_multi_getcontent($Transfer->handle()));
        return $this->transfer_service($Transfer);
    }

	protected
	function transfer_service(BCAPITransfer $Transfer) : BCAPIResponse
	{
		$this->handleErrors($Transfer->body(), curl_getinfo($Transfer->handle(), CURLINFO_HTTP_CODE), $Transfer->Collector());

		return new BCAPIResponse($Transfer->Collector()->asArray(), $Transfer->body());
	}


	protected
	function transfer(string $request, string $op, string $query = null, string $payload = null) : BCAPIResponse
	{
		$Transfer = $this->transfer_init($request, $op, $query, $payload);
		$Transfer->setBody(curl_exec($Transfer->handle()));
		return $this->transfer_service($Transfer);
	}


	private
	function headerHandler($Collector) : Callable
	{
		return (function($h, string $header)
			use($Collector)
		{
			$Collector->push($header);
			return strlen($header);
		})->bindTo($this);
	}


	function handleErrors($body, int $http_code, Collector $Headers)
	{
		if ($body === false)
			throw new Exception(sprintf('curl returned error; HTTP status: %s', $http_code));

		switch ($http_code) {
		case static::HTTP_OK:
		case static::HTTP_CREATED:
		case static::HTTP_OK_BUT_EMPTY:
			return;
		case static::HTTP_TOO_MANY_REQUESTS:
			throw new BCAPIThrottledException(
				new BCAPIResponse($Headers->asArray(), $body),
				'Request throttled' );
		case static::HTTP_UNAUTHORIZED:
			throw new BCAPIAuthException('Invalid authentication credentials');
		case static::HTTP_ISE:
			throw new BCAPIISEException(
				new BCAPIResponse($Headers->asArray(), $body),
				'Request throttled' );
		default:
			throw new BCAPIv2Exception($this, sprintf('unexpected HTTP status: %s', $http_code), $http_code); }
	}

	private
	function authHeaders() : array
	{
		return [
			'Content-type: application/json',
			'Accept: application/json',
			'X-Auth-Client: ' .$this->Account->client_id,
			'X-Auth-Token: ' .$this->Account->access_token,
		];
	}

    function account() : BCAccount
    {
        return $this->Account;
    }

}
