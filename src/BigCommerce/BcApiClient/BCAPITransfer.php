<?php
namespace BigCommerce\BcApiClient;

class BCAPITransfer
{
	protected $handle;
	protected $Collector;
	protected $body;


	function __construct(/*resource*/ $handle, Collector $Collector)
	{
		$this->handle = $handle;
		$this->Collector = $Collector;
	}


	function handle() # : resource
	{
		return $this->handle;
	}


	function Collector() : Collector
	{
		return $this->Collector;
	}


	function setBody(string $body)
	{
		$this->body = $body;
	}


	function body() : string
	{
		return $this->body;
	}
}
