<?php
namespace BigCommerce\BcApiClient;

class BCAPITransientErrorException extends BCAPIException {

	private $ApiResponse;

	function __construct(BCAPIResponse $response, BCAPIv3 $Api, string $message /*, ... */)
	{
		$a = func_get_args();
		$this->ApiResponse = array_shift($a);
		call_user_func_array('parent::__construct', $a);
	}


	function messageAppendRetriesTaken(int $retries)
	{
		$this->message .= '; retries taken: ' .$retries;
	}


	function getApiResponse() : BCAPIResponse
	{
		return $this->ApiResponse;
	}
}
