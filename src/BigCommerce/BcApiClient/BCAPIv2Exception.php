<?php

namespace BigCommerce\BcApiClient;

class BCAPIv2Exception extends \Exception
{
	private $Api;

    /**
     * BCAPIException constructor.
     * @param BCAPIv2 $Api
     * @param string $message
     */
	function __construct(BCAPIv2 $Api, string $message, $error_code = 0)
	{
		$a = func_get_args();
		$this->Api = array_shift($a);
		$msg = array_shift($a);
		$msg = sprintf('%s (bc_account_id %d)', $msg, $this->Api->Account()->bc_account_pk);
		array_unshift($a, $msg);
//		call_user_func_array('parent::__construct', array($a, $error_code));

		parent::__construct($a, $error_code);
	}

}
