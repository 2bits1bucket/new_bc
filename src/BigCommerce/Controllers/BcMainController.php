<?php

namespace BigCommerce\Controllers;

use BigCommerce\BcApiClient\BCAPIv3;
use BigCommerce\Services\AccountService;
use Slim\Container;

class BcMainController
{

    protected $accountService;

    public function __construct(Container $ci, AccountService $accountService) {
        $this->ci = $ci;
        $this->accountService = $accountService;
    }

    public function getProducts($request, $response, $args){

        $acc = $this->accountService->byId(8);

        $BCAPI = new BCAPIv3($acc);

//        $date_modified_min = '2018-02-05T20:05:14+00:00'; //FROM response
        $date_modified_min = '2018-02-05 19:00:14+01'; //FROM database

        $date = date(\DateTime::ISO8601, strtotime($date_modified_min));

//        $date = '2018-02-05T19:00:14+00:00';

        $options = [

            'sort' => 'date_modified',
            'direction' => 'asc',
            'include' => 'variants,images',
            'page' => 0,
            'limit' => 250,
//            'date_modified:min' => $date,
        ];

        $products = $BCAPI->getProducts($options);

        $ret['data'] = json_decode($products->body())->data;;
        $ret['date'] = $date;

        return $response->withJson($ret);
    }




}