<?php

namespace BigCommerce\Controllers;

use BigCommerce\BcApiClient\BCAPIv3;
use BigCommerce\Models\BCAccount;
use BigCommerce\Services\AccountService;
use Slim\Container;

class AccountController
{
    protected $accountService;

    public function __construct(Container $ci, AccountService $accountService)
    {
        $this->ci = $ci;
        $this->accountService = $accountService;
    }

    public function getList($request, $response, $args)
    {
        $accounts = $this->accountService->getAccounts();
        return $response->withJson($accounts);
    }

    public function persist($request, $response, $args)
    {
        $data = $request->getParams();

        if(isset($data['bc_account_pk']) && $data['bc_account_pk']>0){
            $account = $this->accountService->updateAccount($data);
        }else{
            $account = $this->accountService->createAccount($data);
        }

        return $response->withJson( new BCAccount($account) );
    }

    public function getById($request, $response, $args)
    {

    }

    public function update($request, $response, $args)
    {

    }

    public function deactivate($request, $response, $args)
    {

    }

    public function check($request, $response, $args)
    {

        $data = $request->getParams();

        if(isset($data['acc_id'])){
            $account = $this->accountService->byId($data['acc_id']);

            try{
                $bc = new BCAPIv3($account);
                $summary = $bc->getCatalogSummary();
                return $response->withJson( json_decode($summary->body()) );
            }catch (\Exception $e){

            }
        }

        return $response->withStatus(400);
    }

}
