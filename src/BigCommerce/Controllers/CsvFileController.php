<?php

namespace BigCommerce\Controllers;


use BigCommerce\Services\Csv\CsvProductExporter;
use Slim\Container;

class CsvFileController
{

    protected $accountService;

    protected $ci;
    protected $db;

    public function __construct(Container $ci)
    {
        $this->ci = $ci;
        $this->db = $ci->get('pdb');
    }

    public function jetCsvDownload($request, $response, $args)
    {

        $E = CsvProductExporter::factory(8, 'jet', $this->db);

        $E->setOutput(CsvProductExporter::HTTP_OUTPUT_STREAM);

        $E->outputStart();
        while (!$E->done()){
            $E->outputDataQuant();
        }

        $stream = new \Slim\Http\Stream($E->getStream());

        return $response->withHeader('Content-Type', 'application/force-download')
            ->withHeader('Content-Type', 'application/octet-stream')
            ->withHeader('Content-Type', 'application/download')
            ->withHeader('Content-Description', 'File Transfer')
            ->withHeader('Content-Transfer-Encoding', 'binary')
            ->withHeader('Content-Disposition', 'attachment; filename="' . basename($E->fileName()) . '"')
            ->withHeader('Expires', '0')
            ->withHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
            ->withHeader('Pragma', 'public')
            ->withBody($stream);
    }


}