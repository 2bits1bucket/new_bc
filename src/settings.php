<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'Azmodan',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        'db' => [
            'driver' => 'pgsql',
            'port' => $_ENV['PDB_PORT'],
            'host' => $_ENV['PDB_HOST'],   //https://pgsql.nazwa.pl/
            'database' => $_ENV['PDB_DBNAME'], //
            'username' => $_ENV['PDB_USER'],
            'password' => $_ENV['PDB_PASSWORD'],
            'schema'   => 'public',
            'client_encoding' => 'UTF-8',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci'
//            'prefix'    => '',
        ],
    ],
];
