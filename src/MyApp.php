<?php

namespace MyApp;

class MyApp
{
    /**
     * Stores an instance of the Slim application.
     *
     * @var \Slim\App
     */
    private $app;

    public static function getApp(){

        $settings = require __DIR__ . '/settings.php';

        $app = new \Slim\App($settings);

        require __DIR__ . '/dependencies.php';
        require __DIR__ . '/middleware.php';
        require __DIR__ . '/routes.php';

        return $app;

    }

    public function __construct() {

    }

}