<?php

use \PDB\PDB as PDB;

$container = $app->getContainer();

$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

$container['pdb'] = function ($c) {
    $settings = $c->get('settings')['db'];

    return new PDB(
        $settings['host'],$settings['port'], $settings['database'],
        $settings['username'],  $settings['password'] );

};

$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['\Admin\AdminController'] = function ($c) {
    return new Admin\AdminController($c);
};

$container['\Auth\AuthController'] = function ($c) {
    return new Auth\AuthController($c);
};

$container['\Auth\UserController'] = function ($c) {
    return new Auth\UserController($c);
};

$container['\Auth\GroupController'] = function ($c) {
    return new Auth\GroupController($c);
};

$container['accountService'] = function ($c) {
    $accountService = new \BigCommerce\Services\AccountService($c->get('pdb'));
    return $accountService;
};

$container['ordersService'] = function ($c) {
    $accountService = new \BigCommerce\Services\OrderService($c->get('pdb'));
    return $accountService;
};

$container['quantityStore'] = function ($c) {
    $accountService = new \BigCommerce\QtyDownloadSync\QuantityStore($c->get('pdb'));
    return $accountService;
};

$container['productService'] = function ($c) {
    $productService = \BigCommerce\Services\ProductService::getInstance($c->get('pdb'));
    return $productService;
};

$container['uploadMetaService'] = function ($c) {
    $UploadMetaService = \BigCommerce\Services\UploadMetaService::getInstance($c->get('pdb'), $c->get('productService'));
    return $UploadMetaService;
};


// http controllers

$container['\BigCommerce\Controllers\AccountController'] = function ($c) {
    return new \BigCommerce\Controllers\AccountController($c, $c->get('accountService'));
};


$container['\BigCommerce\Controllers\BcMainController'] = function ($c) {
    return new BigCommerce\Controllers\BcMainController($c, $c->get('accountService'));
};


// -------- command controllers

$container['\BigCommerce\CmdController\QtyOrderSyncController'] = function ($c) {
    return new BigCommerce\CmdController\QtyOrderSyncController($c, $c->get('accountService'), $c->get('ordersService'));
};

$container['\BigCommerce\CmdController\SingleThreadTestController'] = function ($c) {
    return new BigCommerce\CmdController\SingleThreadTestController($c, $c->get('accountService'), $c->get('ordersService'));
};

$container['\BigCommerce\CmdController\MultiCurlTestController'] = function ($c) {
    return new BigCommerce\CmdController\MultiCurlTestController($c, $c->get('accountService'), $c->get('ordersService'));
};

$container['\BigCommerce\CmdController\QtyUploadSyncController'] = function ($c) {
    return new BigCommerce\CmdController\QtyUploadSyncController($c, $c->get('accountService'), $c->get('uploadMetaService'));
};

$container['\BigCommerce\CmdController\ProductsQtySyncController'] = function ($c) {
    return new BigCommerce\CmdController\ProductsQtySyncController($c, $c->get('accountService'), $c->get('uploadMetaService'));
};

$container['\BigCommerce\CmdController\QtyDownloadSyncController'] = function ($c) {
    return new BigCommerce\CmdController\QtyDownloadSyncController($c, $c->get('accountService'), $c->get('productService'));
};

$container['\BigCommerce\CmdController\ProductListDownloadController'] = function ($c) {
    return new BigCommerce\CmdController\ProductListDownloadController($c, $c->get('accountService'), $c->get('productService'));
};

$container['\BigCommerce\CmdController\ProductSyncToMasterController'] = function ($c) {
    return new BigCommerce\CmdController\ProductSyncToMasterController($c, $c->get('accountService'), $c->get('productService'));
};

$container['\BigCommerce\CmdController\QtyFullUploadSyncController'] = function ($c) {
    return new BigCommerce\CmdController\QtyFullUploadSyncController($c, $c->get('accountService'), $c->get('productService'), $c->get('uploadMetaService'));
};

$container['\BigCommerce\CmdController\CategoryTreeSyncController'] = function ($c) {
    return new BigCommerce\CmdController\CategoryTreeSyncController($c);
};

