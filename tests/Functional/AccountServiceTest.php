<?php

namespace Tests\Functional;

use MyApp\MyApp;

class AccountServiceTest extends \PHPUnit_Framework_TestCase
{

    protected $app;
    protected $container;
    protected $accountService;

    public function setUp()
    {
        $this->app = MyApp::getApp();
        $this->container = $this->app->getContainer();

        //we need delay to connect database
        usleep(50);

        if($this->container->has('accountService')){
            $this->accountService = $this->container->get('accountService');
        }

    }

    public function testHasAccountService()
    {
        $bool = $this->container->has('accountService');
        $this->assertTrue($bool);
    }

    public function testGetAccountException(){

       try{

           $acc = $this->accountService->byId(8);

           var_dump($acc);

       }catch (\PDB\RowNotFoundException $e){

           echo $e->xdebug_message;

           $this->isTrue();
       }

    }

    public function testHasAccountForDownload(){
        $accounts = $this->accountService->getAccountsForDownloadSync();
        $this->assertNotEmpty($accounts);
    }


    public function testGetQtySyncAccounts()
    {
        $accounts = $this->accountService->getAccountsForOrderSync();
        $this->assertTrue(count($accounts)>0);
    }

}
