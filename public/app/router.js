/**
 * @desc        backbone router for pushState page routing
 *
 *  - animation loading http://tympanus.net/codrops/2014/04/23/page-loading-effects/
 */
define([
    "app",
    "models/SessionModel",
    "models/UserModel",
    "views/HeaderView",
], function(app, SessionModel, UserModel, HeaderView){


    var WebRouter = Backbone.Router.extend({

        initialize: function(){

            // _.bindAll(this, _.functions(this));
            _.bindAll.apply(_, [this].concat(_.functions(this)));

            this.route(":module/*path", 'invokeModule');
            this.route(":module", 'invokeModule');

            console.log('routes done !');

        },

        routes: {
            "" : "index",
            ":module/*path": 'invokeModule'
        },

        invokeModule: function(module, path){

            app.logger.debug('invoke module: '+module+" / "+path);

            var module_name = module+'-module';

            require([module_name], function(route){
                if (!app.Routers[module]) {
                    app.Routers[module] = new route(module);
                }
            });

        },

        index: function() {
            // Fix for non-pushState routing (IE9 and below)
            // this.render();

            console.log('this is index ! ')
        },

        /**
         * DO NOT USE THIS
         */
        render: function(){
            var hasPushState = !!(window.history && history.pushState);

            if(!hasPushState){

                this.navigate(window.location.pathname.substring(1), {trigger: true, replace: true});

            }else{

                if(app.session.isLogged()){

                    var posts = Backbone.Collection.extend({
                        model: {},
                    });

                    var view = new AdminView();

                }else{
                    // this.show(new LoginPageView({})); old way no session checking
                    var view = new LoginPageView();
                }
            }
        },

        // ----- OLD STUFF from -----------

        /* FIXME - check unbinding for Admin view loading stuff - */
        show: function(view, options){

            // Every page view in the router should need a header.
            // Instead of creating a base parent view, just assign the view to this
            // so we can create it if it doesn't yet exist

            /*
             if(!this.headerView){
             this.headerView = new HeaderView({});
             this.headerView.setElement($(".header")).render();
             }
             */

            // Close and unbind any existing page view
            if(this.currentView && _.isFunction(this.currentView.close)) this.currentView.close();

            // Establish the requested view into scope
            this.currentView = view;

            // Need to be authenticated before rendering view. //FIXME no we dont ? we checking on the server anyway
            // For cases like a user's settings page where we need to double check against the server.
            if (typeof options !== 'undefined' && options.requiresAuth){
                var self = this;
                app.session.checkAuth({
                    success: function(res){
                        // If auth successful, render inside the page wrapper
                        $('#content').html( self.currentView.render().$el);
                    }, error: function(res){
                        self.navigate("/", { trigger: true, replace: true }); //move to oj jquery error
                    }
                });

            } else {
                // Render inside the page wrapper
                $('#content').html(this.currentView.render().$el);
                //this.currentView.delegateEvents(this.currentView.events);        // Re-delegate events (unbound when closed)
            }

        },

    });

    return WebRouter;

});
