define([
    "app",
    "text!templates/login-page.html",
    //---------
    "parsley",
], function(app, LoginPageTpl){


    var LoginView = Backbone.View.extend({

        el: 'body',

        initialize: function () {
            // _.bindAll(this);
            _.bindAll.apply(_, [this].concat(_.functions(this)));
            $('body').addClass('login-page');
            this.render();
        },

        events: {

            'click #login-btn'                      : 'onLoginAttempt',
            'keyup #login-password-input'           : 'onPasswordKeyup',

        },



        // Allow enter press to trigger login
        onPasswordKeyup: function(evt){
            var k = evt.keyCode || evt.which;

            if (k == 13 && $('#login-password-input').val() === ''){
                evt.preventDefault();    // prevent enter-press submit when input is empty
            } else if(k == 13){
                evt.preventDefault();
                this.onLoginAttempt();
                return false;
            }
        },

        onLoginAttempt: function(evt){
            if(evt) evt.preventDefault();

            if(this.$("#login-form").parsley('validate')){
                app.session.login({
                    username: this.$("#login-username-input").val(),
                    password: this.$("#login-password-input").val()
                }, {
                    success: function(mod, res){
                        if(DEBUG) console.log("SUCCESS", mod, res);
                        //check what was here ! reload view ! and stuff
                    },
                    error: function(err){
                        if(DEBUG) console.log("ERROR", err);
                        app.showAlert('Bummer dude!', err.error, 'alert-danger'); 
                    }
                });
            } else {
                // Invalid clientside validations thru parsley
                if(DEBUG) console.log("Did not pass clientside validation");

            }
        },

        render:function () {

            if(app.session.isLogged()){
                this.template = _.template(LoggedInPageTpl);

                this.$el.addClass('page-container');



                $('body').removeClass('login-page');

            }else{
                this.template = _.template(LoginPageTpl);

            }

            this.$el.html(this.template({ user: app.session.user.toJSON() }));


           //after render !fixme

            return this;
        }

    });

    return LoginView;
});

