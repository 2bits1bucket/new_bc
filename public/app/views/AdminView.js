define([
    "app",
    "jquery",
    "text!templates/admin-index.html",
    "text!templates/side_menu.htm.html",
    "models/Privileges",
    // "parsley",
    "metisMenu",
], function(app, $, AdminViewTpl, SideBarTpl, Privileges){

    var SideBarView = Backbone.View.extend({
        el: '#side-nav',
        template: _.template(SideBarTpl),
        initialize: function(){

            app.logger.debug("Init SideBar ");

            var self = this;
            var privileges = new Privileges.privilegeList();

            privileges.fetch({
                success: function (data) {

                    app.logger.debug("get privileges/menu ", data);
                    self.privileges = data.toJSON();
                    self.render();

                    $('#side-nav').metisMenu();
                }
            });

        },
        render: function(){
            this.$el.html( this.template({menu: this.privileges}) );
            return this;
        }
    });

    var AdminView = Backbone.View.extend({
        el: 'body',
        privileges: null,
        template:_.template(AdminViewTpl),
        initialize: function () {

            _.bindAll.apply(_, [this].concat(_.functions(this))); //TODO check it if nesecery

            this.$el.addClass('page-container');

            $('body').removeClass('login-page');

            this.user = app.session.get('user');

            this.render();

            new SideBarView();

        },

        events: {
            "click   .sidebar-collapse-icon" :    "sidebarCollapse",
            "click   #logout" : "logout"
        },
        sidebarCollapse: function(e){

            console.log("sidebar collapse ");

            e.preventDefault();
            event.preventDefault();
            $('body.page-container').toggleClass('sidebar-collapsed').toggleClass('can-resize');
        },

        logout: function(e){
            e.preventDefault();
            console.log('logout !');

            window.location.hash = '';
            app.session.logout({});
            //refresh ?
        },

        render:function () {
            this.$el.html(this.template({ user: app.session.user.toJSON() }));
            return this;
        }

    });

    return AdminView;
});

