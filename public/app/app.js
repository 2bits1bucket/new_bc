/**
 * @desc        app globals
 */
define([
    "jquery",
    "underscore",
    "backbone",
    //-------------
    "moment",
    "jQueryValidation",
    // "validation_lang_pl"
],
function($, _, Backbone) {

    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $.validator.setDefaults({
        lang: "pl",
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
    });

    $.extend( $.validator.messages, {
        required: "To pole jest wymagane.",
        remote: "Proszę o wypełnienie tego pola.",
        email: "Proszę o podanie prawidłowego adresu email.",
        url: "Proszę o podanie prawidłowego URL.",
        date: "Proszę o podanie prawidłowej daty.",
        dateISO: "Proszę o podanie prawidłowej daty (ISO).",
        number: "Proszę o podanie prawidłowej liczby.",
        digits: "Proszę o podanie samych cyfr.",
        creditcard: "Proszę o podanie prawidłowej karty kredytowej.",
        equalTo: "Proszę o podanie tej samej wartości ponownie.",
        extension: "Proszę o podanie wartości z prawidłowym rozszerzeniem.",
        maxlength: $.validator.format( "Proszę o podanie nie więcej niż {0} znaków." ),
        minlength: $.validator.format( "Proszę o podanie przynajmniej {0} znaków." ),
        rangelength: $.validator.format( "Proszę o podanie wartości o długości od {0} do {1} znaków." ),
        range: $.validator.format( "Proszę o podanie wartości z przedziału od {0} do {1}." ),
        max: $.validator.format( "Proszę o podanie wartości mniejszej bądź równej {0}." ),
        min: $.validator.format( "Proszę o podanie wartości większej bądź równej {0}." ),
        pattern: $.validator.format( "Pole zawiera niedozwolone znaki." )
    } );


    var app = {
        root : "/",                     // The root path to run the application through.
        URL : "/",                      // Base application URL
        API : "/api/",                   // Base API URL (used by models & collections)

        // Show alert classes and hide after specified timeout
        showAlert: function(title, text, klass) {
            $("#header-alert").removeClass("alert-danger alert-warning alert-success alert-info");
            $("#header-alert").addClass(klass);
            $("#header-alert").html('<button class="close" data-dismiss="alert">×</button><strong>' + title + '</strong> ' + text);
            $("#header-alert").show('fast');
            setTimeout(function() {
                $("#header-alert").hide();
            }, 7000 );
        },

        logger: {
            info: function(args){
                console.log('[INFO]', args);
            },
            debug: function(args){
                if (DEBUG) {
                    console.log('[DEBUG]', args);
                    for (var i=1; i < arguments.length; i++) {
                        console.log(arguments[i]);
                    }
                    console.log('-------------------------');
                }
            }
        }
    };

    $.ajaxSetup({ cache: false });          // force ajax call on all browsers

    // Global event aggregator
    app.eventAggregator = _.extend({}, Backbone.Events);

    // View.close() event for garbage collection

    /*
     */
        Backbone.View.prototype.close = function() {
            this.remove();
            this.unbind();
            if (this.onClose) {
                this.onClose();
            }
        };



    return app;

});
