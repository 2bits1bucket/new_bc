/**
 * Main app initialization and initial auth check
 */
require([
    "app",
    "router",
    "models/SessionModel",
        "views/LoginPageView",
        "views/AdminView",
    "subRoute"
],
function(app, WebRouter, SessionModel, LoginPageView, AdminView) {

    // Just use GET and POST to support all browsers
    Backbone.emulateHTTP = true;

    app.router = new WebRouter();

    app.Routers = {}; 

    // Create a new session model and scope it to the app global
    // This will be a singleton, which other modules can access
    app.session = new SessionModel({});



    function check(){
        // Check the auth status upon initialization,
        // before rendering anything or matching routes
        app.session.checkAuth({ //FIXME rename to check session
            // Start the backbone routing once we have captured a user's auth status
            complete: function(){

                //FIXME
                if(!app.session.isLogged()){
                    new LoginPageView();
                }
            }
        })
    }

    check();

    function render(){
        if(app.session.isLogged()){
            var view = new AdminView();
            // we need to start backbone AFTER main view so we have main-container!
            if(!Backbone.History.started){
                Backbone.history.start();
            }

        }else{
            var view = new LoginPageView();
        }
    }

    // Listen for session logged_in state changes and re-render
    app.session.on("change:logged_in", function(){
        render();
    });

    // All navigation that is relative should be passed through the navigate //FIXME because of push state ?
    // method, to be processed by the router. If the link has a `data-bypass`
    // attribute, bypass the delegation completely.
    $('#content-app').on("click", "a:not([data-bypass])", function(evt) {
        evt.preventDefault();
        var href = $(this).attr("href");
        app.router.navigate(href, { trigger : true, replace : false });
    });

});

