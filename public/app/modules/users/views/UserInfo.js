define(['text!modules/users/templates/user_info.html'], function(tpl){

	var view = Backbone.View.extend({
		tpl: _.template(tpl),
		model: undefined,
		events: {
			
		},

		initialize: function() {
			this.render();
		},

		render: function() {
			this.el.innerHTML = this.tpl();
			return this;
		}
	});

	return view;

});