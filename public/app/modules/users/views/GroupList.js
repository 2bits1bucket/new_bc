define([
	'app',
	'bootbox',
	"models/GroupModel",
	'text!modules/users/templates/group_list.html',
	'text!modules/users/templates/common.html',
	"models/Privileges",
], function(app, bootbox, GroupModels, templates, commonTpl, Privileges){

	var GroupEntryView = Backbone.View.extend({
		tagName: "tr",
		template: _.template($(templates).closest('#group-entry').html()),
		events: {
			"click .delete"   : "deleteGroup",
			"click .privileges"  : "privilegesView",
			"click .users-in-group" : "showUsersInGroup",
			"click .toggle-privilege" : "togglePrivilege",
		},
		initialize: function() {
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove); //what is remove
		},
		render: function() {

			var row = this.template(this.model.toJSON());
			this.$el.html($(row).hide().fadeIn());
			// this.$el.toggleClass('done', this.model.get('done'));
			// this.input = this.$('.edit');
			return this;
		},
		deleteGroup: function(){
			app.logger.debug("delete group", this.model);
		},
		togglePrivilege: function(){
			console.log("toggle privileget !@#!@#!@"); //TODO not working
		},
		privilegesView: function(){

			var msg = $(commonTpl).closest('#privileges-list').html();

			app.logger.debug(" show/set privileges ", this.model);

			var privileges = new Privileges.privilegeList();

			privileges.fetch({
				data: {
					group: this.model.id,
					all: true
				},
				processData: true,
				success: function(categories){

					var tpl = _.template($(commonTpl).closest('#privilege-category').html());

					//fixme - make with a view ! TODO make a view aut of it with !!@#1@#
					$('#privilege-accordion').html(tpl({categories:categories.toJSON()}));
				}
			});

			bootbox.dialog({
				locale: "pl",
				message: msg,
				title: "Prawa grupy",
				buttons: {
					danger: {
						label: "Ok",
						className: "btn-info",
						callback: function() {
							return true;
						}
					}
				}
			});

		},
		showUsersInGroup: function(){
			app.logger.debug("show users in group ", this.model);
		}
	});

	var groupListView = Backbone.View.extend({
		tpl: _.template($(templates).closest('#group-list-tpl').html()),
		model: undefined,
		events: {
			'click #new-group' : 'newGroup'
		},
		initialize: function() {

			this.render();

			this.groupList = new GroupModels.GroupList();

			this.listenTo(this.groupList, 'add', this.addOne);
			this.listenTo(this.groupList, 'reset', this.addAll);

			this.groupList.fetch();
		},

		addOne: function(group) {
			var view = new GroupEntryView({model: group});
			this.$("#group-list-body").prepend(view.render().el);
		},

		addAll: function() {
			this.groupList.each(this.addOne, this);
		},

		render: function(userList) {
			this.el.innerHTML = this.tpl();
			return this;
		},

		newGroup: function(){

			var that = this;

			var formTpl = _.template($(templates).closest('#group-form-tpl').html());

			var Group = new GroupModels.Group();

			var formView = formTpl((Group).toJSON());

			bootbox.dialog({
				locale: "pl",
				message: formView,
				title: "Dodaj nową grupę",
				buttons: {
					success: {
						label: "Zapisz",
						className: "btn-success",
						callback: function() {

							var $form = $('#group-form');

							var isValid = Group.formIsValid($form);

							if(isValid){

								var formArgs = $form.serializeObject();

								new GroupModels.Group(formArgs).save({}, {
									success: function(data){
										that.addOne(data);
									}
								});

								return true;
							}else{
								return false;
							}

						}
					},
					danger: {
						label: "Anuluj",
						className: "btn-danger",
						callback: function() {
							return true;
						}
					}
				}
			});

			return false;
		}


	});

	return groupListView;

});