define([
	'bootbox',
	"models/UserModel",
	'text!modules/users/templates/user_list.html'], function(bootbox, userModels, templates){

	
	var userListView = Backbone.View.extend({
		tpl: _.template($(templates).closest('#user-list-tpl').html()),
		model: undefined,
		events: {
			'click #new-user' : 'newUser'
		},
		initialize: function() {

			var userList = new userModels.UserList();

			var that = this;

			userList.fetch({
				success: function(data){
					that._List = data;
					that.render(data.toJSON());
				}
			});

			this.render();
		},
		render: function(userList) {
			this.el.innerHTML = this.tpl({users: userList});
			return this;
		},

		newUser: function(){

			var that = this;

			var formTpl = _.template($(templates).closest('#user-form-tpl').html());

			var User = new userModels.User();

			var formView = formTpl((User).toJSON());

			bootbox.dialog({
				locale: "pl",
				message: formView,
				title: "Dodaj nowego użytkownika",
				buttons: {
					success: {
						label: "Zapisz",
						className: "btn-success",
						callback: function() {

							var $form = $('#user-form');

							var isValid = User.formIsValid($form);

							if(isValid){

								var formArgs = $form.serializeObject();

								new userModels.User(formArgs).save({}, {
									success: function(){
										that.initialize();
									}
								});

								return true;
							}else{
								return false;
							}

						}
					},
					danger: {
						label: "Anuluj",
						className: "btn-danger",
						callback: function() {
							return true;
						}
					}
				}
			});

			return false;
		}


	});

	return userListView;

});