/**
 * Created by masakra on 05.11.16.
 */

define([
    'app',
    'modules/users/views/UserList',
    'modules/users/views/UserInfo',
    'modules/users/views/GroupList'
], function(app, userListView, userInfoView, groupListView) {

    //http://stackoverflow.com/questions/9403675/backbone-view-inherit-and-extend-events-from-parent
    var r  = Backbone.SubRoute.extend({     //      ModuleSubRoute extend
        routes: {
            ":path"         :"fromPath",
            ":path/:id"     :"fromPath",
        },
        views: [],
        el: '#main-content',
        camelize: function (str) {
            return str.replace(/(-|_)([a-z])/g, function (g) { return g[1].toUpperCase(); });
        },

        fromPath:function(path){

            app.logger.debug('From path ', path);

            var fn =  this.camelize(path);

            if(this[fn]){
                this[fn].apply(this, _.rest(arguments));
            }else{
                app.logger.debug("no function from "+path, fn );
            }

        },

        initialize: function(path){
            app.logger.debug('init subModule for: ', path);
        },

        userList: function() {
            app.logger.debug('user list view ');

            if(!this.views['user_list']){
                this.views['user_list'] = new userListView({el: this.el});
            }else{
                this.views['user_list'].initialize();
            }

        },

        groupList: function() {
            app.logger.debug('group list view ');

            if(!this.views['group_list']) {
                this.views['group_list'] = new groupListView({el: this.el});
            }else{
                this.views['group_list'].initialize();
            }

        },

        info: function(id){
            app.logger.debug('user info view for: id', id);
            if(!this.views['user_info']) {
                this.views['user_info'] = new userInfoView({el: this.el});
            } else {
                this.views['user_info'].initialize();
            }
        }

    });

    return r;
});
