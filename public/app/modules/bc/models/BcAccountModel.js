/**
 * @desc		stores the POST state and response state of authentication for user
 */
define([
    "app"
], function(app){

    var BcAccount = Backbone.Model.extend({

        idAttribute: "bc_account_pk",

        initialize: function(){
            // _.bindAll(this);
            // _.bindAll.apply(_, [this].concat(_.functions(this)));
        },

        defaults: {
            user_id: 0,
            bc_account_pk: 0,
            friendly_name: '',
            store_hash: '',
            client_id: '',
            client_secret: '',
            access_token: '',
            qty_download_master_node: '',
            qty_sync_schema: '',
            qty_upload_master_nodes:''
        },

        url: function(){
            return app.API + 'account';
        },

        formIsValid: function($obj){

            $obj.validate({
                rules: {
                    friendly_name: {
                        minlength: 3,
                        required: true
                    },
                    store_hash: {
                        minlength: 3,
                        required: true
                    },
                    client_id: {
                        minlength: 3,
                        required: true
                    },
                    client_secret: {
                        minlength: 3,
                        required: true
                    },
                    access_token: {
                        required: true,
                        minlength: 5
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                success: function ( label, element ) { }
            });

            return $obj.valid();
        }

    });

    var BcAccountList = Backbone.Collection.extend({
        model: BcAccount,
        initialize: function(args) {
            this.url = function() { return app.API + 'account/list' };
        }
    });

    var models = {
        BcAccount:BcAccount,
        BcAccountList:BcAccountList
    };

    return models;
});

