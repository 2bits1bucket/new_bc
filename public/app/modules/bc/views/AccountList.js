define([
	'bootbox',
	"modules/bc/models/BcAccountModel",
	'text!modules/bc/templates/account_list.html'], function(bootbox, accountModels, templates){
    var AccountEntryView = Backbone.View.extend({
        tagName: "tr",
        template: _.template($(templates).closest('#account-entry').html()),
		_connectionTestTpl: _.template($(templates).closest('#connection-test').html()),
        events: {
            "click .test-connection"  : "testConnection",
            "click .edit-account" : "editAccount",
        },
        initialize: function() {
            this.listenTo(this.model, 'change', this.render);
            // this.listenTo(this.model, 'destroy', this.remove);
        },
        render: function() {

            var row = this.template(this.model.toJSON());
            this.$el.html($(row).hide().fadeIn());
            // this.$el.toggleClass('done', this.model.get('done'));
            // this.input = this.$('.edit');
            return this;
        },
        testConnection: function(e){
        	e.preventDefault();
            console.log("test connection", this.model)

			var that = this;

            var dialog = bootbox.dialog({
                title: 'Checking Big Commerce Account',
                message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
            });

            var id = this.model.attributes.bc_account_pk;

            $.ajax({
                url: 'api/bc/account/check',
                type: 'GET',
				data: {'acc_id': id},
                success: function(response){
                    console.log("success check", response);
                    dialog.find('.bootbox-body').html(that._connectionTestTpl(response.data));
                },
                error: function(data) {
                    dialog.find('.bootbox-body').html('<div class="alert alert-danger" role="alert">Connection problem!</div>');
                }
            });

        },
        editAccount: function(e){
            e.preventDefault();
            console.log("edit account", this.model);

            var that = this;

            var BcAccount = new accountModels.BcAccount();

            var formTpl = _.template($(templates).closest('#account-form-tpl').html());

            var message = formTpl(this.model.toJSON());

            bootbox.dialog({
                locale: "pl",
                message: message,
                title: "Edit Account",
                buttons: {
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: function() {

                            var $form = $('#account-form');

                            var isValid = BcAccount.formIsValid($form);

                            if(isValid){

                                var formArgs = $form.serializeObject();

                                $.map(formArgs, function(value, name){
                                    that.model.set(name, value);
                                });

                                that.model.save();

                                return true;
                            }else{
                                return false;
                            }

                        }
                    },
                    danger: {
                        label: "Cancel",
                        className: "btn-danger",
                        callback: function() {
                            return true;
                        }
                    }
                }
            });


            return false;
        }

    });

    var accountListView = Backbone.View.extend({
        tpl: _.template($(templates).closest('#account-list-tpl').html()),
        model: undefined,
        events: {
            'click #new-account' : 'newAccount',
        },
        initialize: function() {

            this.accountList = new accountModels.BcAccountList();

            this.render();

            this.listenTo(this.accountList, 'add', this.addOne);
            this.listenTo(this.accountList, 'reset', this.addAll);

            this.accountList.fetch();

        },

        addOne: function(account) {
            var view = new AccountEntryView({model: account});
            this.$("#account-list-body").prepend(view.render().el);
        },

        addAll: function() {
            this.accountList.each(this.addOne, this);
        },

        render: function(userList) {
            this.el.innerHTML = this.tpl();
            return this;
        },

        newAccount: function(){

            var that = this;

            var formTpl = _.template($(templates).closest('#account-form-tpl').html());
            var BcAccount = new accountModels.BcAccount();
            var formView = formTpl((BcAccount).toJSON());

            bootbox.dialog({
                // locale: "pl", //change locale
                message: formView,
                title: "Add new Big Commerce Account",
                buttons: {
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: function() {

                            var $form = $('#account-form');

                            var isValid = BcAccount.formIsValid($form);

                            if(isValid){

                                var formArgs = $form.serializeObject();

                                new accountModels.BcAccount(formArgs).save({}, {
                                    success: function(data){
                                        that.addOne(data);
                                    }
                                });

                                return true;
                            }else{
                                return false;
                            }

                        }
                    },
                    danger: {
                        label: "Cancel",
                        className: "btn-danger",
                        callback: function() {
                            return true;
                        }
                    }
                }
            });

            return false;
        }


    });

	return accountListView;

});