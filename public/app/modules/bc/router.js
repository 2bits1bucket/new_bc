
define([
    'app',
    'modules/bc/views/AccountList',
    // 'modules/users/views/UserInfo',
    // 'modules/users/views/GroupList'
], function(app, accountsListView ) {

    var r  = Backbone.SubRoute.extend({
        routes: {
            ":path"         :"fromPath",
            ":path/:id"     :"fromPath",
        },
        views: [],
        el: '#main-content',
        camelize: function (str) {
            return str.replace(/(-|_)([a-z])/g, function (g) { return g[1].toUpperCase(); });
        },

        fromPath:function(path){

            app.logger.debug('From path ', path);

            var fn =  this.camelize(path);

            if(this[fn]){
                this[fn].apply(this, _.rest(arguments));
            }else{
                app.logger.debug("no function from "+path, fn );
            }

        },

        initialize: function(path){
            app.logger.debug('init subModule for: ', path);
        },

        accounts: function() {
            if(!this.views['accounts']){
                this.views['accounts'] = new accountsListView({el: this.el});
            }else{
                this.views['accounts'].initialize();
            }

        },

    });

    return r;
});
