/**
 * @desc        configure aliases and dependencies
 */

if (typeof DEBUG === 'undefined') DEBUG = true; 


/*
 paths: {
 'vendors': '../vendors',
 'js': '../js',
 'start': '../app-admin'
 }
 */

require.config({

    baseUrl: 'app/',

    paths: {
        'js': '../js',
        'jquery'                : '../js/jquery.min',
        'underscore'            : '../js/underscore',  //   load lodash instead of underscore (faster + bugfixes)
        'backbone'              : '../js/backbone',
        'subRoute'              : '../js/backbone.subroute',
        'bootstrap'             : '../js/bootstrap',
        'text'                  : '../js/text',
        'parsley'               : '../js/parsley',
        'bootbox'               : '../js/bootbox.min',
        'metisMenu'             : '../js/plugins/metismenu/jquery.metisMenu',
        'fileUpload'            : '../js/jquery.fileupload',
        'moment'                : '../js/moment.min',
        'jQueryValidation'      : '../js/validation/jquery.validate.min',
        //------------------ modules -----------------
        'users-module'          : 'modules/users/router',
        'auth-module'           : 'modules/users/router',
        'bc-module'             : 'modules/bc/router'
    },

    // non-AMD lib
    shim: {
        'underscore'            : { deps : ['jquery'], exports  : '_' },
        'backbone'              : { deps : ['underscore', 'jquery'], exports : 'Backbone' },
        'bootstrap'             : { deps : ['jquery'], exports : 'Bootstrap' },
        'parsley'               : { deps : ['jquery'] },
        'metisMenu'             : { deps : ['jquery'] },
        'jQueryValidation'      : { deps : ['jquery'] },
        'validation_lang_pl'    : { deps : ['jQueryValidation']}
    }

});

require(['main']);           // Initialize the application with the main application file.
