define(["app"],function(app){

    var privilege = Backbone.Model.extend({
        url: function(){
            return 'api/auth/privilege';
        }
    });

    var privilegeList = Backbone.Collection.extend({
        model: privilege,
        url: function(){
            return 'api/auth/privilege';
        }
    });

    return {privilege:privilege, privilegeList:privilegeList};
});