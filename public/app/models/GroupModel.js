/**
 * @desc		stores the POST state and response state of authentication for user
 */
define([
    "app"
], function(app){

    var Group = Backbone.Model.extend({

        initialize: function(){
            // _.bindAll(this);
            // _.bindAll.apply(_, [this].concat(_.functions(this)));
        },

        defaults: {
            id: 0,
            name: '',
            desc: '',
        },

        url: function(){
            return app.API + 'group';
        },

        formIsValid: function($obj){

            $obj.validate({
                rules: {
                    name: {
                        minlength: 3,
                        required: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                success: function ( label, element ) { }
            });

            return $obj.valid();
        }

    });

    var GroupList = Backbone.Collection.extend({
        model: Group,
        initialize: function(args) {
            this.url = function() { return app.API + 'group/list' };
        }
    });

    var models = {
        Group:Group,
        GroupList:GroupList
    };

    return models;
});

