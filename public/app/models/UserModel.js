/**
 * @desc		stores the POST state and response state of authentication for user
 */
define([
    "app"
], function(app){

    var User = Backbone.Model.extend({

        initialize: function(){
            // _.bindAll(this);
            // _.bindAll.apply(_, [this].concat(_.functions(this)));
        },

        defaults: {
            id: 0,
            username: '',
            firstname: '',
            lastname: '',
            phone: '',
            email: ''
        },

        url: function(){
            return app.API + 'user';
        },

        formIsValid: function($obj){

            $obj.validate({
                rules: {
                    firstname: {
                        minlength: 3,
                        required: true
                    },
                    lastname: {
                        minlength: 3,
                        required: true
                    },
                    username: {
                        minlength: 3,
                        required: true
                    },
                    passwd: {
                        required: true,
                        minlength: 5

                    },
                    re_passwd: {
                        required: true,
                        equalTo: "#passwd",
                        minlength: 3
                    },
                    email: {
                        email: true
                    },
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                success: function ( label, element ) { }
            });

            return $obj.valid();
        }

    });

    var UserList = Backbone.Collection.extend({
        model: User,
        initialize: function(args) {
            this.url = function() { return app.API + 'user/list' };
        }
    });

    var models = {
        User:User,
        UserList:UserList
    };


    return models;
});

