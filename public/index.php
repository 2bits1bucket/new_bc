<?php

require __DIR__ . '/../vendor/autoload.php';

if (file_exists('../.env')){
    (new Dotenv\Dotenv('../'))->load();
}

$app = \MyApp\MyApp::getApp();

$app->run();

