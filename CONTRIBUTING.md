 Contribute

## Pull Requests

1. Fork the Slim Skeleton repository
2. Create a new branch for each feature or improvement
3. Send a pull request from each feature branch to the **3.x** branch

It is very important to separate new features or improvements into separate feature branches, and to send a
pull request for each branch. This allows us to review and pull in new features or improvements individually.

## Available commands

    in package \BigCommerce\CmdController\

* account sync first ?
* pts - Product transfer sync ! DO WE NEED ? - USE quantity Download sync first run for that !?

* qds - Quantity Download Sync (QtyDownloadSyncController:runQtySync)
Downloads products quantities from Big Commerce account and uploads them to master
,for accounts qty_sync_schema = qty_sync_download_marketplace_forward_to_master

* cts - Category Tree Sync (CategoryTreeSyncController:runQtySync)
Downloads and update Category Tree for each BC account

* qus - Quantity Upload Sync (QtyUploadSyncController:runQtySync)
Download sku quantities from Master and uploads them to BC account inventory
,for accounts qty_sync_schema = qty_sync_fetch_master_upload_to_marketplace

* qos - Quantity Order Sync (QtyOrderSyncController:runQtySync)
Download last orders from BC account and update order Products quantities send them to Master
,for accounts qty_sync_schema = qty_sync_fetch_master_upload_to_marketplace


### Tests
* fastqds - QtyDownloadSyncController:fastRunQtySync
* test_run - QtyOrderSyncController:testRun
* single_run - SingleThreadTestController:testSingleThread
* pool_run - MultiCurlTestController:multiCurlTest


## Style Guide

All pull requests must adhere to the [PSR-2 standard](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md).


*/5 * * * * wget --content-on-error -qO-  --no-check-certificate https://bc.geekseller.com/jobs/quantity-download.php >> /var/www/bc/jobs/logs/quantity-download.php.log 2>&1
*/5 * * * * wget --content-on-error -qO-  --no-check-certificate https://bc.geekseller.com/jobs/product-transfers.php >> /var/www/bc/jobs/logs/product-transfers.php.log 2>&1
*/5 * * * * wget --content-on-error -qO-  --no-check-certificate https://bc.geekseller.com/jobs/category-fetch.php >> /var/www/bc/jobs/logs/category-fetch.php.log 2>&1
2,7,12,17,22,27,32,37,43,47,52,57 * * * * wget --content-on-error -qO-  --no-check-certificate https://bc.geekseller.com/jobs/quantity-upload.php >> /var/www/bc/jobs/logs/quantity-upload.php.log 2>&1


